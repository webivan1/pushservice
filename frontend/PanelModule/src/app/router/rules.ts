import { Routes, RouterModule } from '@angular/router';
import { DataResolver } from '../app.resolver';

import * as ALL_COMPONENTS from '../components';

export const ROUTES: Routes = [
  {
    path: ':lang/panel',
    component: ALL_COMPONENTS.LangComponent,
    children: [
      {
        path: '',
        component: ALL_COMPONENTS.HomeComponent
      },
      {
        path: 'profile',
        component: ALL_COMPONENTS.ProfileWrapComponent
      },
      {
        path: 'apps/create',
        component: ALL_COMPONENTS.AppsCreateComponent
      },
      {
        path: 'apps',
        component: ALL_COMPONENTS.AppsListComponent
      },
      {
        path: 'apps/:id',
        component: ALL_COMPONENTS.AppDetailComponent,
        children: [
          {
            path: '',
            component: ALL_COMPONENTS.AppDetailInfoComponent
          },
          {
            path: 'subs',
            component: ALL_COMPONENTS.SubsListComponent
          },
          {
            path: 'template',
            children: [
              {
                path: '',
                component: ALL_COMPONENTS.GridTemplateComponent
              },
              {
                path: 'create',
                component: ALL_COMPONENTS.CreateTemplateComponent
              },
              {
                path: 'update/:id',
                component: ALL_COMPONENTS.EditTemplateComponent
              }
            ]
          }
        ]
      },
      {
        path: '**',
        component: ALL_COMPONENTS.ErrorComponent
      }
    ]
  },
  {
    path: '',
    redirectTo: '/ru/panel',
    pathMatch: 'full'
  }
];