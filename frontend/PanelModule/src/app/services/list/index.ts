import { Injectable } from "@angular/core";
import { HttpService } from "../http.service";

export interface ISortAttribute {
  name: string;
  alias: string;
  sort: string;
}

@Injectable()
export class ListService {

  public loader: boolean = false;
  public sendUrl: string;
  public pageVar: string = 'page';
  public sort: ISortAttribute;
  public handlerResponse: Function;
  public search: {} = {};

  public currentPage: number = 1;
  public total: number = 0;
  public pageSize: number = 0;

  constructor(public http: HttpService) {}

  public getModels() {
    if (this.loader === true) {
      console.warn("Loader list... waiting");
      return false;
    }

    this.loader = true;

    this.http.postPrependLang(this.getUrl(), this.search, (response: any) => {
      if ('current_page' in response) {
        this.currentPage = response.current_page;
      }

      if ('total' in response) {
        this.total = response.total;
      }

      if ('per_page' in response) {
        this.pageSize = response.per_page;
      }

      this.handlerResponse(response);
      this.loader = false;
    });
  }

  public getSearch() {
    this.currentPage = 1;
    this.getModels();
  }

  public getUrl(): string {
    let params = {};

    params[this.pageVar] = this.currentPage;

    if (this.sort) {
      params['sort'] = `${this.sort.alias}.${this.sort.sort}`;
    }

    return this.sendUrl + '?' + Object.entries(params).map(([key, val]) => `${key}=${val}`).join('&');
  }

  public pageAction(page: number) {
    this.currentPage = page;
    this.getModels();
  }

  public sortAction(sort: ISortAttribute) {
    this.sort = sort;
    // clear pager
    this.currentPage = 1;
    this.getModels();
  }

}