import { Injectable } from "@angular/core";
import { HttpService } from "../http.service";

interface IChart {
  datas: any;
  labels: any;
  monthAll: Array<string>;
}

interface IData {
  user_id: number;
  updated_at: string|null;
  state: number;
  server_key: string;
  name: string;
  key: string;
  id: number;
  get_detail_stat_subs: Array<any>;
  created_at: string|null;
  chart?: IChart;
}

@Injectable()
export class DetailAppService {

  public loader: boolean = true;
  public data: IData|null;
  public deleteLoader: boolean = false;

  constructor(private http: HttpService) {}

  getModel(id: number) {
    this.http.postPrependLang(`/panel/apps/${id}`, {}, (response: any) => {
      this.data = response;
      this.loader = false;
    });
  }

  delete(successCall: Function): void|boolean {
    if (this.deleteLoader === true) {
      return false;
    }

    this.deleteLoader = true;

    this.http.deletePrependUrl(`/panel/apps/delete/${this.data.id}`, {}, (response) => {
      if (response.status === 'success') {
        this.deleteLoader = false;
        successCall();
      }
    });
  }

}