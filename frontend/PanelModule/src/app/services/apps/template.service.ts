import { Injectable } from "@angular/core";
import { ListService } from "../list/index";
import { DetailAppService } from "./detail";
import { HttpService } from "../http.service";

@Injectable()
export class TemplateService {

  public models: any[] = [];

  constructor(
    public list: ListService,
    public app: DetailAppService,
    public http: HttpService
  ) {
    this.list.sendUrl = `/panel/templates/${this.app.data.id}/get`;
    this.list.handlerResponse = (response: any) => {
      this.models = response.data
    };
  }

  delete(id: number, key: number) {
    this.http.deletePrependUrl(`/panel/templates/${this.app.data.id}/delete/${id}`, {}, (response) => {
      if (this.models.length > 0) {
        this.models.splice(key, 1);
      }
    });
  }

}