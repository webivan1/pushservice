import { Injectable } from "@angular/core";
import { HttpService } from "../http.service";
import { ListService } from "../list/index";

export interface IChart {
  datas: Array<any>;
  labels: Array<string|number>;
}

export interface IStatSubs {
  items: string;
  day: string;
  app_id: string;
}

export interface IItem {
  get_stat_subs: Array<IStatSubs|any>;
  chart?: IChart;
  key: string;
  id: number;
  count_subs: number;
  name: string|null;
}

export interface IApps {
  current_page: number;
  data: Array<IItem>;
  from: number|null;
  last_page: number;
  next_page_url: string|null;
  path: string;
  per_page: number;
  prev_page_url: string|null;
  to: number|null;
  total: number;
}

@Injectable()
export class AppsService {

  public models: Array<IItem>|Array<{}> = [];

  constructor(public list: ListService) {
    this.list.sendUrl = '/panel/apps/get';
    this.list.handlerResponse = (response: IApps) => {
      if (response.data.length) {
        response.data.forEach((item: IItem) => {
          if (item.get_stat_subs.length > 0) {

            item['chart'] = {
              datas: [[]],
              labels: []
            };

            item.get_stat_subs.forEach((stat: IStatSubs) => {
              item.chart.datas[0].push(stat.items);
              item.chart.labels.push(stat.day);
            });

            return item;
          }
        });
      }

      this.models = response.data
    };
  }

}