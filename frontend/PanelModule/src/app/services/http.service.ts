import { Injectable } from '@angular/core';
import { Http, Headers, URLSearchParams } from '@angular/http';
import { TranslateService } from '@ngx-translate/core';

declare var window;

@Injectable()
export class HttpService {

  public prependUrl: string = '';
  public apiUrl: boolean = true;

  constructor(public http: Http, private lang: TranslateService) {
    if (window.location.hostname.indexOf('localhost') >= 0) {
      this.prependUrl = 'http://push-service.loc';
    }
  }

  private headers() {
    let header = new Headers();

    // header.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

    let csrf = document.querySelector('meta[name="csrf-token"]');

    if (csrf) {
      header.append('X-CSRF-TOKEN', csrf.getAttribute('content'));
    }

    return { headers: header };
  }

  private parseUrl(url: string): string {
    if (!url.match(/^\//)) {
      url = "/" + url;
    }

    return this.prependUrl + url.replace(/\/$/, '');
  }

  private userData(response: any) {
    if ('guest' in response) {
      //this.auth.user.guest = response.guest;
    }

    if ('user' in response && typeof response.user === 'object') {
      //this.auth.user.user = response.user;
    }
  }

  private success(response: any, handlerFunction: Function) {
    let responseData;

    try {
      responseData = response.json();
    } catch (err) {
      responseData = response;
    }

    this.userData(responseData);

    handlerFunction(responseData);

    return responseData;
  }

  private error(error: any, ErrorHandler?: Function) {
    if (ErrorHandler) {
      return ErrorHandler(error);
    }

    return null;
  }

  public get(url: string, handlerFunction: Function, ErrorHandler?: Function) {
    return this.http.get(this.apiUrl ? this.parseUrl(url) : url, this.headers())
      .subscribe(
        response => this.success(response, handlerFunction),
        error => this.error(error, ErrorHandler)
      );
  }

  public post(url: string, datas: any, handlerFunction: Function, ErrorHandler?: Function) {
    return this.http.post(this.apiUrl ? this.parseUrl(url) : url, datas, this.headers())
      .subscribe(
        response => this.success(response, handlerFunction),
        error => this.error(error, ErrorHandler)
      );
  }

  public delete(url: string, options: any, handlerFunction: Function, ErrorHandler?: Function) {
    return this.http.delete(this.apiUrl ? this.parseUrl(url) : url, options)
      .subscribe(
        response => this.success(response, handlerFunction),
        error => this.error(error, ErrorHandler)
      );
  }

  public getPrependLang(url: string, handlerFunction: Function, ErrorHandler?: Function) {
    url = '/' + this.lang.currentLang + url;
    return this.get(url, handlerFunction, ErrorHandler);
  }

  public postPrependLang(url: string, datas: any, handlerFunction: Function, ErrorHandler?: Function) {
    url = '/' + this.lang.currentLang + url;
    return this.post(url, datas, handlerFunction, ErrorHandler);
  }

  public deletePrependUrl(url: string, options: any, handlerFunction: Function, ErrorHandler?: Function) {
    url = '/' + this.lang.currentLang + url;
    return this.delete(url, options, handlerFunction, ErrorHandler);
  }

}
