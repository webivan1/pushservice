import { Injectable } from "@angular/core";
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { HttpService } from "../http.service";
import { ListService, ISortAttribute } from "../list/index";
import { DetailAppService } from "../apps/detail";

export interface ISearch {
  id: number,
  token: string;
  lang: string;
  os_name: string;
  browser: string;
  device: string;
  referer: string;
  create: string;
  last_active: string;
  tags: string[];
}

interface IList {
  app_id: number;
  created_at: string;
  dev: number;
  get_tags: Array<any>;
  id: number;
  lang: string;
  referer: string;
  os_name: string;
  browser: string;
  device: string;
  last_active: string;
}

@Injectable()
export class SubsService {

  public models: any;

  public sortAttributes: ISortAttribute[] = [
    {
      name: 'ID',
      alias: 'id',
      sort: 'desc'
    },
    {
      name: 'CreateDate',
      alias: 'created_at',
      sort: 'desc'
    },
    {
      name: 'LastActive',
      alias: 'active',
      sort: 'desc'
    }
  ];

  constructor(public list: ListService, public detail: DetailAppService) {
    this.list.sendUrl = `/panel/subs/get/${this.detail.data.id}`;
    this.list.handlerResponse = (response: any) => {
      this.models = new SubsDataSource(<Array<IList>>response.data);
    };
  }

}

export class SubsDataSource extends DataSource<any> {

  constructor(public data: Array<IList> = []) {
    super();
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Array<IList>> {
    return Observable.of(this.data);
  }

  disconnect() {}
}