import { Injectable } from '@angular/core';
import { HttpService } from '../http.service';

export interface IUser {
  id: number;
  name: string;
  email: string;
  status: number;
  created_at: string;
  updated_at: string;
}

@Injectable()
export class UserService {

  public identity: IUser;
  public isLoader: boolean = true;

  constructor(private http: HttpService) {}

  public isGuest(): boolean {
    return !this.identity;
  }

  public getUser() {
    this.http.get('/api/get/user', (response: IUser) => {
      this.isLoader = false;

      if (response.status === 2) {
        this.identity = <IUser>response;
      }
    });
  }

}