import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, PreloadAllModules } from '@angular/router';

declare var window: any;

// Modules
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
// import { Ng2BootstrapModule } from 'ngx-bootstrap';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ChartsModule } from 'ng2-charts';

// Router
import { ROUTES } from './router';

// Главный компонент (wrapper)
import { AppComponent } from './app.component';

// Main service
import { MainService } from './services';

// Components
import * as COMPONENTS from './components';

// Directives
import * as DIRECTIVES from './directives';

// AoT requires an exported function for factories
export function createTranslateLoader(http: HttpClient) {
  let urlSendLanguage = window.location.hostname;
  let url = '';

  if (urlSendLanguage.indexOf('localhost') >= 0) {
    url = 'http://push-service.loc';
  }

  return new TranslateHttpLoader(http, url + '/lang/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,

    COMPONENTS.HomeComponent,
    COMPONENTS.ErrorComponent,
    COMPONENTS.LangComponent,

    COMPONENTS.ProfileWrapComponent,
      COMPONENTS.ChangeUserComponent,
      COMPONENTS.EmailEditComponent,
      COMPONENTS.ChangePasswordComponent,

    COMPONENTS.AppsListComponent,
    COMPONENTS.AppsCreateComponent,
    COMPONENTS.AppDetailComponent,
    COMPONENTS.ConfirmDialogApp,
      COMPONENTS.AppDetailInfoComponent,

      COMPONENTS.SubsListComponent,
      COMPONENTS.SubsSearchComponent,

      COMPONENTS.CreateTemplateComponent,
      COMPONENTS.EditTemplateComponent,
      COMPONENTS.GridTemplateComponent,
      COMPONENTS.ConfirmDialogTemplate,
      COMPONENTS.FormTemplatesDefaultComponent,
      COMPONENTS.FormTemplateContentsComponent,
      COMPONENTS.FormHostDerective,

    COMPONENTS.PaginationComponent,
    COMPONENTS.SortComponent,
    COMPONENTS.ErrorMessagesComponent,

    DIRECTIVES.toggleClassDirective
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpModule,
    BrowserAnimationsModule,
    MaterialModule,
    // router
    RouterModule.forRoot(ROUTES, { useHash: false, preloadingStrategy: PreloadAllModules }),
    // bootstrap
    //Ng2BootstrapModule.forRoot(),
    HttpClientModule,
    ChartsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [ HttpClient ]
      }
    })
  ],
  entryComponents: [
    COMPONENTS.SubsSearchComponent,
    COMPONENTS.FormTemplateContentsComponent,
    COMPONENTS.ConfirmDialogTemplate,
    COMPONENTS.ConfirmDialogApp
  ],
  providers: [ MainService, HttpClient ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
