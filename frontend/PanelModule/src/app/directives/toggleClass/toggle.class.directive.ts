/**
 * Created by maltsev on 30.05.2017.
 */
import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[toggleClass]'
})
export class toggleClassDirective {

  constructor(private el: ElementRef) {}

  @Input('toggleClass') className: string;
  @Input('clickOnly') clickOnly: boolean = false;

  @HostListener('click') onClick() {
    if (!this.clickOnly) {
      this.el.nativeElement.classList.toggle(this.className);
    } else {
      if (!this.el.nativeElement.classList.contains(this.className)) {
        this.el.nativeElement.classList.add(this.className);
      }
    }
  }

}