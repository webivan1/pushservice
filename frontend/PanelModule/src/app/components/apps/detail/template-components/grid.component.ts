import { Component, Inject } from "@angular/core";
import { ListService } from "../../../../services/list/index";
import { TemplateService } from "../../../../services/apps/template.service";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'grid-templates',
  template: `
    <mat-card class="text-right">
        <button mat-raised-button color="primary" routerLink="create">
            {{ 'AddNewTemplate' | translate }}
        </button>        
    </mat-card>
    
    <mat-spinner *ngIf="service.list.loader"></mat-spinner>
    
    <mat-card class="mt-25 pt-0 pb-0 pl-0 pr-0">
        <table class="table" *ngIf="!service.list.loader">
            <thead>
            <tr>
                <th>Uid</th>
                <th>Name</th>
                <th>Token</th>
                <th>Create</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <tr *ngFor="let item of service.models; let key = index">
                <td>{{ item.id }}</td>
                <td>{{ item.name }}</td>
                <td>{{ item.token }}</td>
                <td>{{ item.created_at }}</td>
                <td>
                    <a href="javascript:void(0)" routerLink="update/{{ item.id }}">
                        <mat-icon>edit</mat-icon>
                    </a>
                    <a href="javascript:void(0)" class="text-red-700" (click)="openConfirmDialogDeleteItem(item.id, key)">
                        <mat-icon>delete</mat-icon>
                    </a>
                </td>
            </tr>
            </tbody>
        </table>
        <hr />
        <pagination-list
            [loader]="!service.models.length"
            [disabled]="service.list.loader"
            [length]="service.list.total"
            [pageSize]="service.list.pageSize"
            (onAction)="service.list.pageAction($event)"
        ></pagination-list>
    </mat-card>
  `,
  providers: [ ListService, TemplateService ]
})
export class GridTemplateComponent {

  constructor(
    public service: TemplateService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.service.list.getModels();
  }

  openConfirmDialogDeleteItem(id: number, key: number) {
    let dialogRef = this.dialog.open(ConfirmDialogTemplate, {
      width: '250px',
      data: { id: id }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.service.delete(id, key);
      }
    });
  }
}

@Component({
  selector: 'confirm-dialog-template-delete',
  template: `
      <h4 class="text-center mat-20">{{ 'YouSure' | translate }}</h4>
      <div class="text-center">
          <button [disabled]="data.loader" type="button" (click)="actionBtn(false)" mat-raised-button class="bg-orange text-white">
              {{ 'Cancel' | translate }}
          </button>
          <button [disabled]="data.loader" type="button" (click)="actionBtn(true)" mat-raised-button class="bg-red text-white">
              {{ 'Delete' | translate }}
          </button>
      </div>
  `,
})
export class ConfirmDialogTemplate {

  constructor(
    public dialogRef: MatDialogRef<ConfirmDialogTemplate>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  actionBtn(state: boolean): void {
    this.dialogRef.close(state);
  }

}