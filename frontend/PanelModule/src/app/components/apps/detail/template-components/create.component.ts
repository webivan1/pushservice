import { Component } from '@angular/core';
import { HttpService } from "../../../../services/http.service";
import { DetailAppService } from "../../../../services/apps/detail";
import { MainService } from "../../../../services/main.service";

@Component({
  selector: 'component-create-template',
  template: `
    <h3 class="mb-25">{{ 'AddNewTemplate' | translate }}</h3>
    
    <form-templates-default 
        [loader]="loaderSend"
        (onSubmit)="formHandler($event)"
    ></form-templates-default>
  `
})
export class CreateTemplateComponent {

  public loaderSend: boolean = false;

  constructor(
    private http: HttpService,
    public app: DetailAppService,
    private main: MainService
  ) {}

  ngOnInit() {}

  formHandler(value: any) {
    if (this.loaderSend === true) {
      return false;
    }

    this.loaderSend = true;

    this.http.postPrependLang(`/panel/templates/${this.app.data.id}/create`, value,
      (response: any) => {
        if (response.status == 'ok') {
          this.main.toUrl(`/apps/${this.app.data.id}/template`);
        }

        this.loaderSend = false;
      },
      (error: any) => {
        console.warn(error);
        this.loaderSend = false;
      }
    );
  }

}