import { Component, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';

interface IItemFormContents {
  type: string;
  languages_id: number|null;
  languages_name?: string|null;
  content: string;
}

@Component({
  selector: 'component-create-template-contents',
  template: `
    <mat-card [formGroup]="form" class="mb-25">
        <div class="mb-20">
            <mat-form-field>
                <mat-select [placeholder]="'SelectTypeContent' | translate" formControlName="type">
                    <mat-option *ngFor="let type of types" [value]="type.value">
                        {{ type.name | translate }}
                    </mat-option>
                </mat-select>
            </mat-form-field>
        </div>
        
        <mat-form-field>
            <input 
                type="text" 
                matInput 
                formControlName="languages_name"
                [placeholder]="'EnterChartLanguageName' | translate"
                [matAutocomplete]="langList"
                (keyup)="onKeyupLanguage(form.controls.languages_name.value)"
            />
        </mat-form-field>

        <mat-autocomplete #langList="matAutocomplete">
            <mat-option *ngFor="let lang of filterLanguages | async" [value]="lang.name" (onSelectionChange)="changeValueLang(lang)">
                {{ lang.name }}
            </mat-option>
        </mat-autocomplete>

        <mat-form-field>
            <textarea matInput matTextareaAutosize formControlName="content" [placeholder]="'FieldNameTemplate' | translate"></textarea>
        </mat-form-field>
        
        <div class="text-right">
            <button type="button" class="text-orange" mat-button (click)="close()">
                {{ 'Close' | translate }}
            </button>
            <button type="button" class="text-info" mat-button (click)="submitForm(form.value, form.valid)">
                {{ 'Save' | translate }}
            </button>
        </div>
        
        <error-messages [form]="form" [active]="activeSubmit"></error-messages>
    </mat-card>
  `
})
export class FormTemplateContentsComponent {

  public form: FormGroup;
  public languages: any[] = [];
  public activeSubmit: boolean = false;

  // input data
  public item: IItemFormContents = {
    type: '',
    languages_id: null,
    languages_name: null,
    content: ''
  };

  public filterLanguages: Observable<any[]>;

  // static content types
  public types: any[] = [
    {
      name: 'Heading',
      value: 'heading'
    },
    {
      name: 'Content',
      value: 'content'
    },
  ];

  @Output('onValid') onValid = new EventEmitter();
  @Output('onClose') onClose = new EventEmitter();

  constructor() {}

  ngOnInit() {
    this.filterLanguages = Observable.of(this.languages);
    this.createForm();
  }

  onKeyupLanguage(value: string): void {
    this.filterLanguages = Observable.of(this.filter(value));
  }

  filter(value: string): any[] {
    if (value.length > 1) {
      return this.languages.filter((option: any) => {
        return option.name.toLowerCase().indexOf(value.toLowerCase()) === 0;
      });
    } else {
      return this.languages;
    }
  }

  changeValueLang(item: any) {
    (<FormControl>this.form.controls.languages_id).setValue(item.id);
  }

  createForm() {
    this.form = new FormGroup({
      type: new FormControl(this.item.type, [
        <any>Validators.required
      ]),
      languages_id: new FormControl(this.item.languages_id, [
        <any>Validators.required,
        <any>Validators.pattern(/^\d+$/)
      ]),
      content: new FormControl(this.item.content, [
        <any>Validators.required
      ]),
      languages_name: new FormControl(this.item.languages_name, [
        <any>Validators.required,
        <any>Validators.minLength(1)
      ])
    });
  }

  submitForm(value: IItemFormContents, isValid: boolean) {
    this.activeSubmit = true;

    if (isValid) {
      this.onValid.emit(value);
    }
  }

  close() {
    this.onClose.emit();
  }
}