import {
  Component,
  ViewChild,
  ComponentFactoryResolver,
  Output,
  EventEmitter,
  Input
} from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { HttpService } from "../../../../services/http.service";
import { FormTemplateContentsComponent } from "./form.template.contents.component";
import { FormHostDerective } from "./form.host.derective";

export interface IFormTemplate {
  name: string;
  icon: string;
  image: string|null;
  contents: any[]|null;
}

@Component({
  selector: 'form-templates-default',
  template: `
      <form [formGroup]="form" novalidate (submit)="onSubmit(form.value, form.valid)">

          <mat-form-field>
              <input matInput formControlName="name" [placeholder]="'FieldNameTemplate' | translate" />
          </mat-form-field>

          <mat-form-field>
              <input matInput formControlName="icon" [placeholder]="'FieldIconTemplate' | translate" />
          </mat-form-field>

          <mat-form-field>
              <input matInput formControlName="image" [placeholder]="'FieldImageTemplate' | translate" />
          </mat-form-field>

          <mat-spinner *ngIf="loaderLang"></mat-spinner>

          <div *ngIf="!loaderLang" class="mb-25">
              <mat-card *ngFor="let item of datas.contents; let key = index" class="b-r-0 p-r" [ngClass]="{ 
                  'bg-green-100': item.type === 'heading',
                  'bg-orange-100': item.type === 'content'
              }">
                  <div class="row">
                      <div class="col-md-6">
                          <h4>
                              <b class="text-info">{{ item.languages_name }}</b> |
                              {{ item.type === 'heading' ? 'Heading' : 'Content' | translate }}
                          </h4>
                          {{ item.content }}
                      </div>
                      <div class="col-md-6 text-right">
                          <button (click)="editItem(item, key)" type="button" mat-mini-fab color="primary">
                              <mat-icon>edit</mat-icon>
                          </button>
                          <button (click)="removeItem(key)" type="button" mat-mini-fab class="bg-red text-white">
                              <mat-icon>delete</mat-icon>
                          </button>
                      </div>
                  </div>
              </mat-card>

              <div class="text-right pt-10 pb-10 pr-10 pl-10 text-white mt-15">
                  <button mat-raised-button color="primary" type="button" (click)="addContentForm()">
                      {{ 'AddFormTemplateContents' | translate }}
                  </button>
              </div>

              <div form-host></div>
          </div>

          <error-messages [form]="form" [active]="activeSubmit"></error-messages>

          <mat-card class="text-right">
              <button [disabled]="loaderSubmit" mat-raised-button color="primary">
                  {{ sendButtonName | translate }}
              </button>
          </mat-card>

      </form>
  `
})
export class FormTemplatesDefaultComponent {

  public form: FormGroup;
  public loaderLang: boolean = true;
  public listLanguages: any[] = [];
  public activeSubmit: boolean = false;

  public datas: any = {
    contents: []
  };

  @ViewChild(FormHostDerective) formHost: FormHostDerective;

  @Input('sendUrl') url: string;
  @Input('sendButton') sendButtonName: string = 'AddNewTemplate';
  @Input('loader') loaderSubmit: boolean = false;
  @Input('formInput') formInput: IFormTemplate|null;

  @Output('onSubmit') onSubmitEvent = new EventEmitter();

  constructor(
    private http: HttpService,
    private componentFactoryResolver: ComponentFactoryResolver
  ) {}

  ngOnInit() {
    // create form
    // input datas
    this.createForm();
    this.setDatasInForm();

    // get languages all
    this.http.get('/api/get/langs-list', (response: any) => {
      this.listLanguages = response;
      this.loaderLang = false;
      this.setLanguageName();
    });
  }

  setLanguageName() {
    this.datas.contents.map((item: any) => {
      this.listLanguages.map((lang: any) => {
        if (item.languages_id == lang.id) {
          item['languages_name'] = lang.name;
        }
      })
    });
  }

  createForm() {
    this.form = new FormGroup({
      name: new FormControl('', [
        <any>Validators.required,
        <any>Validators.minLength(3)
      ]),
      icon: new FormControl('', [
        <any>Validators.required
      ]),
      image: new FormControl(''),
      contents: new FormControl([])
    });
  }

  setDatasInForm() {
    if (this.formInput) {
      Object.keys(this.formInput).forEach((key: string) => {
        if (key in this.form.controls) {
          (<FormControl>this.form.controls[key]).setValue(this.formInput[key]);
        }

        if (key in this.datas) {
          this.datas[key] = this.formInput[key];
        }
      });
    }
  }

  // create component form
  addContentForm(item: any = null) {
    let componentFactory = this.componentFactoryResolver.resolveComponentFactory(
      FormTemplateContentsComponent
    );

    let viewContainerRef = this.formHost.viewContainerRef;
    let componentRef = viewContainerRef.createComponent(componentFactory);

    if (item !== null) {
      (<FormTemplateContentsComponent>componentRef.instance).item = item;
    }

    (<FormTemplateContentsComponent>componentRef.instance).languages = this.listLanguages;

    (<FormTemplateContentsComponent>componentRef.instance).onClose.subscribe(value => {
      if (item !== null) {
        this.addValidContent(item);
      }

      componentRef.destroy();
    });

    (<FormTemplateContentsComponent>componentRef.instance).onValid.subscribe(value => {
      this.addValidContent(value);
      componentRef.destroy();
    });
  }

  addValidContent(value: any) {
    this.datas.contents.push(value);
    this.form.controls.contents.setValue(this.datas.contents);
  }

  editItem(item: any, key: number) {
    this.addContentForm(item);
    this.datas.contents.splice(key, 1);
    this.form.controls.contents.setValue(this.datas.contents);
  }

  removeItem(key: number) {
    this.datas.contents.splice(key, 1);
    this.form.controls.contents.setValue(this.datas.contents);
  }

  onSubmit(value: any, isValid: boolean) {
    this.activeSubmit = true;

    if (isValid) {
      this.onSubmitEvent.emit(value);
    }
  }

}