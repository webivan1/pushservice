import { Directive, ViewContainerRef } from "@angular/core";

@Directive({
  selector: '[form-host]'
})
export class FormHostDerective {
  constructor(public viewContainerRef: ViewContainerRef) {}
}
