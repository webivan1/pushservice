import { Component } from '@angular/core';
import { HttpService } from "../../../../services/http.service";
import { DetailAppService } from "../../../../services/apps/detail";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'component-create-template',
  template: `
    <h3 class="mb-25">{{ 'EditTemplate' | translate }}</h3>

    <mat-spinner *ngIf="loader"></mat-spinner>
    
    <form-templates-default 
        *ngIf="!loader"
        sendButton="EditTemplate"
        [loader]="loaderSend"
        [formInput]="data" 
        (onSubmit)="formHandler($event)"
    ></form-templates-default>
    
    <div *ngIf="successMessage" class="alert alert-success mt-25">
        {{ 'SuccessMessageUpdateTemplate' | translate }}
    </div>
  `
})
export class EditTemplateComponent {

  public loader: boolean = true;
  public loaderSend: boolean = false;
  public id: number;
  public data: any;
  public successMessage: boolean = false;

  constructor(
    private http: HttpService,
    public app: DetailAppService,
    public router: ActivatedRoute
  ) {
    this.router.params.subscribe((params: any) => {
      this.id = <number>params.id;
      this.templateData(params.id);
    });
  }

  templateData(id: number) {
    this.http.getPrependLang(`/panel/templates/${this.app.data.id}/update/${id}`, (response: any) => {
      this.data = response;
      this.loader = false;
    });
  }

  ngOnInit() {}

  formHandler(value: any) {
    if (this.loaderSend === true) {
      return false;
    }

    this.loaderSend = true;

    this.http.postPrependLang(`/panel/templates/${this.app.data.id}/update/${this.id}`, value,
      (response: any) => {
        if (response.status == 'ok') {
          this.successMessage = true;
          this.loaderSend = false;

          setTimeout(() => this.successMessage = false, 5000);
        }
      },
      (error: any) => {
        console.warn(error);
        this.loaderSend = false;
      }
    );
  }

}