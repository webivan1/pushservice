import { Component } from "@angular/core";
import { DetailAppService } from "../../../services/apps/detail";

@Component({
  selector: 'detail-info-app',
  template: `
      <h3 class="mb-25">{{ service.data.name }}</h3>

      <mat-card class="mb-15">
          <mat-list>
              <mat-list-item> App key: <b class="ml-15 text-info">{{ service.data.key }}</b> </mat-list-item>
              <mat-list-item> Server key: <b class="ml-15 text-info">{{ service.data.server_key }}</b> </mat-list-item>
          </mat-list>
      </mat-card>

      <div *ngIf="service.data.get_detail_stat_subs.length">
          
          <div class="row">
              <div class="col"></div>
              <div class="col-auto">
                  <mat-select [placeholder]="'DropdownVisibleStatSubs' | translate" name="monthStat" #monthStat>
                      <mat-option *ngFor="let item of service.data.chart.monthAll" [value]="item.value">
                          {{ item.label }}
                      </mat-option>
                  </mat-select>
              </div>
          </div>
          
          <div *ngIf="monthStat.value">
              <canvas
                  baseChart
                  chartType="line"
                  [data]="service.data.chart.datas[monthStat.value]"
                  [labels]="service.data.chart.labels[monthStat.value]"
                  [options]="{responsive: true}">
              </canvas>
          </div>
      </div>
  `
})
export class AppDetailInfoComponent {

  constructor(public service: DetailAppService) {}

}