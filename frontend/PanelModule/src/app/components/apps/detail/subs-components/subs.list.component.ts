import { Component } from "@angular/core";
import { DetailAppService } from "../../../../services/apps/detail";
import { ListService } from "../../../../services/list/index";
import { SubsService, ISearch } from "../../../../services/subs/list";
import { MatDialog } from "@angular/material";
import { SubsSearchComponent } from "./subs.search.component";

@Component({
  selector: 'app-subs-list',
  template: `    
    
      <div [ngClass]="{'loading': data.list.loader}" *ngIf="data.models">
          
          <div class="mb-15">
              <div class="row">
                  <div class="col-md-6">
                      <sort-component
                          activeAlias="created_at"
                          [loader]="data.list.loader"
                          [attributes]="data.sortAttributes"
                          (onSort)="data.list.sortAction($event)"
                      ></sort-component>
                  </div>
                  <div class="col-md-6 text-right">
                      <button [disabled]="data.list.loader" mat-button color="primary" (click)="openSearchForm()">
                          {{ 'Search' | translate }}
                      </button>
                  </div>
              </div>
          </div>
          
          <mat-card class="pt-0 pb-0 pl-0 pr-0">
              <mat-table #table [dataSource]="data.models">
                  <ng-container matColumnDef="id">
                      <mat-header-cell *matHeaderCellDef> Uid </mat-header-cell>
                      <mat-cell *matCellDef="let item"> {{ item.id }} </mat-cell>
                  </ng-container>

                  <ng-container matColumnDef="lang">
                      <mat-header-cell *matHeaderCellDef> {{ 'Lang' | translate }} </mat-header-cell>
                      <mat-cell *matCellDef="let item"> {{ item.lang }} </mat-cell>
                  </ng-container>

                  <ng-container matColumnDef="device">
                      <mat-header-cell *matHeaderCellDef> Device </mat-header-cell>
                      <mat-cell *matCellDef="let item"> {{ item.device }} </mat-cell>
                  </ng-container>

                  <ng-container matColumnDef="browser">
                      <mat-header-cell *matHeaderCellDef> Browser </mat-header-cell>
                      <mat-cell *matCellDef="let item"> {{ item.browser }} ({{ item.os_name }}) </mat-cell>
                  </ng-container>

                  <ng-container matColumnDef="created_at">
                      <mat-header-cell *matHeaderCellDef> Create </mat-header-cell>
                      <mat-cell *matCellDef="let item"> {{ item.created_at }} </mat-cell>
                  </ng-container>

                  <ng-container matColumnDef="last_active">
                      <mat-header-cell *matHeaderCellDef> Active </mat-header-cell>
                      <mat-cell *matCellDef="let item"> {{ item.last_active }} </mat-cell>
                  </ng-container>

                  <ng-container matColumnDef="referer">
                      <mat-header-cell *matHeaderCellDef> Referer </mat-header-cell>
                      <mat-cell *matCellDef="let item"> {{ item.referer }} </mat-cell>
                  </ng-container>

                  <ng-container matColumnDef="get_tags">
                      <mat-header-cell *matHeaderCellDef> Tags </mat-header-cell>
                      <mat-cell *matCellDef="let item">
                          <span *ngFor="let tag of item.get_tags">{{ tag }}, </span>
                      </mat-cell>
                  </ng-container>

                  <mat-header-row *matHeaderRowDef="displayedColumns"></mat-header-row>
                  <mat-row *matRowDef="let row; columns: displayedColumns;"></mat-row>
              </mat-table>
          </mat-card>
          
      </div>

      <div *ngIf="!data.models && data.list.loader">
          <div class="text-center">
              <mat-spinner></mat-spinner>
          </div>
      </div>

      <pagination-list
          [loader]="!data.models"
          [disabled]="data.list.loader"
          [length]="data.list.total"
          [pageSize]="data.list.pageSize"
          [page]="data.list.currentPage"
          (onAction)="data.list.pageAction($event)"
      ></pagination-list>
  `,
  providers: [ ListService, SubsService ]
})
export class SubsListComponent {

  public displayedColumns: Array<string> = [
    'id', 'lang', 'browser', 'device', 'referer', 'created_at', 'last_active', 'get_tags'
  ];

  constructor(
    public service: DetailAppService,
    public data: SubsService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.data.list.getModels();
  }

  openSearchForm(): void {
    let dialogRef = this.dialog.open(SubsSearchComponent, {
      data: {
        search: this.data.list.search,
        appId: this.service.data.id
      }
    });

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result && 'status' in result && result.status === 'send') {
        console.log('The dialog was closed', result);
        this.data.list.search = <ISearch>result.datas;
        this.data.list.getSearch();
      }
    });
  }

}