import { Component, Inject, ViewChild } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { HttpService } from "../../../../services/http.service";

@Component({
  selector: 'subs-search-form',
  providers: [ HttpService ],
  template: `
      <div class="container-fluid">
          <form #formHtml [formGroup]="form" novalidate (keyup.enter)="noSubmit($event)" (ngSubmit)="submit(form.value, form.valid)">
              <div class="row mb-25">
                  <div class="col-md-4">
                      <mat-form-field>
                          <input
                              matInput
                              [disabled]="loader"
                              [placeholder]="'LabelSearchSubFromId' | translate"
                              formControlName="id"
                          />
                      </mat-form-field>
                  </div>
                  <div class="col-md-4">
                      <mat-form-field>
                          <input
                              matInput
                              [disabled]="loader"
                              [placeholder]="'LabelSearchSubFromToken' | translate"
                              formControlName="token"
                          />
                      </mat-form-field>
                  </div>
                  <div class="col-md-4">
                      <mat-form-field>
                          <input
                              matInput
                              [disabled]="loader"
                              [placeholder]="'LabelSearchSubFromLang' | translate"
                              formControlName="lang"
                          />
                      </mat-form-field>
                  </div>
              </div>

              <div class="row mb-25">
                  <div class="col-md-4">
                      <mat-form-field>
                          <input
                              matInput
                              [disabled]="loader"
                              [placeholder]="'LabelSearchSubFromDevice' | translate"
                              formControlName="device"
                          />
                      </mat-form-field>
                  </div>
                  <div class="col-md-4">
                      <mat-form-field>
                          <input
                              matInput
                              [disabled]="loader"
                              [placeholder]="'LabelSearchSubFromBrowser' | translate"
                              formControlName="browser"
                          />
                      </mat-form-field>
                  </div>
                  <div class="col-md-4">
                      <mat-form-field>
                          <input
                              matInput
                              [disabled]="loader"
                              [placeholder]="'LabelSearchSubFromOsName' | translate"
                              formControlName="os_name"
                          />
                      </mat-form-field>
                  </div>
              </div>
              
              <div class="row mb-25">
                  <div class="col-md-4">
                      <mat-form-field>
                          <input
                              matInput
                              [disabled]="loader"
                              [placeholder]="'LabelSearchSubFromUserAgent' | translate"
                              formControlName="userAgent"
                          />
                      </mat-form-field>
                  </div>
                  <div class="col-md-4">
                      <mat-form-field>
                          <input
                              matInput
                              [disabled]="loader"
                              [placeholder]="'LabelSearchSubFromReferer' | translate"
                              formControlName="referer"
                          />
                      </mat-form-field>
                  </div>
                  <div class="col-md-4">
                      <mat-form-field>
                          <input
                              matInput
                              [readonly]="true"
                              [disabled]="loader"
                              [matDatepicker]="activeUser"
                              formControlName="last_active"
                              [placeholder]="'LabelSearchSubFromLastActive' | translate"
                          />
                          <mat-datepicker-toggle matSuffix [for]="activeUser"></mat-datepicker-toggle>
                          <mat-datepicker #activeUser></mat-datepicker>
                      </mat-form-field>
                  </div>
              </div>

              <div class="row mb-25">
                  <div class="col-md-6">
                      <mat-form-field>
                          <input
                              matInput
                              [readonly]="true"
                              [disabled]="loader"
                              [matDatepicker]="picker"
                              formControlName="create"
                              [placeholder]="'LabelSearchSubFromCreate' | translate"
                          />
                          <mat-datepicker-toggle matSuffix [for]="picker"></mat-datepicker-toggle>
                          <mat-datepicker #picker></mat-datepicker>
                      </mat-form-field>
                  </div>
                  <div class="col-md-6">
                      <mat-form-field>
                          <input
                              name="tags"
                              matInput
                              [disabled]="loader"
                              [placeholder]="'LabelSearchSubFromTags' | translate"
                              [matAutocomplete]="auto"
                              #fieldTag
                              (keyup)="loadTags($event, fieldTag)"
                          />
                          <mat-autocomplete #auto="matAutocomplete">
                              <mat-option *ngFor="let tag of filteredTags" [value]="tag.alias" (click)="addTag(tag.alias)">
                                  {{ tag.alias }}
                              </mat-option>
                          </mat-autocomplete>
                      </mat-form-field>

                      <mat-chip-list *ngIf="search.tags.length">
                          <mat-chip *ngFor="let tag of search.tags" (click)="removeTag(tag)">
                              {{ tag }}
                          </mat-chip>
                      </mat-chip-list>
                  </div>
              </div>
              
              <div class="text-right">
                  <button type="button" (click)="reset()" mat-button class="text-orange">{{ 'Reset' | translate }}</button>
                  <button mat-raised-button color="primary">{{ 'Search' | translate }}</button>
              </div>
          </form>
      </div>
  `
})
export class SubsSearchComponent {

  public form: FormGroup;
  public loader: boolean = false;
  public filteredTags: string[] = [];
  public search: any = {
    tags: []
  };
  public appId: number;

  @ViewChild('formHtml') formHtml: any;

  constructor(
    public dialog: MatDialogRef<any>,
    public http: HttpService,
    @Inject(MAT_DIALOG_DATA) public input: any
  ) {
    Object.assign(this.search, input.search);
    this.appId = <number>input.appId;
  }

  ngOnInit() {
    this.formHtml.nativeElement.onkeydown = (e: any) => {
      if (e.keyCode === 13) {
        e.preventDefault();
        return false;
      }
    };

    this.createForm();
  }

  get(name: string) {
    return (name in this.search) ? this.search[name] : '';
  }

  createForm() {
    this.form = new FormGroup({
      id: new FormControl(this.get('id'), [
        <any>Validators.pattern(/^\d+$/),
        <any>Validators.maxLength(11)
      ]),
      token: new FormControl(this.get('token')),
      lang: new FormControl(this.get('lang')),
      userAgent: new FormControl(this.get('userAgent')),
      browser: new FormControl(this.get('browser')),
      device: new FormControl(this.get('device')),
      os_name: new FormControl(this.get('os_name')),
      referer: new FormControl(this.get('referer')),
      last_active: new FormControl(this.dateConvert(this.get('last_active'))),
      create: new FormControl(this.dateConvert(this.get('create')))
    });
  }

  dateConvert(date: Date|string) {
    if (date instanceof Date) {
      return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
    } else if (typeof date === 'string' && date !== '') {
      let arrayDate = <any>(date.split('-'));
      let time = new Date();
      time.setDate(arrayDate[2]);
      time.setMonth(parseInt(arrayDate[1]) - 1);
      time.setFullYear(arrayDate[0]);
      return time;
    } else {
      return null;
    }
  }

  reset() {
    this.search = { tags: [] };
    this.createForm();
    this.submit(this.form.value, this.form.valid);
  }

  submit(value: any, isValid: boolean) {
    if (isValid) {
      this.loader = true;
      value.create = this.dateConvert(value.create);
      value.last_active = this.dateConvert(value.last_active);

      Object.assign(this.search, value);

      this.dialog.close({ status: 'send', datas: this.search });
    }
  }

  noSubmit(form: any) {
    form.preventDefault();
    return false;
  }

  loadTags(keyEvent: any, field: any) {
    if (keyEvent.keyCode === 13) {
      return false;
    }

    if (field.value.length > 1) {
      this.http.postPrependLang(`/panel/tags/search/${this.appId}`, { value: field.value }, (response: string[]) => {
        this.filteredTags = response;
      });
    }
  }

  addTag(tag: string) {
    this.search.tags.indexOf(tag) === -1 ? this.search.tags.push(tag) : null;
  }

  removeTag(tag: string) {
    if (this.search.tags.indexOf(tag) >= 0) {
      this.search.tags.splice(this.search.tags.indexOf(tag), 1);
    }
  }

}