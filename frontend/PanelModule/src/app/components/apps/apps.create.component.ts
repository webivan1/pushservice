import { Component } from "@angular/core";
import { HttpService } from "../../services/http.service";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MainService } from "../../services/main.service";

interface IAppsCreate {
  name: string;
}

@Component({
  selector: 'create-app-component',
  templateUrl: '../../views/apps/create.html'
})
export class AppsCreateComponent {

  public form: FormGroup;
  public sendLoader: boolean = false;
  public submitted: boolean = false;
  public error: string|null;

  constructor(public main: MainService, private http: HttpService) {

  }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.form = new FormGroup({
      name: new FormControl('', [
        <any>Validators.required,
        <any>Validators.minLength(2),
        <any>Validators.maxLength(25),
        <any>Validators.pattern(/^[A-zА-я0-9\s\-\_\.]+$/)
      ])
    });
  }

  submit(value: IAppsCreate, isValid: boolean) {
    this.submitted = true;

    if (isValid) {
      if (this.sendLoader === true) {
        return false;
      }

      this.sendLoader = true;
      this.error = null;

      this.http.postPrependLang('/panel/apps/create', value, (response: any) => {
        if (response.status === 'success') {
          this.main.toUrl(`/apps/${response.id}`);
        } else {
          this.error = 'ErrorCreateApps';
        }
      });
    }
  }

}