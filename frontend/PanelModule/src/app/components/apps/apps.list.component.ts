import { Component } from "@angular/core";
import { ListService } from "../../services/list/index";
import { AppsService, IApps } from "../../services/apps/index";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'apps-list-component',
  templateUrl: '../../views/apps/index.html',
  providers: [ ListService, AppsService ]
})
export class AppsListComponent {

  constructor(public service: AppsService, public translate: TranslateService) {}

  ngOnInit() {
    this.service.list.getModels();
  }

}