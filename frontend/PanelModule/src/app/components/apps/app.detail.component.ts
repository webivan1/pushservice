import { Component, Inject } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { DetailAppService } from "../../services/apps/detail";
import { TranslateService } from "@ngx-translate/core";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { MainService } from "../../services/main.service";

interface IMenu {
  url: string,
  label: string;
  active: boolean;
}

@Component({
  selector: 'detail-app-wrapper',
  templateUrl: '../../views/apps/detail/index.html',
  providers: [ DetailAppService ]
})
export class AppDetailComponent {

  public menu: Array<IMenu> = [
    {
      url: '',
      label: 'LinkAppDetailInfo',
      active: false
    },
    {
      url: '/subs',
      label: 'LinkAppDetailSubs',
      active: false
    },
    {
      url: '/template',
      label: 'LinkAppDetailTemplates',
      active: false
    }
  ];

  constructor(
    private route: ActivatedRoute,
    private main: MainService,
    public service: DetailAppService,
    public translate: TranslateService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {
    this.route.params.subscribe((param: any) => {
      this.service.getModel(param.id);
    });
  }

  checkActive(key: number) {
    this.menu.forEach((item: IMenu) => {
      item.active = false;
      return item;
    });

    this.menu[key].active = true;
  }

  confirmDeleteApp() {
    let dialog = this.dialog.open(ConfirmDialogApp, {
      data: { id: this.service.data.id, loader: this.service.deleteLoader }
    });

    dialog.afterClosed().subscribe(result => {
      if (result === true) {
        this.service.delete(_=> {
          this.translate.get('SuccessDeleteApp').subscribe((response: string) => {
            this.snackBar.open(response, 'App', {
              duration: 3000,
            });
          });

          this.main.toUrl('/apps');
        });
      }
    });
  }

}

@Component({
  selector: 'dialog-delete-app',
  template: `
      <h4 class="text-center mat-20">{{ 'YouSure' | translate }}</h4>
      <div class="text-center">
          <button [disabled]="data.loader" type="button" (click)="actionBtn(false)" mat-raised-button class="bg-orange text-white">
              {{ 'Cancel' | translate }}
          </button>
          <button [disabled]="data.loader" type="button" (click)="actionBtn(true)" mat-raised-button class="bg-red text-white">
              {{ 'Delete' | translate }}
          </button>
      </div>
  `,
})
export class ConfirmDialogApp {

  constructor(
    public dialogRef: MatDialogRef<ConfirmDialogApp>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  actionBtn(state: boolean): void {
    this.dialogRef.close(state);
  }

}