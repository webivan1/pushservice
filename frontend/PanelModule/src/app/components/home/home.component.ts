
import { Component } from "@angular/core";

@Component({
  selector: 'home-component',
  templateUrl: '../../views/home/index.html'
})
export class HomeComponent {}