import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LangService } from '../../services/lang';

@Component({
  selector: 'lang-component',
  template: `
    <router-outlet></router-outlet>
  `
})
export class LangComponent {

  constructor(
    private activeRoute: ActivatedRoute,
    public translate: TranslateService,
    public lang: LangService,
    public route: Router
  ) {

    this.activeRoute.params.subscribe((params: any) => {
      if (this.lang.accessLanguage.indexOf(params.lang) === -1) {
        this.redirectDefaultLang();
      } else {
        this.translate.use(params.lang);
      }
    });

  }

  public redirectDefaultLang() {
    this.route.navigate(['/' + this.translate.getDefaultLang()]);
  }

}