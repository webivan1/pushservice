import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ISortAttribute } from "../../services/list/index";

@Component({
  selector: 'sort-component',
  template: `
      <div *ngIf="attrs.length">
          <button [disabled]="loader" mat-button [matMenuTriggerFor]="sortable" color="primary">
              <span *ngIf="itemActive">
                  {{ itemActive.name | translate }} {{ itemActive.sort }}
              </span>
              <span *ngIf="!itemActive">
                  {{ 'SelectedSorter' | translate }}
              </span>
          </button>

          <mat-menu #sortable="matMenu">
              <button
                  *ngFor="let attr of attrs; let key = index"
                  mat-menu-item
                  [disabled]="loader"
                  [ngClass]="{ 'bg-info text-white': attr === itemActive }"
                  (click)="sortItem(attr)"
              >{{ attr.name | translate }} {{ attr.sort }}</button>
          </mat-menu>
      </div>
  `
})
export class SortComponent {

  @Input('loader') public loader: boolean = false;
  @Input('activeAlias') public activeAlias: string;

  @Input('attributes') set setAttributes(attributes: ISortAttribute[]) {
    this.attrs = attributes;
  }

  public attrs: ISortAttribute[] = [];
  public itemActive: ISortAttribute;

  @Output('onSort') sortAction = new EventEmitter();

  constructor() {}

  sortItem(item: ISortAttribute) {
    item.sort = this.toggleSort(item.sort);
    this.itemActive = item;
    this.sortAction.emit(item);
  }

  toggleSort(sort: string): string {
    return sort === 'desc' ? 'asc' : 'desc';
  }

  ngOnInit() {
    // default active item
    if (this.activeAlias) {
      this.attrs.forEach((value: ISortAttribute) => {
        if (value.alias === this.activeAlias) {
          this.itemActive = value;
        }
      });
    }
  }
}