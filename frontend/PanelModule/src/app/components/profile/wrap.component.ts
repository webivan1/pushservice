import { Component } from "@angular/core";
import { UserService } from "../../services/user/index";

@Component({
  selector: 'profile-wrapper-component',
  templateUrl: '../../views/profile/wrapper.html'
})
export class ProfileWrapComponent {

  public step: number = 0;

  constructor(public user: UserService) { }

  setStep(index: number) {
    this.step = index;
  }

}