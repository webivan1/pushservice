// Change password form

import { Component } from "@angular/core";
import { HttpService } from "../../../services/http.service";
import { UserService } from "../../../services/user/index";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { MatSnackBar } from '@angular/material';
import { TranslateService } from "@ngx-translate/core";

interface IData {
  password: string;
  newPassword: string;
}

@Component({
  selector: 'profile-change-password-form',
  template: `
      <form [formGroup]="form" novalidate (ngSubmit)="submit(form.value, form.valid)">
          <mat-form-field>
              <input
                  matInput
                  [readonly]="loader"
                  [placeholder]="'PanelProfileFormPlaceholderPassword' | translate"
                  name="form[password]"
                  formControlName="password"
              />
          </mat-form-field>
          <div
              class="text-danger text-size-13 mb-15"
              [hidden]="form.controls.password.valid || (form.controls.password.pristine && !submitted)"
              [innerHtml]="'PanelErrorFormPassword' | translate"
          ></div>

          <mat-form-field>
              <input
                  matInput
                  [readonly]="loader"
                  [placeholder]="'PanelProfileFormPlaceholderNewPassword' | translate"
                  name="form[newPassword]"
                  formControlName="newPassword"
              />
          </mat-form-field>
          <div
              class="text-danger text-size-13 mb-15"
              [hidden]="form.controls.newPassword.valid || (form.controls.newPassword.pristine && !submitted)"
              [innerHtml]="'PanelErrorFormPasswordNew' | translate"
          ></div>
          
          <div class="text-right">
              <button mat-button color="primary">{{ 'Save' | translate }}</button>
          </div>
      </form>
  `
})
export class ChangePasswordComponent {

  public form: FormGroup;
  public loader: boolean = false;
  public submitted: boolean = false;

  constructor(
    private http: HttpService,
    public user: UserService,
    private snackBar: MatSnackBar,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.form = new FormGroup({
      password: new FormControl('', [
        <any>Validators.required,
        <any>Validators.minLength(5),
        <any>Validators.pattern(/^[A-z0-9\_\-\.]+$/),
        <any>this.validateNotConfirmed.bind(this, 'newPassword')
      ]),
      newPassword: new FormControl('', [
        <any>Validators.required,
        <any>Validators.minLength(5),
        <any>Validators.pattern(/^[A-z0-9\_\-\.]+$/),
        <any>this.validateNotConfirmed.bind(this, 'password')
      ])
    });
  }

  submit(value: IData, isValid: boolean) {
    this.submitted = true;

    if (isValid) {
      this.loader = true;

      this.http.postPrependLang('/panel/user/change-password', value, (response: any) => {
        this.loader = false;
        this.visibleMessage(response.message);

        if (response.status === 'success') {
          this.submitted = false;
          this.createForm();
        }
      }, (error: any) => {
        this.visibleMessage('ErrorMessageChangePassword');
      });
    }
  }

  validateNotConfirmed(fieldName: string, control: FormControl) {

    if (this.form instanceof FormGroup) {
      const controlField = this.form.controls[fieldName];

      if (control.value != controlField.value) {
        return null;
      }
    }

    return {
      notConfirmed: {
        valid: false
      }
    }
  }

  visibleMessage(message: string) {
    this.translate.get(message).subscribe((response: string) => {
      this.snackBar.open(response, '', {
        duration: 2000,
      });
    });
  }

}