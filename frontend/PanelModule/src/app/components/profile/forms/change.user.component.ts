// Change User name, server-key

import { Component } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { UserService } from "../../../services/user/index";
import { HttpService } from '../../../services/http.service';
import { MatSnackBar } from '@angular/material';
import { TranslateService } from "@ngx-translate/core";

interface IFormData {
  name: string;
}

@Component({
  selector: 'profile-form-user-change-component',
  template: `    
    <form [formGroup]="form" novalidate (ngSubmit)="submit(form.value, form.valid)">
        <mat-form-field>
            <input
                matInput
                [readonly]="sendLoader"
                [placeholder]="'FormLoginPlaceholderName' | translate" 
                name="form[name]" 
                formControlName="name"
                (focus)="setOldName(form.value.name)"
                (blur)="submit(form.value, form.valid)"
            />
        </mat-form-field>
        <div
            class="text-danger text-size-13 mb-15"
            [hidden]="form.controls.name.valid || (form.controls.name.pristine && !submitted)"
            [innerHtml]="'PanelErrorFormUserName' | translate"
        ></div>
    </form>
  `
})
export class ChangeUserComponent {

  public form: FormGroup;
  public sendLoader: boolean = false;
  public submitted: boolean = false;

  private oldName: string = '';

  constructor(
    public user: UserService,
    private http: HttpService,
    private snackBar: MatSnackBar,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.createForm();
  }

  setOldName(name: string) {
    this.oldName = name;
  }

  createForm() {
    this.form = new FormGroup({
      name: new FormControl(this.user.identity.name, [
        <any>Validators.required,
        <any>Validators.minLength(3),
        <any>Validators.maxLength(25),
        <any>Validators.pattern(/^[A-zА-я0-9\s\_\-\.]+$/)
      ])
    });
  }

  submit(data: IFormData, isValid: boolean) {
    if (isValid && data.name !== this.oldName) {
      this.changeUserName(data);
    }
  }

  changeUserName(data: IFormData) {
    if (this.sendLoader === true) {
      return false;
    }

    this.sendLoader = true;

    this.http.postPrependLang('/panel/user/change-name', { name: data.name }, (response: any) => {
      this.sendLoader = false;
      this.user.identity.name = <string>data.name;
      this.visibleMessage('SuccessMessageChangeUsername');
    }, (error: any) => {
      this.visibleMessage('ErrorMessageChangeUsername');
    });
  }

  visibleMessage(message: string) {
    this.translate.get(message).subscribe((response: string) => {
      this.snackBar.open(response, '', {
        duration: 2000,
      });
    });
  }

}
