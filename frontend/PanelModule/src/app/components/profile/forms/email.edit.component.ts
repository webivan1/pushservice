// Edit email

import { Component } from "@angular/core";
import { UserService } from "../../../services/user/index";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { HttpService } from "../../../services/http.service";
import { MatSnackBar } from '@angular/material';
import { TranslateService } from "@ngx-translate/core";

interface IData {
  email: string;
  secretKey: string;
}

@Component({
  selector: 'email-edit-user-profile',
  template: `
      <form [formGroup]="form" novalidate (ngSubmit)="submit(form.value, form.valid)">

          <div *ngIf="!sendCodeKey">
              <div class="row">
                  <div class="col">
                      <mat-form-field>
                          <input
                              matInput
                              [readonly]="loaderSendMail"
                              [placeholder]="'FormLoginPlaceholderEmail' | translate"
                              name="form[email]"
                              formControlName="email"
                          />
                      </mat-form-field>
                  </div>
                  <div class="col-md-auto">
                      <button 
                          [disabled]="loaderSendMail || !isValidEmail(form.controls.email)" 
                          type="button" 
                          mat-raised-button 
                          color="accent"
                          (click)="sendMail(form.controls.email)"
                      >{{ 'ButtonEditEmail' | translate }}</button>
                  </div>
              </div>
              <div
                  class="text-danger text-size-13 mb-15"
                  [hidden]="form.controls.email.valid"
                  [innerHtml]="'PanelErrorFormUserName' | translate"
              ></div>
          </div>
          
          <div *ngIf="sendCodeKey && !loaderSendMail">
              <div class="mb-15">
                  {{ form.controls.email.value }} <span (click)="cancel()" [matTooltip]="'ClearNewEmail | translate'" class="cursor-pointer text-red">
                      <mat-icon>close</mat-icon>
                  </span>
              </div>
              
              <div class="alert alert-primary">
                  {{ 'InfoUserSendMailCode' | translate }}
              </div>

              <mat-form-field>
                  <input
                      matInput
                      [readonly]="loaderSubmit"
                      [placeholder]="'ChangeEmailSecretKey' | translate"
                      name="form[secretKey]"
                      formControlName="secretKey"
                  />
              </mat-form-field>
              
              <div
                  class="text-danger text-size-13 mb-15"
                  [hidden]="form.controls.secretKey.valid"
                  [innerHtml]="'PanelErrorFormUserSecretKey' | translate"
              ></div>
              
              <div class="text-right">
                  <button [disabled]="loaderSubmit" mat-button color="primary">{{ 'Save' | translate }}</button>
              </div>
              
          </div>
      </form>
  `
})
export class EmailEditComponent {

  public form: FormGroup;

  public sendCodeKey: boolean = false;
  public loaderSendMail: boolean = false;
  public loaderSubmit: boolean = false;

  constructor(
    public user: UserService,
    private http: HttpService,
    private snackBar: MatSnackBar,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.form = new FormGroup({
      email: new FormControl(this.user.identity.email, [
        <any>Validators.required,
        <any>Validators.email
      ]),
      secretKey: new FormControl('', [
        <any>Validators.required,
        <any>Validators.minLength(6),
        <any>Validators.maxLength(16)
      ])
    });
  }

  cancel() {
    this.createForm();
    this.sendCodeKey = false;
    this.loaderSendMail = false;
    this.loaderSubmit = false;
  }

  isValidEmail(email: FormControl): boolean {
    return email.valid && this.user.identity.email != email.value;
  }

  sendMail(email: FormControl) {
    if (this.isValidEmail(email)) {
      this.loaderSendMail = true;
      // send
      this.http.postPrependLang('/panel/user/send-code-email', { email: email.value }, (response: any) => {
        this.sendCodeKey = true;
        this.loaderSendMail = false;
        this.visibleMessage('SuccessMessageSendMailSecretKey');
      }, (error: any) => {
        this.visibleMessage('ErrorMessageSendMailSecretKey');
        this.loaderSendMail = false;
        this.createForm();
      });
    }
  }

  submit(value: IData, isValid: boolean) {
    if (isValid) {
      this.loaderSubmit = true;
      this.http.postPrependLang('/panel/user/save-new-email', value, (response: any) => {
        this.user.identity.email = value.email;
        this.sendCodeKey = false;
        this.cancel();
        this.visibleMessage('SuccessMessageChangeEmail');
      }, (error: any) => {
        this.loaderSubmit = false;
        this.visibleMessage('ErrorMessageChangeEmail');
      });
    }
  }

  visibleMessage(message: string) {
    this.translate.get(message).subscribe((response: string) => {
      this.snackBar.open(response, '', {
        duration: 2000,
      });
    });
  }

}