import { Component, Input, Output, EventEmitter } from "@angular/core";
import { PageEvent } from '@angular/material';

@Component({
  selector: 'pagination-list',
  template: `
      <div *ngIf="pageSize < length && length > 0">
          <mat-card *ngIf="loader" class="text-center">
              <mat-progress-bar mode="indeterminate"></mat-progress-bar>
          </mat-card>
          <div [ngClass]="{'loading': disabled}">
              <mat-paginator
                  *ngIf="!loader"
                  [pageIndex]="currentPage"
                  [length]="length"
                  [pageSize]="pageSize"
                  (page)="onPage($event)">
              </mat-paginator>
          </div>
      </div>
  `
})
export class PaginationComponent {

  @Input('length') length: number;
  @Input('pageSize') pageSize: number = 16;
  @Input('loader') loader: boolean = true;
  @Input('disabled') disabled: boolean = false;

  @Input('page') set page(page: number) {
    this.currentPage = page - 1;
  }

  @Output('onAction') eventPageAction = new EventEmitter();

  public currentPage: number = 0;

  onPage(event: PageEvent) {
    this.currentPage = <number>event.pageIndex;
    this.eventPageAction.emit(<number>event.pageIndex + 1);
  }
}