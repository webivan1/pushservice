import { Component, Input, Output, EventEmitter, HostListener } from "@angular/core";
import { FormGroup, FormControl, ValidationErrors } from "@angular/forms";

@Component({
  selector: 'error-messages',
  template: `
    <div *ngIf="active && errors.length > 0" class="mt-15 mb-15">
        <div class="alert alert-danger">
            <div *ngFor="let err of errors">
                <b>{{ err.label }}</b> <span *ngFor="let str of err.invalid">{{ str }} </span>
            </div>
        </div>
    </div>
  `
})
export class ErrorMessagesComponent {

  constructor() {}

  @Input('active') active: boolean = false;
  @Input('form') form: FormGroup;
  @Input('labels') labels: any = {};

  public errors: any[] = [];

  ngOnInit() {
    this.setErrors();
    this.form.valueChanges.subscribe((change: any) => {
      this.setErrors();
    });
  }

  setErrors() {
    if (this.form.valid === false) {
      this.errors = [];

      Object.keys(this.form.controls).forEach(key => {
        const controlErrors: ValidationErrors = this.form.get(key).errors;
        if (controlErrors != null) {
          let error = {
            label: key in this.labels ? this.labels[key] : key,
            invalid: []
          };

          Object.keys(controlErrors).forEach(keyError => {
            error.invalid.push(keyError);
          });

          this.errors.push(error);
        }
      });
    } else {
      this.errors = [];
    }
  }
}