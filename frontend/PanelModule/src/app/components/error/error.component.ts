/**
 * Created by maltsev on 31.05.2017.
 */
import { Component } from '@angular/core';

@Component({
  selector: 'error',
  template: `<h1>Error 404</h1>`
})
export class ErrorComponent {  }