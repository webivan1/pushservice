import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { HttpService } from './services/http.service';
import { MainService } from './services/main.service';
import { LangService } from './services/lang';
import { UserService } from './services/user';

declare var window: any;

@Component({
  selector: 'app-root',
  templateUrl: './views/layout/index.html',
  providers: [ MainService, LangService, HttpService, UserService ]
})
export class AppComponent {

  constructor(
    public main: MainService,
    public translate: TranslateService,
    public lang: LangService,
    public user: UserService,
    private http: HttpService
  ) {
    // fetch all languages
    this.lang.getLangs();

    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang('ru');

    // the lang to use, if the lang isn't available, it will use the current loader to get them
    translate.use('ru');

    // info user
    this.user.getUser();
  }

  logout() {
    this.http.getPrependLang('/logout', (response: any) => {
      window.location.href = '/' + this.translate.currentLang;
    });
  }

}
