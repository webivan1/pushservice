
var gulp = require("gulp");
var browserify = require("browserify");
var source = require('vinyl-source-stream');
var tsify = require("tsify");
var uglify = require('gulp-uglify');
var uglifyify = require('uglifyify');
var buffer = require('vinyl-buffer');
var sourcemaps = require('gulp-sourcemaps');
var paths = {
  pages: ['app/views/*.html']
};

var assetDir = function(dirName) {
  return gulp.dest("../../public/assets/" + dirName);
};

var browserifyConfig = function(debug, filesApp) {
  return browserify({
    basedir: '.',
    debug: debug,
    entries: [ filesApp ],
    cache: {},
    packageCache: {}
  }).plugin(tsify)
};

/**
 * Script push app
 */

gulp.task("copy-html", function () {
  return gulp.src(paths.pages)
    .pipe(gulp.dest("dist"));
});

gulp.task("app", ["copy-html"], function () {
  return browserifyConfig(false, 'app/app.ts')
    .bundle()
    .pipe(source('app.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(uglify())
    .pipe(sourcemaps.write('./'))
    .pipe(assetDir('public/js'));
});

gulp.task("small:app", ["copy-html"], function () {
  return browserifyConfig(true, 'app/app.ts')
    .bundle()
    .pipe(source('app-dev.js'))
    .pipe(assetDir('public/js'));
});

gulp.task('watch:app', function() {
  gulp.watch('./app/**/*.ts', ['small:app']);
});