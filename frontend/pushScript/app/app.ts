
/**
 * @version 1.0.0
 * @author IvanM
 */

import {
  InitConfigure,
  PushService
} from './scripts';

declare var require: any;
declare var window: any;

const axios: any = require('axios');
const DEV = window.location.hostname.match(/\.loc$/) ? true : false;

if (window.location.protocol === 'http:' && DEV === false) {
  console.error("Должен быть https протокол!");
} else if (!('Notification' in window)) {
  console.error("Смените браузер на более новый!");
} else if (!('pushConfigure' in window)) {
  console.error("Не указана конфигурация pushConfigure!");
} else {

  try {

    // Default configurations request
    axios.defaults.baseURL = DEV === true ? window.location.protocol + '//' + window.location.host + '/api' : 'https://erogo.pw/api';
    axios.defaults.headers.common['App-key'] = window.pushConfigure.apiKey;
    axios.defaults.headers.common['Domain-referer'] = window.location.hostname;

    let init = new InitConfigure();

    window.PushService = new PushService(init.firebase());

  } catch (err) {
    console.error(err);
  }

}