
import { PushService } from './push.service';

export class TagService {

  // Token user
  private token: string|boolean;

  constructor(private service: PushService) {
    this.token = <string>this.service.token;

    if (!this.token) {
      throw new Error("Error! Access notification browser");
    }
  }

  getAllTags(callable?: Function) {
    this.service.http.get('/tags/all')
      .then((response: any) => {
        typeof callable === 'function' ? callable(response.data) : null;
      })
      .catch((err: any) => console.error(err));
  }

  getTags(callable?: Function) {
    this.service.http.post('/tags', { token: this.token })
      .then((response: any) => {
        typeof callable === 'function' ? callable(response.data) : null;
      })
      .catch((err: any) => console.error(err));
  }

  setTag(tagName: string, callable?: Function) {
    this.service.http.post('/tags/create', { token: this.token, alias: tagName })
      .then((response: any) => {
        typeof callable === 'function' ? callable(response.data) : null;
      })
      .catch((err: any) => console.error(err));
  }

  setTags(Tags: Array<string>, callable?: Function) {
    this.service.http.post('/tags/set-tags', { token: this.token, tags: Tags })
      .then((response: any) => {
        typeof callable === 'function' ? callable(response.data) : null;
      })
      .catch((err: any) => console.error(err));
  }

  deleteTag(tagName: string, callable?: Function) {
    this.service.http.post('/tags/delete', { token: this.token, alias: tagName })
      .then((response: any) => {
        typeof callable === 'function' ? callable(response.data) : null;
      })
      .catch((err: any) => console.error(err));
  }

  deleteTags(Tags: Array<string>, callable?: Function) {
    this.service.http.post('/tags/delete-tags', { token: this.token, tags: Tags })
      .then((response: any) => {
        typeof callable === 'function' ? callable(response.data) : null;
      })
      .catch((err: any) => console.error(err));
  }

}