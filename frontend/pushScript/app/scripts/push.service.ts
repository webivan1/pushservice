
import { MainVendor } from './main.vendor';
import { UserInterface } from '../config/user.conf';
import { TagService } from './tag.service';
import { DeviceConfig } from "./device.config";

declare var window: any;
// declare var Object: any;

export class PushService extends MainVendor {

  private messaging: any;
  public token: string;
  public eventLoader: boolean = false;
  public interval: any;

  constructor(private firebase: any) {
    super();

    this.messaging = firebase.messaging();

    // handle catch the notification on current page
    this.messaging.onMessage((payload: any) => {
      console.log('Message received. ', payload);

      // register fake ServiceWorker for show notification on mobile devices
      navigator.serviceWorker.register('/messaging-sw.js');

      Notification.requestPermission((permission: any) => {
        if (permission === 'granted') {
          navigator.serviceWorker.ready.then((registration: any) => {
            payload.notification.data = payload.notification;
            registration.showNotification(payload.notification.title, payload.notification);
          }).catch((error: any) => {
            // registration failed :(
            console.log('ServiceWorker registration failed.', error);
          });
        }
      });
    });

    this.interval = setInterval(() => this.eventActiveUser(), 60000 * 10);
  }

  /**
   * Update latsActive user
   *
   * @return void
   */
  eventActiveUser() {
    if (this.eventLoader === true) {
      console.warn('Process event active user');
      return false;
    }

    this.eventLoader = true;

    this.getToken((token: string|boolean) => {
      // Create user
      this.eventLoader = false;
    }, (err: any) => {
      this.eventLoader = false;
      console.error('Error getToken', err);
    });
  }

  /**
   * Token exists?
   *
   * @return void
   */
  getToken(callHandler: Function, errHandler?: Function): void {
    if (this.getStateSubs() !== 'success') {
      callHandler(false);
    }

    this.messaging.getToken()
      .then((token: string) => {
        this.token = token;
        this.setUid(token);

        callHandler(token);
      })
      .catch((error: any) => typeof errHandler === 'function' ? errHandler(error) : null);
  }

  getStateSubs(): string {
    let state = '';

    switch (window.Notification.permission) {
      case 'granted': state = 'success'; break;
      case 'default': state = 'pending'; break;
      default : state = 'rejected'; break;
    }

    return state;
  }

  jsonToQuery(jsonObject: any) {
    Object.keys(jsonObject).map(function(key) {
      return encodeURIComponent(key) + '=' +
        encodeURIComponent(jsonObject[key]);
    }).join('&')
  }

  /**
   * Subscribe
   *
   * @param Function callable
   * @return void
   */
  onSubscribe(callback?: Function, errorHandler?: Function) {
    // запрашиваем разрешение на получение уведомлений
    this.messaging.requestPermission()
      .then(() => {
        // получаем ID устройства
        this.messaging.getToken()
          .then((currentToken: string|null|false) => {

            if (currentToken) {

              this.token = currentToken;

              let params = {
                data: {
                  token: currentToken,
                  lang: window.navigator.language,
                  user_agent: window.navigator.userAgent
                }
              };

              let infoDevice: any = (new DeviceConfig()).init();

              Object.assign(params.data, infoDevice);

              // Create user
              this.http.post('/user/create', params)
                .then((response: any) => {
                  this.setUid(currentToken);
                  typeof callback === 'function' ? callback(currentToken) : null;
                })
                .catch((err: any) => {
                  let errString = 'Нет доступа!';
                  console.warn(errString);
                  typeof errorHandler === 'function' ? errorHandler(1, errString, err) : null;
                });

            } else {
              let errString = 'Не удалось получить токен.';
              console.warn(errString);
              typeof errorHandler === 'function' ? errorHandler(2, errString) : null;
            }
          })
          .catch((err: any) => {
            let errString = 'При получении токена произошла ошибка.';
            console.warn(errString, err);
            typeof errorHandler === 'function' ? errorHandler(3, errString, err) : null;
          });
      })
      .catch((err: any) => {
        let errString = 'Не удалось получить разрешение на показ уведомлений.';
        console.warn(errString, err);
        typeof errorHandler === 'function' ? errorHandler(4, errString, err) : null;
      });
  }

  /**
   * Info user
   *
   * @param Function callable
   * @return void|boolean
   */
  getUserInfo(callable?: Function, errorCall?: Function): void {
    this.getToken((token: string|boolean) => {
      if (!token) {
        console.warn("User is not subscribe!");
        return false;
      }

      this.http.post('user', { token: token })
        .then((response: any) => {
          let data: UserInterface = response.data;
          typeof callable === 'function' ? callable(data) : null;
        })
        .catch((err: any) => {
          typeof errorCall === 'function' ? errorCall(err) : null;
        });
    }, (err: any) => {
      typeof errorCall === 'function' ? errorCall(err) : null;
    });
  }

  /**
   * Tag service
   *
   * @return TagService|boolean
   */
  tag() {
    try {
      if (this.token) {
        return new TagService(this);
      } else {
        throw new Error('Undefined token server');
      }
    } catch (err) {
      console.error(err);
      return false;
    }
  }

}