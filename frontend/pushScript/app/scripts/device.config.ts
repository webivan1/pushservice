
declare var require: any;

const { device } = require('device.js');
const { detect } = require('detect.js');

const detectModel: any = detect.parse(navigator.userAgent);

// create classes document
device.addClasses(document.documentElement);

export class DeviceConfig {

  public data = {
    device: '',
    browser: '',
    os_name: ''
  };

  public detect: any = detectModel;

  init() {
    this.deviceDetect();
    this.browserDetect();
    this.osClient();

    return this.data;
  }

  deviceDetect() {
    let classList = <any>document.documentElement.classList;
    let classArray: string[] = [];

    classList.forEach((className: string) => classArray.push(className));

    this.data.device = classArray.join(',');
  }

  browserDetect() {
    this.data.browser = this.detect.browser.family;
  }

  osClient() {
    this.data.os_name = this.detect.os.family;
  }

}