
import * as firebase from 'firebase';
import { CONFIG, configPushInterface } from '../config/main.conf';

export class InitConfigure {

  private config: configPushInterface;

  constructor() {
    this.config = CONFIG;
  }

  firebase() {
    return firebase.initializeApp(this.config)
  }

}