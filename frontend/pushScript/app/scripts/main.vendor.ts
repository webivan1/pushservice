
declare var localStorage: any;
declare var require: any;

const axios: any = require('axios');

export class MainVendor {

  private saveKeyLocalStorage: string = 'pushServiceUidUser';

  // http request service
  public http: any;

  constructor() {
    this.http = axios;
  }

  getUid() {
    return localStorage.getItem(this.saveKeyLocalStorage);
  }

  setUid(token: string) {
    localStorage.setItem(this.saveKeyLocalStorage, token);
  }

  isUid(currentToken: string) {
    return localStorage.getItem(this.saveKeyLocalStorage) !== currentToken;
  }

}