
declare let XMLHttpRequest: any;
declare let XDomainRequest: any;
declare let Promise: any;

export class HttpService {

  private httpClass: any;
  private http: any;

  constructor() {
    this.httpClass = ("onload" in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest;
    this.http = new this.httpClass();
  }

  get(url: string) {
    this.http.open('GET', url, true);

    return new Promise((resolve, error) => {
      this.http.onload = function() {
        resolve(this); //responseText
      };

      this.http.onerror = function() {
        error(this); // status
      };
    });
  }

  post(url: string) {
    this.http.withCredentials = true;
    this.http.open('POST', url, true);

    return new Promise((resolve, error) => {
      this.http.onload = function() {
        resolve(this); //responseText
      };

      this.http.onerror = function() {
        error(this); // status
      };
    });
  }

}