
export interface TagItemInterfaceFromUser {
  alias: string;
  name: string;
}

export interface UserInterface {
  id?: number|string;
  app_id: string|number;
  token: string;
  lang: string;
  user_agent?: string;
  referer?: string;
  tags?: Array<TagItemInterfaceFromUser>;
  create_at?: string;
  update_at?: string;
}