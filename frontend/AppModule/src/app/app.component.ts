import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { HttpService } from './services/http.service';
import { MainService, MenuInterface } from './services/main.service';
import { LangService } from './services/lang';
import { UserService } from './services/user';

declare var window: any;
declare var PushService: any;

interface CoordsInterface {
  x: number;
  y: number;
  xmove: number;
  ymove: number;
}

@Component({
  selector: 'app-root',
  templateUrl: './views/layout/index.html',
  providers: [ MainService, LangService, HttpService, UserService ]
})
export class AppComponent implements OnInit {

  public menuItems: Array<MenuInterface> = [
    {
      title: 'linkHome',
      href: 'home'
    },
    {
      title: 'linkPrice',
      href: 'pricing',
    },
    {
      title: 'linkDocumentation',
      href: 'docs',
    },
    {
      title: 'linkAbout',
      href: 'about'
    },
    {
      title: 'linkContacts',
      href: 'contacts'
    }
  ];

  public menu: boolean = false;

  private touchMenu: CoordsInterface|null = null;

  public isPush: boolean = false;
  public pushToken: boolean|string = false;
  public successMessage: string|null;
  public errorMessage: string|null;

  constructor(
    public main: MainService,
    public translate: TranslateService,
    public lang: LangService,
    public user: UserService,
    private http: HttpService
  ) {
    // fetch all languages
    this.lang.getLangs();

    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang('ru');

    // the lang to use, if the lang isn't available, it will use the current loader to get them
    translate.use('ru');

    // info user
    this.user.getUser();
  }

  ngOnInit() {
    this.swipeMenu();

    if (typeof PushService === 'object') {
      this.isPush = true;

      PushService.getToken(
        (token: string|boolean) => this.pushToken = token,
        (error: any) => this.errorMessage = error.message
      );

    } else {
      this.isPush = false;
      this.errorMessage = 'Включите пуши';
    }
  }

  toggleMenu() {
    this.menu = !this.menu;
  }

  swipeMenu() {
    var swipe = <HTMLElement>document.querySelector('.js-swipe-menu');

    if (!swipe) {
      return false;
    }

    if ('ontouchstart' in window && this.main.isMobile()) {
      // Запоминаем точку касания
      swipe.ontouchstart = event => {
        this.touchMenu = {
          x: this.getPageX(event.touches),
          y: 0, //this.getPageY(event.touches),
          xmove: this.getPageX(event.touches),
          ymove: 0, //this.getPageY(event.touches),
        };
      };

      swipe.ontouchmove = event => {
        if (this.touchMenu) {
          Object.assign(this.touchMenu, {
            xmove: this.getPageX(event.touches),
            ymove: 0, //this.getPageY(event.touches)
          });

          if (this.touchMenu.y < this.touchMenu.ymove && (this.touchMenu.ymove - this.touchMenu.y) > 40 && this.menu === true) {
            this.menu = false;
            return false;
          }

          if (this.touchMenu.y > this.touchMenu.ymove && (this.touchMenu.y - this.touchMenu.ymove) > 40 && this.menu === false) {
            this.menu = true;
            return false;
          }
        }
      };
    } else {
      /*swipe.onmousewheel = (event: WheelEvent) => {
        this.menu = event.deltaY > 0;
      }*/
    }
  }

  getPageX(element: TouchList): number {
    if (0 in element) {
      return element[0].clientX;
    }

    return 0;
  }

  getPageY(element: TouchList): number {
    if (0 in element) {
      return element[0].clientY;
    }

    return 0;
  }

  initPush() {
    if (PushService.getStateSubs() === 'rejected') {

      this.pushToken = false;
      this.errorMessage = 'Дайте разрешение на пуши.';

    } else if (PushService.getStateSubs() === 'pending') {

      PushService.onSubscribe((token: string) => {
        this.pushToken = token;
        this.successMessage = 'Вы успешно подписались';

        // send test push
        this.sendPush();

      }, (state: number, errString: string, err: any) => {
        if (err) {
          this.errorMessage = err.message;
        } else {
          this.errorMessage = errString;
        }
      });
    }
  }

  sendPush() {
    this.http.postPrependLang('/welcome/push', { token: this.pushToken },
      (response: any) => {
        console.log(response);
      }
    );
  }

}
