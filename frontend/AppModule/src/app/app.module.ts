import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule, PreloadAllModules } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

declare var window: any;

// Modules
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { ModalModule } from "ngx-bootstrap";
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

// Router
import { ROUTES } from './router';

// Main service
import { MainService } from "./services/main.service";

// Главный компонент (wrapper)
import { AppComponent } from './app.component';

// Components
import * as COMPONENTS from './components';

// Directives
import * as DIRECTIVES from './directives';

// AoT requires an exported function for factories
export function createTranslateLoader(http: HttpClient) {
  let urlSendLanguage = window.location.hostname;
  let url = '';

  if (urlSendLanguage.indexOf('localhost') >= 0) {
    url = 'http://push-service.loc';
  }

  return new TranslateHttpLoader(http, url + '/lang/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,

    COMPONENTS.LangComponent,
    COMPONENTS.HomeComponent,
    COMPONENTS.ErrorComponent,
    COMPONENTS.WrapperAuthComponent,
    COMPONENTS.LoginComponent,
    COMPONENTS.RegisterComponent,
    COMPONENTS.RestoreComponent,
    COMPONENTS.ResetComponent,
    COMPONENTS.LangButtonComponent,

    COMPONENTS.ModalComponent,
    COMPONENTS.ModalErrorComponent,
    COMPONENTS.HeadingComponent,
    COMPONENTS.BreadcrumbComponent,
    COMPONENTS.ShareComponent,
    COMPONENTS.FooterPageComponent,
    COMPONENTS.CropComponent,

    DIRECTIVES.toggleClassDirective
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpModule,
    BrowserAnimationsModule,
    MaterialModule,
    ModalModule.forRoot(),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [ HttpClient ]
      }
    }),
    // router
    RouterModule.forRoot(ROUTES, { useHash: false, preloadingStrategy: PreloadAllModules }),
  ],
  providers: [ MainService, HttpClient ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
