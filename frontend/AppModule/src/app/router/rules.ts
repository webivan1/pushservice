import { Routes, RouterModule } from '@angular/router';
import { DataResolver } from '../app.resolver';

import * as ALL_COMPONENTS from '../components';

export const ROUTES: Routes = [
  {
    path: ':lang',
    component: ALL_COMPONENTS.LangComponent,
    children: [
      {
        path: '',
        component: ALL_COMPONENTS.LangComponent
      },
      {
        path: 'home',
        component: ALL_COMPONENTS.HomeComponent
      },
      {
        path: 'auth',
        component: ALL_COMPONENTS.WrapperAuthComponent,
        children: [
          {
            path: '',
            component: ALL_COMPONENTS.LoginComponent
          },
          {
            path: 'register',
            component: ALL_COMPONENTS.RegisterComponent
          },
          {
            path: 'restore',
            component: ALL_COMPONENTS.RestoreComponent
          },
          {
            path: 'restore/:token',
            component: ALL_COMPONENTS.ResetComponent
          }
        ]
      },
      {
        path: '**',
        component: ALL_COMPONENTS.ErrorComponent
      }
    ]
  },
  {
    path: '',
    redirectTo: '/ru',
    pathMatch: 'full'
  }
];