/**
 * Created by maltsev on 02.05.2017.
 */
import { Injectable } from '@angular/core';

export interface BreadCrumbsInterface {
  name: string;
  url: string|null;
}

@Injectable()
export class BreadCrumbsService {

  public breadCrumbs: Array<BreadCrumbsInterface> = [
    {
      name: 'Главная',
      url: '/'
    }
  ];

  add(item: BreadCrumbsInterface) {
    if (this.breadCrumbs.indexOf(item) === -1) {
      this.breadCrumbs.push(item);
    }
  }

}