import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from "@ngx-translate/core";

declare var window;

export interface MenuInterface {
  title: string;
  href: string;
}

@Injectable()
export class MainService {

  constructor(private router: Router, private lang: TranslateService) {

  }

  public getBaseUrl() {
    return this.getBaseHostName() + '/' + (<String>window.location.pathname.replace(/^\//, ''));
  }

  public getBaseHostName() {
    let protocol = window.location.protocol + '//';
    return window.location.hostname.indexOf('localhost') >= 0
      ? protocol + 'mediapronet.loc'
      : protocol + window.location.host;
  }

  public isMobile(): boolean {
    return window.screen.width <= 640;
  }

  public getCurrentYear() {
    let date = new Date();
    return date.getFullYear();
  }

  public replaceLang(lang: string) {
    this.router.navigateByUrl(this.router.url.replace(this.lang.currentLang, lang));
  }

}