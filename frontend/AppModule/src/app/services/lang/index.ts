import { Injectable } from '@angular/core';
import { HttpService } from '../http.service';

export interface ILang {
  id: string;
  name: string;
}

@Injectable()
export class LangService {

  public languages: Array<ILang>;
  public isLoader: boolean = true;

  // list access languages id
  public accessLanguage: Array<string> = ['de', 'en', 'ru'];

  constructor(private http: HttpService) {}

  getLangs() {
    this.http.get('/api/get/languages', (response: Array<ILang>) => {
      this.languages = response;
      this.isLoader = false;
    });
  }

}