import { Injectable } from '@angular/core';
import { paper } from 'paper/dist/paper-core';

class Bacterium {

  public view: any;
  public blob: any;
  public Point: any;
  public Path: any;
  public Group: any;
  public mouseForce: number;
  public mousePoint: any;

  public fitRect: any;
  public circlePath: any;
  public threshold: any;
  public center: any;
  public group: any;
  public controlCircle: any;
  public settings: Array<any> = [];

  constructor(color: string, blob) {
    this.blob = blob;
    this.view = blob.view;
    this.Point = blob.Point;
    this.Path = blob.Path;
    this.Group = blob.Group;

    // adjustable variables
    this.mouseForce = 0.2;

    // other variables
    this.mousePoint = new this.Point(-1000, -1000);

    let radius = Math.min(this.view.size.width, this.view.size.height) / 2 * 0.7;

    this.build(this.view.bounds.center, radius, color);
  }

  build(center: any, radius: any, color: string) {
    let padding = Math.min(this.view.size.width, this.view.size.height) * 0.2;
    let timeScale = 1;
    let maxWidth = this.view.size.width - padding * 2;
    let maxHeight = this.view.size.height - padding * 2;
    let w = maxWidth * timeScale;
    let h = maxHeight * timeScale;

    this.fitRect = new this.Path.Rectangle({
      point: [this.view.size.width / 2 - w / 2, this.view.size.height / 2 - h / 2],
      size: [w, h]
    });

    this.circlePath = new this.Path.Circle(center, radius);

    this.group = new this.Group([this.circlePath]);
    //this.group.strokeColor = color;
    this.group.position = this.view.center;

    this.circlePath.fillColor = color;
    this.circlePath.fullySelected = false;

    // Mausdistanz
    this.threshold = radius * 1.4;
    this.center = center;
    // Elemente hinzufügen
    this.circlePath.flatten(radius * 1.5);
    // wieder zum Kreis machen
    this.circlePath.smooth();
    // einpassen in das fitRect
    this.circlePath.fitBounds(this.fitRect.bounds);

    // control circle erstellen, auf den die einzelnen Punkte später zurückgreifen können
    this.controlCircle = this.circlePath.clone();
    this.controlCircle.fullySelected = false;
    this.controlCircle.visible = false;

    let rotationMultiplicator = radius / 200;

    // Settings pro segment
    this.settings = [];
    for (let i = 0; i < this.circlePath.segments.length; i++) {
      let segment = this.circlePath.segments[i];
      this.settings[i] = {
        relativeX: segment.point.x - this.center.x,
        relativeY: segment.point.y - this.center.y,
        offsetX: rotationMultiplicator,
        offsetY: rotationMultiplicator,
        momentum: new this.Point(0, 0)
      };
    }
  }

  clear() {
    this.circlePath.remove();
    this.fitRect.remove();
  }

  animate(event) {
    this.group.rotate(-0.2, this.view.center);

    for (let i = 0; i < this.circlePath.segments.length; i++) {
      let segment = this.circlePath.segments[i];

      let settings = this.settings[i];
      let controlPoint = new this.Point();
      controlPoint = this.controlCircle.segments[i].point;

      // Avoid the mouse
      let mouseOffset = this.mousePoint.subtract(controlPoint);
      let mouseDistance = this.mousePoint.getDistance(controlPoint);
      let newDistance = 0;

      if (mouseDistance < this.threshold) {
        newDistance = (mouseDistance - this.threshold) * this.mouseForce;
      }

      let newOffset = new this.Point(0, 0);

      if (mouseDistance !== 0) {
        newOffset = new this.Point(mouseOffset.x / mouseDistance * newDistance, mouseOffset.y / mouseDistance * newDistance);
      }

      let newPosition = controlPoint.add(newOffset);

      let distanceToNewPosition = segment.point.subtract(newPosition);

      settings.momentum = settings.momentum.subtract(distanceToNewPosition.divide(6));
      settings.momentum = settings.momentum.multiply(0.6);

      // Add automatic rotation

      let amountX = settings.offsetX;
      let amountY = settings.offsetY;
      let sinus = Math.sin(event.time + i * 2);
      let cos = Math.cos(event.time + i * 2);

      settings.momentum = settings.momentum.add(new this.Point(cos * -amountX, sinus * -amountY));

      // go to the point, now!
      segment.point = segment.point.add(settings.momentum);
    }
  }

  redrawBacterium() {
    let radius = Math.min(this.view.size.width, this.view.size.height) / 2;
    radius = Math.floor(radius * 0.7);

    this.clear();

    new Bacterium("black", this.blob);
  }

  frame() {
    this.view.onFrame = event => this.animate(event);

    let tool = new this.blob.Tool();

    tool.onMouseMove = event => this.mousePoint = event.lastPoint;

    this.view.onResize = event => this.redrawBacterium();
  }
}

@Injectable()
export class CircleService {

  public element: any = null;

  run(color: string) {
    let el = this.element;

    if (!el) {
      return false;
    }

    let blob = new paper.PaperScope();

    blob.setup(el);

    let circle = new Bacterium(color, blob);

    circle.frame();
  }

}