/**
 * Created by maltsev on 31.05.2017.
 */
import { Component, Input } from '@angular/core';
import { MainService } from '../../services/main.service';

@Component({
  selector: 'footer-page',
  templateUrl: '../../views/layout/footer_page.html'
})
export class FooterPageComponent {

  constructor(public main: MainService) {}

}