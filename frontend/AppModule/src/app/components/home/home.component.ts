import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'home-component',
  templateUrl: '../../views/home/index.html'
})
export class HomeComponent implements OnInit {

  constructor(public t: TranslateService) {}

  ngOnInit() {}

}