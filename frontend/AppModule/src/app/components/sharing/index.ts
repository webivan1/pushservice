/**
 * Created by maltsev on 04.05.2017.
 */
import { Component, Input, ViewChild } from '@angular/core';
import { MainService } from '../../services/main.service';
import { HttpService } from '../../services/http.service';

declare var Ya: any;

@Component({
  selector: 'share',
  template: `
    <div class="{{ className }}" #share></div>
  `,
  providers: [ HttpService ]
})
export class ShareComponent {

  constructor(
    private main: MainService,
    private http: HttpService
  ) {}

  @Input('className') className: string = '';

  @ViewChild('share') share: any;

  ngAfterViewInit() {
    let urlSite = this.main.getBaseUrl();

    let share = Ya.share2(this.share.nativeElement, {
      content: {
        url: urlSite
      },
      theme: 'collections,vkontakte,facebook,odnoklassniki,moimir',
      hooks: {
        // Вытаскиваем мета теги
        onready: () => {
          this.http.apiUrl = false;
          this.http.get(urlSite, response => {
            try {
              if (typeof response.metatags === 'object') {
                share.updateContent({
                  title: response.metatags.title,
                  description: response.metatags.description
                });
              }
            } catch(err) {}
          });
        },
      }
    });
  }

}