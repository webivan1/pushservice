import { Component, ViewChild, Input, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { MainService } from '../../services/main.service';
import { HttpService } from '../../services/http.service';
import { LangService } from "../../services/lang/index";

@Component({
  selector: 'modal-component',
  templateUrl: '../../views/layout/modal.html',
  providers: [ HttpService ]
})
export class ModalComponent {

  private styleClass: HTMLElement;
  private hasButtonClass: boolean = false;

  @ViewChild('modalLayout') modal;
  @ViewChild('modalBox') modalBox: ElementRef;

  @Input('heading') title: string = '';

  constructor(
    public router: Router,
    public main: MainService,
    public translate: TranslateService,
    public lang: LangService
  ) {}

  ngOnInit() {}

  onHidden($event: any = null) {
    // redirect to main page
    this.router.navigate(['/' + this.translate.currentLang]);
  }

  ngAfterViewInit() {
    this.modal.show();
  }

}