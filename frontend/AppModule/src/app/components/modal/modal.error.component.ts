import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'modal-error-component',
  templateUrl: '../../views/layout/modal_error.html'
})
export class ModalErrorComponent {

  @ViewChild('modalLayout') modal;

  constructor(
    public router: Router,
    public translate: TranslateService
  ) {}

  onHidden($event: any = null) {
    // redirect to main page
    this.router.navigate(['/' + this.translate.currentLang]);
  }

  onShow() {}

  ngAfterViewInit() {
    this.modal.show();
  }

  backHome(event: any) {
    this.modal.hide();
  }

}