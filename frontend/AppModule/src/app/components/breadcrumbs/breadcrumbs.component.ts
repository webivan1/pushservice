/**
 * Created by maltsev on 02.05.2017.
 */
import { Component, Input } from '@angular/core';
import { BreadCrumbsService } from '../../services/breadcrumbs';

@Component({
  selector: 'breadcrumb',
  templateUrl: '../../views/breadcrumb/index.html'
})
export class BreadcrumbComponent {

  constructor(public service: BreadCrumbsService) {}

  @Input('className') className: string = '';

  ngOnInit() {

  }

}