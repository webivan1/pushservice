
import { Component } from "@angular/core";
import { TranslateService } from '@ngx-translate/core';
import { LangService } from "../../services/lang";
import { MainService } from "../../services/main.service";

@Component({
  selector: 'lang-button-component',
  templateUrl: '../../views/lang/button.html'
})
export class LangButtonComponent {

  constructor(
    public lang: LangService,
    public translate: TranslateService,
    public main: MainService
  ) {}

}