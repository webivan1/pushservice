import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpService } from '../../services/http.service';
import { TranslateService } from '@ngx-translate/core';

import { MainAuthComponent, IMainAuthComponent, Ilogin } from './main.auth.component';

@Component({
  selector: 'login-component',
  templateUrl: '../../views/auth/login.html'
})
export class LoginComponent extends MainAuthComponent implements IMainAuthComponent {

  constructor(
    private http: HttpService,
    public lang: TranslateService
  ) {
    super();
  }

  ngOnInit() {
    // init form !
    this.createForm();
  }

  createForm(): void {
    this.form = new FormGroup({
      email: new FormControl('', [
        <any>Validators.required,
        <any>Validators.email
      ]),
      password: new FormControl('', [
        <any>Validators.required,
        <any>Validators.minLength(5),
        <any>Validators.pattern(/^[A-z0-9\_\-\.]+$/)
      ]),
      remember: new FormControl(true, [])
    });
  }

  signIn(data: Ilogin, isValid: boolean): void {
    if (isValid) {
      this.send(data)
    }
  }

  send(data) {
    // blocked form
    this.sendLoader = true;
    this.error = null;

    this.http.postPrependLang('/login', <Ilogin>data, (response: any) => {
      this.sendLoader = false;

      if ('url' in response || ('status' in response && response.status == 'success')) { // login success
        window.location.href = '/' + this.lang.currentLang + '/panel';
      } else if (response.status == 'fail') {
        this.error = response.message;
      }
    }, (error: any) => {
      this.sendLoader = false;

      // validation error
      if (error.status === 422) {
        this.handlerErrorResponse(error);
      } else {
        console.warn('error', error);
      }
    });
  }

}