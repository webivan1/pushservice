import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpService } from '../../services/http.service';
import { TranslateService } from '@ngx-translate/core';

import { MainAuthComponent, IMainAuthComponent, IRestore } from './main.auth.component';

@Component({
  selector: 'restore-component',
  templateUrl: '../../views/auth/restore.html'
})
export class RestoreComponent extends MainAuthComponent implements IMainAuthComponent {

  constructor(
    private http: HttpService,
    public lang: TranslateService
  ) {
    super();
  }

  ngOnInit() {
    // init form !
    this.createForm();
  }

  createForm(): void {
    this.form = new FormGroup({
      email: new FormControl('', [
        <any>Validators.required,
        <any>Validators.email
      ])
    });
  }

  signIn(data: IRestore, isValid: boolean): void {
    if (isValid) {
      this.send(data)
    }
  }

  send(data) {
    // blocked form
    this.sendLoader = true;
    this.error = null;

    this.http.postPrependLang('/password/email', <IRestore>data, (response: any) => {
      this.sendLoader = false;
      this.success = 'SuccessSendCodeEmail';
    }, (error: any) => {
      this.sendLoader = false;

      // validation error
      if (error.status === 422) {
        this.handlerErrorResponse(error);
      } else {
        console.warn('error', error);
      }
    });
  }

}