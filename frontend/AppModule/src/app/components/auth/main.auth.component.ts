import { FormGroup } from '@angular/forms';
import { TranslateService } from "@ngx-translate/core";

export interface Ilogin {
  email: string;
  password: string;
  remember: boolean;
}

export interface IRegister {
  name: string;
  email: string;
  password: string;
  password_confirmation: string;
}

export interface IRestore {
  email: string;
}

export interface IReset {
  email: string;
  password: string;
  password_confirmation: string;
  token: string;
}

export interface IMainAuthComponent {
  createForm(): void;
  signIn(data: any, isValid: boolean): void;
  send(data: Ilogin): void;
}

export class MainAuthComponent {

  public form: FormGroup;
  public submitted: boolean = false;
  public sendLoader: boolean = false;
  public error: string|null = null;
  public success: string|null = null;

  handlerErrorResponse(error: any): void {
    try {
      let data = JSON.parse(error._body);
      let errString: string = '';

      for (let key in data) {
        errString += `<p> ${key}: `;

        if (typeof data[key] === 'object') {
          for (let i in data[key]) {
            errString += ` ${data[key][i]} <br />`;
          }
        } else {
          errString += ` ${data[key]} <br />`;
        }

        errString = errString.replace(/\<br \/\>$/, '');
        errString += `</p>`;
      }

      this.error = errString;
    } catch (err) {
      console.error(err);
    }
  }

}