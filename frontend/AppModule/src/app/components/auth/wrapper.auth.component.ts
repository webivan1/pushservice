
import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UserService } from "../../services/user/index";
import { TranslateService } from "@ngx-translate/core";

declare var window: any;

@Component({
  selector: 'wrapper-auth',
  templateUrl: '../../views/auth/wrapper.html'
})
export class WrapperAuthComponent {

  constructor(
    private activeRoute: ActivatedRoute,
    private route: Router,
    public user: UserService,
    public lang: TranslateService
  ) {}

  redirectToPanel() {
    window.location.href = `/${this.lang.currentLang}/panel`;
  }

}