import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpService } from '../../services/http.service';
import { TranslateService } from '@ngx-translate/core';

import { MainAuthComponent, IMainAuthComponent, IRegister } from './main.auth.component';

// custom validate range values
export function validateConfirmed(confirmFieldName: string, self: MainAuthComponent|any) {
  return (c: FormControl) => {

    if (self.form && self.form instanceof FormGroup) {
      let field = <FormControl>self.form.controls[confirmFieldName];

      if (field.valid && c.value === field.value) {
        return null;
      }
    }

    return {
      validateConfirmed: {
        valid: false
      }
    };

  };
}

@Component({
  selector: 'register-component',
  templateUrl: '../../views/auth/register.html'
})
export class RegisterComponent extends MainAuthComponent implements IMainAuthComponent {

  constructor(
    private http: HttpService,
    public lang: TranslateService,
  ) {
    super();
  }

  ngOnInit() {
    // init form !
    this.createForm();
  }

  createForm(): void {
    this.form = new FormGroup({
      name: new FormControl('', [
        <any>Validators.required,
        <any>Validators.minLength(3)
      ]),
      email: new FormControl('', [
        <any>Validators.required,
        <any>Validators.email
      ]),
      password: new FormControl('', [
        <any>Validators.required,
        <any>Validators.minLength(5),
        <any>Validators.pattern(/^[A-z0-9\_\-\.]+$/),
      ]),
      password_confirmation: new FormControl('', [
        <any>validateConfirmed('password', this)
      ])
    });
  }

  signIn(data: IRegister, isValid: boolean): void {
    if (isValid) {
      this.send(data)
    }
  }

  send(data): void {
    // blocked form
    this.sendLoader = true;
    this.error = null;

    this.http.postPrependLang('/register', <IRegister>data, (response: any) => {
      this.sendLoader = false;

      if (response.status === 'success') {
        this.success = response.message;
      }
    }, (error: any) => {
      this.sendLoader = false;

      // validation error
      if (error.status === 422) {
        this.handlerErrorResponse(error);
      } else {
        console.warn('error', error);
      }
    });
  }

}