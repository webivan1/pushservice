import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpService } from '../../services/http.service';
import { TranslateService } from '@ngx-translate/core';

import { MainAuthComponent, IMainAuthComponent, IReset } from './main.auth.component';
import { validateConfirmed } from './register.component';

declare var window: any;

@Component({
  selector: 'reset-component',
  templateUrl: '../../views/auth/reset.html'
})
export class ResetComponent extends MainAuthComponent implements IMainAuthComponent {

  private token;
  public loader: boolean = true;
  public errorToken: string|null = null;

  constructor(
    private http: HttpService,
    public lang: TranslateService,
    public route: ActivatedRoute
  ) {
    super();

    this.route.params.subscribe((params: any) => {
      this.token = params.token;

      console.log(params);

      this.http.postPrependLang(`/password/reset/${this.token}${window.location.search}`, {}, (response: any) => {
        this.loader = false;
      }, (error: any) => {
        this.errorToken = 'ErrorTokenResetPassword';
      });

      this.createForm();
    });
  }

  createForm(): void {
    this.form = new FormGroup({
      email: new FormControl('', [
        <any>Validators.required,
        <any>Validators.email
      ]),
      password: new FormControl('', [
        <any>Validators.required,
        <any>Validators.minLength(5),
        <any>Validators.pattern(/^[A-z0-9\_\-\.]+$/)
      ]),
      password_confirmation: new FormControl('', [
        <any>validateConfirmed('password', this)
      ]),
      token: new FormControl(this.token, [
        <any>Validators.required,
        <any>Validators.minLength(10)
      ])
    });
  }

  signIn(data: IReset, isValid: boolean): void {
    if (isValid) {
      this.send(data)
    }
  }

  send(data) {
    // blocked form
    this.sendLoader = true;
    this.error = null;

    this.http.postPrependLang('/password/reset', <IReset>data, (response: any) => {
      this.sendLoader = false;

      if (response.status === 'ok') {
        window.location.href = '/' + this.lang.currentLang + '/panel';
      } else {
        this.error = 'ErrorResetForm';
      }

    }, (error: any) => {
      this.sendLoader = false;

      // validation error
      if (error.status === 422) {
        this.handlerErrorResponse(error);
      } else {
        console.warn('error', error);
      }
    });
  }

}