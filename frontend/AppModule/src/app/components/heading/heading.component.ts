import { Component, Input, ViewChild, ElementRef } from '@angular/core';
// import { CircleService } from '../../services/animation/circle.service';

@Component({
  selector: 'heading',
  templateUrl: '../../views/heading/heading.html',
  // providers: [ CircleService ]
})
export class HeadingComponent {

  @Input('color-circle') colorCircle: string = '#0ccc54';
  @Input('wrap-class') wrapClass: string = '';
  
  @ViewChild('circleSelector') circleSelector: ElementRef;

  constructor(/*public circle: CircleService*/) {}

  ngAfterViewInit() {
    // this.circle.element = this.circleSelector.nativeElement;
    // this.circle.run(this.colorCircle);
  }

}