/**
 * Created by maltsev on 05.06.2017.
 */
import { Component, Input, ElementRef, ViewChild } from '@angular/core';
import { MainService } from '../../services/main.service';

declare var String;

@Component({
  selector: 'crop',
  template: `
    <div #fullTextElement [ngStyle]="{display: !showFullText ? 'none' : 'block'}">
        <ng-content></ng-content>
    </div>
    
    <div *ngIf="!showFullText">
        {{ cropText }}{{ append }} <a href="javascript:void(0)" (click)="toggleText()" class="small link-item c-p">{{ linkName }}</a>
    </div>
  `
})
export class CropComponent {

  public fullText: string = '';
  public cropText: string = '';

  public showFullText: boolean = false;

  constructor(private main: MainService) {}

  @Input('length') length: number = 150;
  @Input('append') append: string = '...';
  @Input('linkName') linkName: string = 'more';
  @Input('mobile') mobileDetect: boolean = false;

  @ViewChild('fullTextElement') fullTextElement: ElementRef;

  ngOnInit() {
    if ((this.main.isMobile() && this.mobileDetect === true) || this.mobileDetect === false) {
      let str = this.fullTextElement.nativeElement.innerText;
      this.fullText = new String(str);

      if (str.length > this.length) {
        this.cropText = str.substring(0, this.length);
      } else {
        this.toggleText();
      }
    } else {
      this.toggleText();
    }
  }

  toggleText() {
    this.showFullText = !this.showFullText;
  }

}