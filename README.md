# README #

Запустить установку
-------------------

```
cd ./backend
php composer.phar self-update
php composer.phar install
```

### Создать файл ./backend/.env ###

Закинуть в него содержимое файла `./backend/.env.exsample`, пример:

```
cd ./backend
cp ./.env.exsample ./env
```

Необходимо прописать конфиг с конфигурацией DB в файле `.env`

Запустить команду для генерации ключа
```
cd ./backend
php artisan key:generate
```

Запустить миграции
```
cd ./backend
php artisan migrate
```