<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => [
        // Module Localization
        'localeSessionRedirect',
        'localizationRedirect',
        'localeViewPath',
        // My handler
        'App\Http\Middleware\SetLocale'
    ]
], function() {
    /** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/

    Route::get('/', 'Controller@index')->name('index');
    Route::get('about', 'Controller@about')->name('about');
    Route::get('home', 'HomeController@index')->name('home');

    Route::post('welcome/push', 'WelcomePushController@index');

//    Auth::routes();

    Route::get('login', function () { abort(404); })->name('auth.login');
    Route::get('register', function () { abort(404); });
    Route::get('password/reset/{token}', function ($token) {
        return redirect(LaravelLocalization::setLocale() . '/auth/restore/' . $token, 301);
    });

    Route::get('logout', 'Auth\LoginController@logout')->name('auth.logout');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('register', 'Auth\RegisterController@register');

    Route::post('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::post('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::get('user/activate/{token}', 'Auth\RegisterController@activate');
});
