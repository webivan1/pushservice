<?php

namespace App\Events;

use App\Components\BaseModel;
use App\Containers\EventWithRelationItem;
use App\Models\RulesInterface;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class EventRelationValidate
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @property BaseModel
     */
    public $model;

    /**
     * @property EventWithRelationItem[]
     */
    public $configRelation = [];

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(RulesInterface $model, array $configRelation)
    {
        $this->model = $model;

        foreach ($configRelation as $item) {
            try {
                $itemObject = new EventWithRelationItem($item);
            } catch (\Exception $e) {
                throw $e;
            }

            if (!property_exists($model, $itemObject->field) || !method_exists($model, 'rules')) {
                continue;
            }

            $this->configRelation[] = $itemObject;
        }
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
