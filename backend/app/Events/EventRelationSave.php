<?php

namespace App\Events;

use App\Components\BaseModel;
use App\Containers\EventWithRelationItem;
use App\Models\RulesInterface;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class EventRelationSave
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Query where
     *
     * @property array
     */
    public $withSearch = [];

    /**
     * @property RulesInterface
     */
    public $model;

    /**
     * @property array
     */
    public $configRelation = [];

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(RulesInterface $model, array $withColumns, array $configRelation)
    {
        $this->model = $model;

        $this->addWithColumns($withColumns)
            ->createConfigRelationObject($configRelation);
    }

    /**
     * Create object relations config
     *
     * @param array $configRelation
     * @return self
     */
    private function createConfigRelationObject(array $configRelation)
    {
        foreach ($configRelation as $item) {
            try {
                $itemObject = new EventWithRelationItem($item);
            } catch (\Exception $e) {
                throw $e;
            }

            if (!property_exists($this->model, $itemObject->field) || !method_exists($this->model, 'rules')) {
                continue;
            }

            $this->configRelation[] = $itemObject;
        }

        return $this;
    }

    /**
     * Add with columns
     *
     * @param array $withColumns
     * @return self
     */
    private function addWithColumns(array $withColumns)
    {
        foreach ($withColumns as $field => $columns) {
            if (property_exists($this->model, $field)) {
                $attrbuteValue = $this->model->{$field};

                if (empty($attrbuteValue) || !is_array($attrbuteValue)) {
                    continue;
                }

                $this->withSearch[$field] = [];

                foreach ($attrbuteValue as &$attr) {
                    foreach ($columns as $column => $fieldName) {
                        if ($this->model->getAttribute($column)) {
                            $attr[$fieldName] = $this->model->getAttribute($column);
                            array_push($this->withSearch[$field], [$fieldName, '=', $attr[$fieldName]]);
                        }
                    }
                }

                $this->model->{$field} = $attrbuteValue;
            }
        }

        return $this;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
