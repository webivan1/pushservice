<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return User::validate($data);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::createUser($data);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        Mail::send('emails.register', ['user' => $user, 'request' => $request], function ($model) use ($user) {
            $model
                ->from(config('app.email'), config('app.name'))
                ->to($user->email, $user->name)
                ->subject(trans('data.SubjectMailRegisterForm'));
        });

        return [
            'status' => 'success',
            'message' => 'RegisterOkSendMailInfo'
        ];
    }

    /**
     * Activete user
     *
     * @param  \Illuminate\Http\Request  $request
     * @param string $token
     * @return \Illuminate\Http\Response
     */
    public function activate(Request $request, $token)
    {
        $data = ['email' => $request->email, 'token' => $token];

        Validator::make($data, [
            'email' => 'required|string|email',
            'token' => 'required|string'
        ])->validate();

        $user = User::where('email', '=', $data['email'])->where('status', '=', 0)->first();

        if (empty($user) || User::getTokenActiveUser($user) !== $token) {
            return redirect()->route('index');
        }

        $user->status = 2;
        $user->save();

        return redirect('/' . app()->getLocale() . '/auth');
    }
}
