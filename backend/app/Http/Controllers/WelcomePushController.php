<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Modules\Api\Http\Components\SendPush;
use Modules\Api\Services\SendPushParamsService;

class WelcomePushController extends BaseController
{
    /**
     * Action index [POST]
     *
     * @Route /welcome/push
     * @return View
     */
    public function index(Request $request)
    {
        SendPushParamsService::getInstance([
            'userId' => 1,
            'apiKey' => '931e9f6bdaed5d77b8a486222cc3f8c4',
            'appKey' => 'aaa-bbb-1234',
            'appId' => 1
        ]);

        return SendPush::send([
            'to' => $request->input('token'),
            'notification' => [
                'title' => 'Спасибо за подписку!',
                'body' => 'Вот так выглядит пуш уведомление',
                'icon' => 'https://peter-gribanov.github.io/serviceworker/Bubble-Nebula.jpg',
                'click_action' => 'https://erogo.pw/ru'
            ]
        ]);
    }
}
