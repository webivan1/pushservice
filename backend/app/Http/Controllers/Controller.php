<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Action index
     *
     * @Route /
     * @return View
     */
    public function index(Request $request)
    {
        return view('welcome');
    }

    /**
     * Action about
     *
     * @Route /about
     * @return View
     */
    public function about()
    {
        return view('about');
    }
}
