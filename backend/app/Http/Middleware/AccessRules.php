<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 22.07.2017
 * Time: 11:43
 */

namespace App\Http\Middleware;

use App\Models\Users;
use Closure;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class AccessRules
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role = null)
    {
        $role = is_null($role) ? Users::DEFAULT_ROLE : $role;

        if (Auth::guest() || Auth::user()->status != 2) {
            abort(403);
        }

        if (!$this->accessUser(\App\User::identity(Auth::user()->getAuthIdentifier())->roleId->role->name, $role)) {
            abort(403);
        }

        return $next($request);
    }

    /**
     * Проверяем доступность роли пользователя
     *
     * @param string $roleUser
     * @param string $roleDoctype
     * @return bool
     */
    private function accessUser($roleUser, $roleDoctype): bool
    {
        $roleRules = $this->allRoles();

        return !isset($roleRules[$roleUser]) || !isset($roleRules[$roleDoctype]) ? false : (
            $roleRules[$roleUser]->value >= $roleRules[$roleDoctype]->value
        );
    }

    /**
     * All roles
     *
     * @return array
     */
    private function allRoles(): array
    {
        return Cache::remember('allRoles', 60 * 24 * 30, function() {
            return \App\Models\Role::groupDoctypeRole();
        });
    }
}