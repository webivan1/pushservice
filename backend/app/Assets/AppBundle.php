<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 14.10.2017
 * Time: 1:52
 */

namespace App\Assets;

use App\Components\BaseAssetBundle;
use App\Helpers\ArrayHelper;

class AppBundle extends BaseAssetBundle
{
    /**
     * Дефолтный урл скриптов
     *
     * @property string
     */
    protected $basePath = '/assets/public/bundle';

    /**
     * Приоритет подключения js скриптов
     *
     * @property array
     */
    private $prioryJs = [
        'inline',
        'polyfills',
        'vendor',
        'main'
    ];

    /**
     * @property array
     */
    public $js = [
        '/assets/public/js/app.js',
//        '/assets/public/bundle/assets/TweenLite.min.js',
//        '/assets/public/bundle/assets/EasePack.min.js',
//        '/assets/public/bundle/assets/Canvas.js'
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->scanJs()->scanCss();
    }

    /**
     * Подключаем динамические скрипты JS
     *
     * @return self
     */
    private function scanJs(): self
    {
        $allScripts = glob('.' . rtrim($this->basePath, '/') . '/*.js');

        if (empty($allScripts)) {
            throw new \ErrorException('Undefined scripts public module');
        }

        $allScripts = array_map(function ($path) {
            $info = pathinfo($path);

            return [
                'name' => explode('.', $info['basename'])[0],
                'value' => rtrim($this->basePath, '/') . '/' . $info['basename']
            ];
        }, $allScripts);

        $mergeScrips = array_merge(
            array_flip($this->prioryJs),
            ArrayHelper::map($allScripts, 'name', 'value')
        );

        array_push($this->js, ...array_values($mergeScrips));

        return $this;
    }

    /**
     * Подключаем динамические скрипты CSS
     *
     * @return bool
     */
    private function scanCss(): bool
    {
        $allScripts = glob('.' . rtrim($this->basePath, '/') . '/*.css');

        array_map(function ($path) {
            $info = pathinfo($path);
            array_push($this->css, rtrim($this->basePath, '/') . '/' . $info['basename']);
            return null;
        }, $allScripts);

        return true;
    }
}