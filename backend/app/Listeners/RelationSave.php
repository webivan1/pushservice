<?php

namespace App\Listeners;

use App\Components\BaseModel;
use App\Events\EventRelationSave;
use App\Helpers\Query;
use Faker\Provider\Base;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RelationSave
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EventRelationSave  $event
     * @return void
     */
    public function handle(EventRelationSave $event)
    {
        foreach ($event->configRelation as $config) {
            if (property_exists($event->model, $config->field)) {
                $this->deleteAllWith(
                    $event->withSearch[$config->field] ?? [],
                    $config->model
                );

                $models = $this->clearModels(
                    $event->model->{$config->field},
                    $config->model
                );

                $this->insertAll(
                    $models,
                    $config->model
                );
            }
        }
    }

    /**
     * Clear items
     *
     * @param array $datas
     * @param BaseModel $model
     * @return array
     */
    private function clearModels(array $datas, BaseModel $model): array
    {
        if (empty($datas)) {
            return [];
        }

        foreach ($datas as &$item) {
            $item = $model->getAttributes(
                $model->setAttributes($item)
            );
        }

        return $datas;
    }

    /**
     * Delete all
     *
     * @param array $whereArray
     * @param BaseModel $model
     * @return void
     */
    private function deleteAllWith(array $whereArray, BaseModel $model)
    {
        if (empty($whereArray)) {
            return false;
        }

        $whereArray = \App\Helpers\ArrayHelper::unique($whereArray);

        $model = $model::select(['*']);

        foreach ($whereArray as $where) {
            $model->where(...$where);
        }

        $model->delete();
    }

    /**
     * Insert all
     *
     * @param array $insertDatas
     * @param BaseModel $model
     * @return void
     */
    private function insertAll(array $insertDatas, BaseModel $model)
    {
        if (empty($insertDatas)) {
            return false;
        }

        $model->insert($insertDatas);
    }
}
