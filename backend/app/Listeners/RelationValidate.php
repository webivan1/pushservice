<?php

namespace App\Listeners;

use App\Components\BaseModel;
use App\Events\EventRelationValidate;
use App\Models\RulesInterface;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Validator;

class RelationValidate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EventRelationValidate  $event
     * @return void
     */
    public function handle(EventRelationValidate $event)
    {
        foreach ($event->configRelation as $item) {
            $this->handleItemConfig(
                $event->model->{$item->field},
                $item->model
            );
        }
    }

    /**
     * Handle item
     *
     * @param array $datas
     * @param BaseModel $model
     */
    private function handleItemConfig(array $datas, RulesInterface $model)
    {
        if (empty($datas)) {
            return false;
        }

        // Валидируем каждый item
        // @error 422
        foreach ($datas as $data) {
            $model->setAttributes($data);
            $model->validate();

            $className = get_class($model);

            $model = new $className;
        }
    }
}
