<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 22.07.2017
 * Time: 11:44
 */

namespace App\Helpers;

class ArrayHelper
{
    public static function unique(array $data)
    {
        return array_map('unserialize', array_unique(array_map('serialize', $data)));
    }

    public static function map($data, $key, $value = null, $assoc = false)
    {
        if (empty($data)) {
            return null;
        }

        $newData = [];

        foreach ($data as $item) {

            if (!is_array($value)) {
                $valueRow = is_null($value) ? $item : (
                    is_object($item) ? $item->{$value} : $item[$value]
                );
            } else {
                $valueRow = array_map(function ($key) use ($item) {
                    return is_object($item) ? $item->{$key} : $item[$key];
                }, $value);
            }

            $keyRow = is_object($item) ? $item->{$key} : $item[$key];

            if ($assoc === true) {
                $newData[$keyRow][] = $valueRow;
            } else {
                $newData[$keyRow] = $valueRow;
            }
        }

        return $newData;
    }

    /**
     * Encodes special characters in an array of strings into HTML entities.
     * Only array values will be encoded by default.
     * If a value is an array, this method will also encode it recursively.
     * Only string values will be encoded.
     * @param array $data data to be encoded
     * @param bool $valuesOnly whether to encode array values only. If false,
     * both the array keys and array values will be encoded.
     * @param string $charset the charset that the data is using. If not set,
     * [[\yii\base\Application::charset]] will be used.
     * @return array the encoded data
     * @see http://www.php.net/manual/en/function.htmlspecialchars.php
     */
    public static function htmlEncode($data, $valuesOnly = true, $charset = null)
    {
        if ($charset === null) {
            $charset = 'UTF-8';
        }
        $d = [];
        foreach ($data as $key => $value) {
            if (!$valuesOnly && is_string($key)) {
                $key = htmlspecialchars($key, ENT_QUOTES | ENT_SUBSTITUTE, $charset);
            }
            if (is_string($value)) {
                $d[$key] = htmlspecialchars($value, ENT_QUOTES | ENT_SUBSTITUTE, $charset);
            } elseif (is_array($value)) {
                $d[$key] = self::htmlEncode($value, $valuesOnly, $charset);
            } else {
                $d[$key] = $value;
            }
        }

        return $d;
    }
}