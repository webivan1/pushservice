<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 12.08.2017
 * Time: 16:04
 */

namespace App\Helpers;

use App\Models\Users;

class Hash
{
    /**
     * Generate api key
     *
     * @param Users $user
     * @return string
     */
    public static function generateApiKey(Users $user): string
    {
        return md5(substr(sha1($user->id) . md5($user->email), 0, 16));
    }

    /**
     * Generate hash sql
     *
     * @param Builder $queryBuilder
     * @return string
     */
    public static function uniqueHashFromQuery($queryBuilder): string
    {
        return md5(Query::getRawSql($queryBuilder));
    }
}