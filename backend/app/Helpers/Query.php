<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 20.09.2017
 * Time: 0:21
 */

namespace App\Helpers;

class Query
{
    /**
     * Raw sql
     *
     * @param Builder $queryBuilder
     * @return string
     */
    public static function getRawSql($queryBuilder): string
    {
        $query = clone $queryBuilder;

        try {
            return sprintf(str_replace('?', '%s', $query->toSql()), ...$query->getBindings());;
        } catch (\Exception $e) {
            return $query->toSql() . ": " . json_encode(
                $query->getBindings()
            );
        }
    }
}