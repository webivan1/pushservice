<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 23.07.2017
 * Time: 14:55
 */

namespace App\Helpers;

class Pjax
{
    private static $name;

    public static function start($name)
    {
        self::$name = $name;
        echo Html::beginTag('div', ['id' => self::$name]);
    }

    public static function end()
    {
        echo Html::endTag('div');
        echo view('script.pjax', ['id' => self::$name]);
    }
}