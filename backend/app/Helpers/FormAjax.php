<?php
/**
 * Created by PhpStorm.
 * User: maltsev
 * Date: 24.07.2017
 * Time: 17:43
 */

namespace App\Helpers;

class FormAjax
{
    private static $attrs;

    public static function start(array $attr = [])
    {
        self::$attrs = $attr;
        echo Html::beginTag('form', $attr);
    }

    public static function end()
    {
        echo Html::endTag('form');
        echo view('script.form-ajax', self::$attrs);
    }
}