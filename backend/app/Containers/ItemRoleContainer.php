<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 22.07.2017
 * Time: 14:13
 */

namespace App\Containers;

use Validator;

class ItemRoleContainer extends Components\CoreContainer
{
    /**
     * @property int
     */
    public $id;

    /**
     * @property int
     */
    public $value;

    /**
     * Validate
     *
     * @param array $data
     * @return Validator
     */
    public function validate(array $data)
    {
        return Validator::make($data, [
            'id' => 'required|int',
            'value' => 'required|int'
        ]);
    }
}