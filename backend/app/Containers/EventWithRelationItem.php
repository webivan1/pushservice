<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 22.09.2017
 * Time: 22:46
 */

namespace App\Containers;

use App\Components\BaseModel;
use Illuminate\Support\Facades\Validator;

class EventWithRelationItem extends Components\CoreContainer
{
    /**
     * @property string
     */
    public $field;

    /**
     * @property BaseModel
     */
    public $model;

    /**
     * Validate
     *
     * @param array $data
     * @return Validator
     */
    public function validate(array $data)
    {
        return Validator::make($data, [
            'field' => 'required|string',
            'model' => 'required|string'
        ]);
    }

    /**
     * Init
     *
     * @return void
     */
    public function init()
    {
        if (!class_exists($this->model)) {
            throw new \Exception("Not found model {$this->model}");
        }

        $modelName = $this->model;

        $this->model = new $modelName;
    }
}