<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 22.07.2017
 * Time: 14:13
 */

namespace App\Containers\Components;

use Exception;
use Illuminate\Support\Facades\Validator;

abstract class CoreContainer
{
    /**
     * Init
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->setAttrs($data);

        if (method_exists($this, 'init')) {
            $this->init();
        }
    }

    /**
     * Set attributes
     *
     * @param array $data
     * @return self
     */
    public function setAttrs(array $data)
    {
        if (method_exists($this, 'validate')) {
            $validator = $this->validate($data);

            if ($validator->fails()) {
                throw new Exception("Error validate ". get_class($this));
            }
        }

        foreach ($data as $key => $value) {
            if (!property_exists($this, $key)) {
                throw new Exception("Undefined property $key - class ". get_class($this));
            }

            $this->{$key} = $value;
        }

        return $this;
    }
}