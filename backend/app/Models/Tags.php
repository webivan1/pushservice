<?php

namespace App\Models;

use App\Components\BaseModel;
use App\Helpers\Html;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\Api\Http\Components\Api;
use Modules\Api\Services\ApiDataService;

class Tags extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tags';

    /**
     * Rules
     *
     * @return array
     */
    public function rulesCreate(): array
    {
        return [
            'alias' => ['required', 'string', 'min:1', 'max:50', 'regex:/^[A-z]+$/'],
            'token' => 'required|string',
            'app_key' => 'required|string',
        ];
    }

    /**
     * Rules create all tags
     *
     * @return array
     */
    public function rulesAllCreate(): array
    {
        return [
            'tags' => 'required|array',
            'tags.*' => 'string',
            'token' => 'required|string',
            'app_key' => 'required|string',
        ];
    }

    /**
     * Create new tag
     *
     * @param Request $request
     * @return array
     */
    public function tagCreateWithSubs(Request $request)
    {
        // Validate datas
        Validator::make($data = [
            'alias' => $request->input('alias'),
            'token' => $request->input('token'),
            'app_key' => (new Api($request))->app_id
        ], $this->rulesCreate())->validate();

        $app = ApiDataService::getInstance()->app;

        // Exist tag or insert
        $tag = self::firstOrCreate([
            'alias' => Html::encode($data['alias']),
            'app_id' => $app->id
        ]);

        // Exist sub or fail (403)
        $sub = Subscribers::where('token', '=', Html::encode($data['token']))->firstOr(['id'], function () {
            abort(403);
        });

        // Exist tag sub or insert
        TagSub::firstOrCreate([
            'tag_id' => $tag->id,
            'sub_id' => $sub->id
        ]);

        return ['status' => 'success'];
    }

    /**
     * Create array tags
     *
     * @param Request $request
     * @return array
     */
    public function tagsCreateWithSubs(Request $request)
    {
        // Validate datas
        Validator::make($data = [
            'tags' => $request->input('tags'),
            'token' => $request->input('token'),
            'app_key' => (new Api($request))->app_id
        ], $this->rulesAllCreate())->validate();

        $data['tags'] = array_map(function ($value) {
            return is_string($value) ? Html::decode($value) : '';
        }, $data['tags']);

        // clear empty value
        $data['tags'] = array_diff($data['tags'], ['']);

        // Exist sub or fail (403)
        $sub = Subscribers::where('token', '=', Html::encode($data['token']))->firstOr(['id'], function () {
            abort(403);
        });

        $app = ApiDataService::getInstance()->app;

        // tags match
        $tagMatch = self::whereIn('alias', $data['tags'])
            ->where('app_id', '=', $app->id)
            ->lists('alias')
            ->toArray();

        if (!empty($tagMatch)) {
            // clear double tags
            $data['tags'] = array_filter($data['tags'], function ($tag) use($tagMatch) {
                return !in_array($tag, $tagMatch);
            });
        }

        if (!empty($data['tags'])) {
            self::insert(array_map(function ($tag) use($app) {
                return ['alias' => $tag, 'app_id' => $app->id];
            }, $data['tags']));

            $tags = self::whereIn('alias', $data['tags'])->where('app_id', '=', $app->id)->get();
            $tagsSubInsert = [];

            foreach ($tags as $tag) {
                $tagsSubInsert[] = [
                    'tag_id' => $tag->id,
                    'sub_id' => $sub->id
                ];
            }

            TagSub::insert($tagsSubInsert);
        }

        return ['status' => 'success'];
    }

    /**
     * Delete tag
     *
     * @param Request $request
     * @return array
     */
    public function removeTag(Request $request)
    {
        // Validate datas
        Validator::make($data = [
            'alias' => $request->input('alias'),
            'token' => $request->input('token'),
            'app_key' => (new Api($request))->app_id
        ], $this->rulesCreate())->validate();

        $app = ApiDataService::getInstance()->app;

        $tag = self::where(function ($query) use ($data, $app) {
            return $query
                ->where('app_id', '=', $app->id)
                ->where('alias', '=', Html::encode($data['alias']));
        })->first();

        if (empty($tag) || $tag->def == 1) {
            abort(403);
        }

        // Exist sub or fail (403)
        $sub = Subscribers::where('token', '=', Html::encode($data['token']))->firstOr(['id'], function() {
            abort(403);
        });

        TagSub::where('sub_id', $sub->id)->where('tag_id', $tag->id)->delete();

        return ['status' => 'success'];
    }

    /**
     * Delete tags
     *
     * @param Request $request
     * @return array
     */
    public function removeTags(Request $request)
    {
        // Validate datas
        Validator::make($data = [
            'tags' => $request->input('tags'),
            'token' => $request->input('token'),
            'app_key' => (new Api($request))->app_id
        ], $this->rulesAllCreate())->validate();

        $app = ApiDataService::getInstance()->app;

        $data['tags'] = array_map(function ($value) {
            return is_string($value) ? Html::decode($value) : '';
        }, $data['tags']);

        // clear empty value
        $data['tags'] = array_diff($data['tags'], ['']);

        // Exist sub or fail (403)
        $sub = Subscribers::where('token', '=', Html::encode($data['token']))->firstOr(['id'], function() {
            abort(403);
        });

        $tags = self::whereIn('alias', $data['tags'])
            ->where('app_id', '=', $app->id)
            ->where('def', '!=', 1)
            ->lists('id')
            ->toArray();

        if (!empty($tags)) {
            self::whereIn('id', $tags)->where('app_id', '=', $app->id)->delete();
            TagSub::whereIn('tag_id', $tags)->delete();
        }

        return ['status' => 'success'];
    }

    /**
     * Get all tags
     *
     * @param Request $request
     * @return array
     */
    public static function allTags(Request $request): array
    {
        $api = new Api($request);

        $allTags = self::from('tags AS t')
            ->select(['t.alias'])
            ->leftJoin('apps AS a', 'a.id', '=', 't.app_id')
            ->whereRaw('a.key = ? OR def = 1', [$api->app_id])
            ->groupBy('t.id')
            ->orderBy('t.alias')
            ->get();

        return !empty($allTags) ? array_column($allTags->toArray(), 'alias') : [];
    }

    /**
     * Вытаскиваем теги одного юзера
     *
     * @param string $token
     * @return array
     */
    public static function tagsSubs($token): array
    {
        $token = Html::encode(is_string($token) ? $token : '');

        $allTags = Subscribers::from('subscribers AS sub')
            ->select('t.alias')
            ->join('tag_sub AS ts', 'ts.sub_id', '=', 'sub.id')
            ->join('tags AS t', 't.id', '=', 'ts.tag_id')
            ->whereRaw('sub.token = ?', [$token])
            ->groupBy('t.id')
            ->orderBy('t.alias')
            ->get();

        return !empty($allTags) ? array_column($allTags->toArray(), 'alias') : [];
    }
}
