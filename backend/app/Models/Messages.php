<?php

namespace App\Models;

use App\Components\BaseModel;

class Messages extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'messages';
}
