<?php

namespace App\Models;

use App\Components\BaseModel;
use Illuminate\Support\Facades\DB;

class TagSub extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tag_sub';

    /**
     * @property array access not doctype column
     */
    protected $guarded = [];

    /**
     * Relation tags
     */
    public function getTags()
    {
        return $this->hasOne(Tags::class, 'tag_sub', 'id');
    }

    /**
     * Add default tags
     *
     * @param int $subId
     * @return void
     */
    public static function createDefaultTags($subId)
    {
        // clear all
        self::where('sub_id', '=', $subId)->delete();

        // default tags
        $alltags = Tags::where('def', '=', 1)->get();

        if (!empty($alltags)) {
            $inserts = [];

            foreach ($alltags as $item) {
                $inserts[] = [
                    'sub_id' => $subId,
                    'tag_id' => $item->id
                ];
            }

            DB::table('tag_sub')->insert($inserts);
        }
    }
}
