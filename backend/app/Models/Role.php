<?php
/**
 * Created by PhpStorm.
 * User: maltsev
 * Date: 21.07.2017
 * Time: 16:29
 */

namespace App\Models;

use Zizaco\Entrust\EntrustRole;
use App\Containers\ItemRoleContainer;

class Role extends EntrustRole
{
    /**
     * Group roles
     *
     * @return array
     */
    public static function groupDoctypeRole(): array
    {
        $data = self::orderBy('parent_id', 'asc')->get();

        $value = count($data);

        // Create admin
        $roles[$data[0]->name] = new ItemRoleContainer([
            'id' => $data[0]->id,
            'value' => $value,
        ]);

        unset($data[0]);

        if (!empty($data)) {
            for ($i = 1; $i <= count($data); $i++) {
                $value--;

                foreach ($roles as $name => $role) {
                    foreach ($data as $item) {
                        if ($item->parent_id == $role->id && !isset($roles[$item->name])) {
                            $roles[$item->name] = new ItemRoleContainer([
                                'id' => $item->id,
                                'value' => $value
                            ]);
                        }
                    }
                }

            }
        }

        return $roles;
    }
}