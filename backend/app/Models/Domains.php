<?php

namespace App\Models;

use App\Components\BaseModel;

class Domains extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'domains';
}
