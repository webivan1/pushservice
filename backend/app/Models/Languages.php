<?php

namespace App\Models;

use App\Components\BaseModel;

class Languages extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'languages';

    const DEFAULT_LANG = 'en';
}
