<?php

namespace App\Models;

use App\Components\BaseModel;

class WordKey extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'word_key';

    /**
     * Relation words
     */
    public function words()
    {
        return $this->hasMany(Words::class, 'word_key_id');
    }
}
