<?php

namespace App\Models;

use App\Components\BaseModel;

class RoleUser extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'role_user';

    /**
     * Relation role
     */
    public function role()
    {
        return $this->belongsTo('App\Models\Role', 'role_id');
    }
}
