<?php

namespace App\Models;

use App\Components\BaseModel;

class PasswordResets extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'password_resets';
}
