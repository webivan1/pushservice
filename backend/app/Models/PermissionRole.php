<?php

namespace App\Models;

use App\Components\BaseModel;

class PermissionRole extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'permission_role';
}
