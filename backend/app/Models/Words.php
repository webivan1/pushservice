<?php

namespace App\Models;

use App\Components\BaseModel;

class Words extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'words';

    /**
     * @property array access not doctype column
     */
    protected $guarded = [];
}
