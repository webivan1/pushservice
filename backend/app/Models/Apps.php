<?php

namespace App\Models;

use App\Components\BaseModel;
use Illuminate\Support\Facades\Auth;

class Apps extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'apps';

    /**
     * Проверяем принадлежит ли app id данному юзеру
     *
     * @param int $id
     * @return bool
     */
    public static function accessAppId(int $id): bool
    {
        $app = self::whereRaw('user_id = ? AND id = ?', [
            Auth::id(), $id
        ])->first();

        return !empty($app);
    }
}
