<?php

namespace App\Models;

use App\Components\BaseModel;
use Illuminate\Support\Facades\Validator;

class TemplateContent extends BaseModel implements RulesInterface
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'template_content';

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return array_merge([
            'id' => 'integer',
            'type' => 'required|string',
            'languages_id' => 'required|integer',
            'content' => 'required',
            'template_id' => 'integer'
        ], parent::rules());
    }

    /**
     * @inheritdoc
     */
    public function validate()
    {
        $validator = Validator::make($this->getAttributes(), $this->rules())->validate();
    }

    /**
     * Relation with lang
     */
    public function lang()
    {
        return $this->hasOne(Languages::class, 'id', 'languages_id')->select([
            'value', 'id'
        ]);
    }
}
