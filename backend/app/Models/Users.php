<?php

namespace App\Models;

use App\Components\BaseModel;
use Illuminate\Contracts\Auth\CanResetPassword;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class Users extends BaseModel
{
    use EntrustUserTrait;

    const DEFAULT_ROLE = 'user';

    /**
     * @property array access not doctype column
     */
    protected $guarded = [];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * Relation role
     */
    public function roleId()
    {
        return $this->hasOne('App\Models\RoleUser', 'user_id');
    }
}
