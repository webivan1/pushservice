<?php
/**
 * Created by PhpStorm.
 * User: maltsev
 * Date: 29.09.2017
 * Time: 15:49
 */

namespace App\Models;

interface RulesInterface
{
    /**
     * Rules validate attributes
     *
     * @return array
     */
    public function rules(): array;

    /**
     * Validation attributes
     *
     * @return bool|void
     */
    public function validate();
}