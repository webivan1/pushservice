<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 19.09.2017
 * Time: 22:38
 */

namespace App\Components;

use App\Helpers\Hash;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Cache;

class QueryBuilder extends Builder
{
    /**
     * Alias table name
     *
     * @property string
     */
    public $alias;

    /**
     * Cache duration
     *
     * @property int
     */
    public $cache = 0; // minute

    /**
     * Set alias name
     *
     * @param string $name
     * @return self;
     */
    public function alias(string $name)
    {
        $this->alias = $name;

        $this->from .= " AS {$name}";

        return $this;
    }

    /**
     * Set cache duration
     *
     * @param int $duration
     * @return self;
     */
    public function cache(int $duration)
    {
        $this->cache = $duration;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function first($columns = ['*'])
    {
        if ($this->cache > 0) {
            $queryBuild = clone $this;

            return Cache::remember(Hash::generateApiKey($queryBuild), $this->cache, function () use ($columns) {
                return parent::first($columns);
            });
        } else {
            return parent::first($columns);
        }
    }

    /**
     * @inheritdoc
     */
    public function get($columns = ['*'])
    {
        if ($this->cache > 0) {
            $queryBuild = clone $this;

            return Cache::remember(Hash::uniqueHashFromQuery($queryBuild), $this->cache, function () use ($columns) {
                return parent::get($columns);
            });
        } else {
            return parent::get($columns);
        }
    }
}