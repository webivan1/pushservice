<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 16.09.2017
 * Time: 17:33
 */

namespace App\Components;

use Illuminate\Validation\Validator;

interface SearchInterface
{
    /**
     * Search by params
     *
     * @return array
     */
    public function params(): array;

    /**
     * Validate query params
     *
     * @return array
     */
    public function rule(): Validator;
}