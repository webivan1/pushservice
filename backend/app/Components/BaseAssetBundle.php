<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 14.10.2017
 * Time: 1:52
 */

namespace App\Components;

use App\Helpers\Html;

abstract class BaseAssetBundle
{
    /**
     * Список js скриптов
     *
     * @property array
     */
    protected $js = [];

    /**
     * Список css скриптов
     *
     * @property array
     */
    protected $css = [];

    /**
     * @init
     */
    protected function __construct()
    {
        $this->init();
    }

    /**
     * Запускаем до генерации скриптов
     *
     * @return void
     */
    protected function init()
    {
    }

    /**
     * Регистрируем bundle
     *
     * @return BaseAssetBundle
     */
    public static function register(): BaseAssetBundle
    {
        return new static();
    }

    /**
     * Выводим все js скрипты
     *
     * @return string
     */
    public function generateJsScripts()
    {
        return $this->packJs();
    }

    /**
     * Выводим все css скрипты
     *
     * @return string
     */
    public function generateCssScripts()
    {
        return $this->packCss();
    }

    /**
     * Выводим все скрипты
     *
     * @return string
     */
    public function generateAllScripts()
    {
        return implode("\n", [
            $this->packCss(),
            $this->packJs()
        ]);
    }

    /**
     * Генерим js теги из массива
     *
     * @return string
     */
    private function packJs()
    {
        $out = [];

        if (!empty($this->js)) {
            foreach ($this->js as $path) {
                $out[] = Html::jsFile($path);
            }
        }

        return implode("\n", $out);
    }

    /**
     * Генерим css теги из массива
     *
     * @return string
     */
    private function packCss()
    {
        $out = [];

        if (!empty($this->css)) {
            foreach ($this->css as $path) {
                $out[] = Html::cssFile($path);
            }
        }

        return implode("\n", $out);
    }
}