<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 23.07.2017
 * Time: 21:01
 */

namespace App\Components;

use App\Helpers\Html;

class Sort
{
    const SORT_ASC = 'asc';
    const SORT_DESC = 'desc';

    /**
     * Get параметр для сортировки
     *
     * @property string
     */
    public $pageVar = 'sort';

    /**
     * Специальный разделитель (между индексом и типом сортировки) в значении сортировки
     *
     * @property string
     */
    public $r = '.';

    /**
     * Сортировка поумолчанию
     *
     * @property string
     */
    public $defaultOrder = 'asc';

    /**
     * Теккущий индекс
     *
     * @property string
     */
    private $indexName;

    /**
     * Текущий тип сортировки
     *
     * @property string
     */
    private $orderSort;

    /**
     * Параметры
     *
     * @property array
     */
    private $attrs;

    /**
     * Принимаем параметры для работы класса
     *
        'attributes' => [
            'id' => [
                'asc' => [
                    ['id', Sort::SORT_ASC]
                ],
                'desc' => [
                    ['id', Sort::SORT_DESC]
                ]
            ]
        ],
        'defaultOrder' => 'id.asc'
     *
     * @param array $attributes
     */
    public function __construct(array $attributes)
    {
        $this->attrs = $attributes;
    }

    /**
     * Возвращаем текущий параметр сортировки
     *
     * @return array
     */
    public function sortBy(): array
    {
        try {
            return $this->getParamSort();
        } catch (\Exception $e) {
            return [];
        }
    }

    /**
     * Создаем ссылку для сортировки
     *
     * @param string $name
     * @param string $text
     * @param array $attrs
     * @return string
     */
    public function link($name, $text, array $attrs = [])
    {
        if (!isset($this->attrs['attributes'][$name])) {
            return $text;
        }

        $queryString = explode('?', $_SERVER['REQUEST_URI'])[1] ?? '';
        $params = [];

        parse_str($queryString, $params);

        // remove pagination
        if (isset($params['page'])) {
            unset($params['page']);
        }

        $orderParam = $this->defaultOrder;

        // check link
        if ($name === $this->indexName) {
            $arrayOnlyOrder = array_diff([self::SORT_ASC, self::SORT_DESC], [$this->orderSort]);
            $orderParam = array_shift($arrayOnlyOrder);
            $attrs['class'] = ($attrs['class'] ?? '') . ' active ' . $this->orderSort;
        }

        $params['sort'] = $name . $this->r . $orderParam;

        return Html::tag('a', $text, array_merge($attrs, [
            'href' => explode('?', $_SERVER['REQUEST_URI'])[0] . '?' . http_build_query($params)
        ]));
    }

    /**
     * Возвращаем значение из Get
     *
     * @return string
     */
    public function getString()
    {
        return Html::encode(filter_input(INPUT_GET, $this->pageVar, FILTER_SANITIZE_STRING));
    }

    /**
     * Возвращаем массив с параметрами из $this->attrs
     *
     * @param string $str
     * @return array|bool
     */
    private function dataOrderByString($str)
    {
        list($indexName, $order) = explode($this->r, $str);

        if (
            isset($indexName, $order) &&
            isset($this->attrs['attributes'][$indexName]) &&
            isset($this->attrs['attributes'][$indexName][$order])
        ) {
            $this->indexName = $indexName;
            $this->orderSort = $order;
            return $this->attrs['attributes'][$indexName][$order];
        }

        return false;
    }

    /**
     * Проверяем наличие Get или берем параметр поумолчанию defaultOrder
     *
     * @return array
     */
    private function getParamSort(): array
    {
        if ($getParamSort = $this->getString()) {
            if ($result = $this->dataOrderByString($getParamSort)) {
                return $result;
            }
        }

        if (isset($this->attrs['defaultOrder'])) {
            if ($result = $this->dataOrderByString($this->attrs['defaultOrder'])) {
                return $result;
            }
        }

        return [];
    }
}