<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 27.07.2017
 * Time: 0:44
 */

namespace App\Components;

use Illuminate\Database\Eloquent\Model;
use League\Flysystem\Exception;

class BaseModel extends Model
{
    /**
     * @property bool
     */
    public $isNewRecord = true;

    /**
     * @property array access not doctype column
     */
    protected $guarded = [];

    /**
     * With rules
     *
     * @property array
     */
    private $rules = [];

    /**
     * Get a new query builder instance for the connection.
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function newBaseQueryBuilder()
    {
        $connection = $this->getConnection();

        return new QueryBuilder(
            $connection, $connection->getQueryGrammar(), $connection->getPostProcessor()
        );
    }

    /**
     * Сохранение данных
     *
     * @param array $options
     * @return bool
     */
    public function save(array $options = [])
    {
        if ($this->getAttribute($this->primaryKey)) {
            $this->isNewRecord = false;
        }

        if (!$this->beforeSave()) {
            return false;
        }

        if ($result = parent::save($options)) {
            $this->afterSave();
        }

        return $result;
    }

    /**
     * Удаление
     *
     * @return bool
     */
    public function delete()
    {
        if (!$this->beforeDelete()) {
            return false;
        }

        if ($result = parent::delete()) {
            $this->afterDelete();
        }

        return $result;
    }

    /**
     * Before save
     *
     * @return bool
     */
    public function beforeSave(): bool
    {
        return true;
    }

    /**
     * After save
     *
     * @return void
     */
    public function afterSave()
    {
    }

    /**
     * Before delete
     *
     * @return bool
     */
    public function beforeDelete(): bool
    {
        return true;
    }

    /**
     * After delete
     *
     * @return void
     */
    public function afterDelete()
    {
    }

    /**
     * Set all attributes
     *
     * @param array $data
     * @return void
     */
    public function setAttributes(array $data)
    {
        $keyRules = array_keys($this->rules());

        foreach ($data as $key => $value) {
            if (!empty($keyRules) && !in_array($key, $keyRules)) {
                continue;
            }

            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            } else {
                $this->setAttribute($key, $value);
            }
        }
    }

    /**
     * Get all attributes
     *
     * @return array
     */
    public function getAllAttributes(): array
    {
        $defaultAttrs = $this->getAttributes();

        $publicProperties = new \ReflectionClass($this);
        $props = $publicProperties->getProperties(\ReflectionProperty::IS_PUBLIC);

        if (!empty($props)) {
            foreach ($props as $prop) {
                if ($prop->class === static::class) {
                    $defaultAttrs[$prop->name] = $this->{$prop->name};
                }
            }
        }

        return $defaultAttrs;
    }

    /**
     * Rules validation
     *
     * @return array
     */
    protected function rules(): array
    {
        return $this->rules;
    }

    /**
     * Add rule
     *
     * @param string $key
     * @param array|string $rule
     * @return BaseModel
     */
    public function addRule(string $key, $rule)
    {
        $this->rules[$key] = $rule;

        return $this;
    }

    /**
     * Remove rule
     *
     * @param string $key
     * @return BaseModel
     */
    public function removeRule(string $key)
    {
        if (array_key_exists($key, $this->rules)) {
            unset($this->rules[$key]);
        }

        return $this;
    }
}