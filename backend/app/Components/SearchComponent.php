<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 16.09.2017
 * Time: 17:27
 */

namespace App\Components;

use App\Helpers\ArrayHelper;
use App\Helpers\Html;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Validator;

class SearchComponent
{
    /**
     * @property Builder
     */
    private $query;

    /**
     * @property SearchInterface
     */
    private $search;

    /**
     * @param Builder $query
     * @param SearchInterface $search
     */
    public function __construct(Builder &$query, SearchInterface $search)
    {
        $this->query = &$query;
        $this->search = $search;

        $this->withQueryParams(
            $this->validAttributes()
        );
    }

    /**
     * All valid attributes
     *
     * @return array
     */
    private function validAttributes(): array
    {
        $validator = $this->search->rule();
        $allAttributes = $this->search->toArray();

        if ($validator->fails()) {
            $errors = $validator->errors();

            foreach ($allAttributes as $attr => $value) {
                if ($errors->has($attr)) {
                    unset($allAttributes[$attr]);
                }
            }
        }

        return $allAttributes;
    }

    /**
     * With
     *
     * @param array $attrs
     */
    private function withQueryParams(array $attrs = [])
    {
        $paramsConfig = $this->search->params();

        if (empty($attrs)) {
            return null;
        }

        foreach ($attrs as $key => $value) {
            if (array_key_exists($key, $paramsConfig) && is_callable($paramsConfig[$key])) {
                if (is_array($value)) {
                    $value = ArrayHelper::htmlEncode($value);
                } else {
                    $value = Html::encode($value);
                }

                if ((is_array($value) && empty($value)) || $value == '') {
                    continue;
                }

                call_user_func_array($paramsConfig[$key], [&$this->query, $value]);
            }
        }
    }
}