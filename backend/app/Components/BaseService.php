<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 12.08.2017
 * Time: 23:51
 */

namespace App\Components;

use Exception;
use Illuminate\Support\Facades\Validator;

class BaseService
{
    /**
     * Хранилище данных
     *
     * @property array
     */
    protected $attr = [];

    /**
     * @property array<BaseService>
     */
    private static $instance = [];

    /**
     * Init
     *
     * @param array
     */
    protected function __construct(array $data = [])
    {
        $this->inputAttr($data);
    }

    /**
     * Set datas
     *
     * @param array
     */
    protected function inputAttr(array $data = [])
    {
        $publicProperty = (new \ReflectionObject($this))->getProperties(\ReflectionProperty::IS_PUBLIC);

        if (count($publicProperty) > 0) {
            foreach ($publicProperty as $property) {
                if (array_key_exists($property->name, $data)) {
                    $this->{$property->name} = $data[$property->name];
                    unset($data[$property->name]);
                }
            }

            if (!empty($data)) {
                // throw new Exception("Trash datas: " . json_encode($data), 400);
            }
        } else {
            $this->attr = $data;
        }
    }

    /**
     * @close
     */
    private function __clone()
    {
        // TODO: Implement __clone() method.
    }

    /**
     * @close
     */
    private function __wakeup()
    {
        // TODO: Implement __wakeup() method.
    }

    /**
     * magic getter
     *
     * @param string
     * @throws Exception
     * @return any
     */
    public function __get($name)
    {
        if (isset($this->attr[$name])) {
            return $this->attr[$name];
        } else {
            throw new Exception("Undefined attribute $name", 400);
        }
    }

    /**
     * Singleton pattern
     *
     * @param array $data
     * @return self
     */
    public static function getInstance(array $data = []): self
    {
        $className = get_called_class();

        if (!array_key_exists($className, self::$instance)) {
            self::$instance[$className] = new static($data);
        }

        return self::$instance[$className];
    }

    /**
     * Validate attributes
     *
     * @param array $attr
     * @param array $rules
     * @throws Exception
     * @return self
     */
    public function setAttrsIsValid(array $attr, array $rules)
    {
        $attr = array_intersect_key($attr, $rules);

        $validator = Validator::make($attr, $rules);

        if ($validator->fails()) {
            throw new Exception("Is not valid: " . $validator->errors()->first());
        }

        return $this->setAttrs($attr);
    }

    /**
     * Set attributes
     *
     * @param array $attr
     * @return self
     */
    public function setAttrs(array $attr)
    {
        $this->inputAttr($attr);
        return $this;
    }

    /**
     * All attribs
     *
     * @return array
     */
    public function toArray(): array
    {
        $publicProperty = (new \ReflectionObject($this))->getProperties(\ReflectionProperty::IS_PUBLIC);

        if (!empty($publicProperty)) {
            $attributes = [];

            if (!empty($publicProperty)) {
                foreach ($publicProperty as $property) {
                    if ($this->{$property->name}) {
                        $attributes[$property->name] = $this->{$property->name};
                    }
                }
            }

            return $attributes;
        }

        return $this->attr;
    }

    /**
     * Realod class
     *
     * @param array $data
     * @return self
     */
    public static function reload(array $data = [])
    {
        return new static($data);
    }
}