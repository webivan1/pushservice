<?php

namespace App;

use App\Models\Users as BaseModel;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cache;
use App\Models\Role;

class User extends BaseModel implements Authenticatable, CanResetPasswordContract
{
    use AuthenticableTrait, CanResetPassword, Notifiable;

    private static $identity;

    protected $fillable = ['name', 'email', 'password'];

    protected $hidden = [
        'remember_token',
    ];

    /**
     * Validation
     *
     * @param array $data
     * @return Validator
     */
    public static function validate(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:50',
            'email' => 'required|string|email|max:70|unique:users',
            'password' => 'required|string|min:5|confirmed',
        ], [
            'email.unique' => trans('data.errFormRegisterEmailUnique'),
            'name' => trans('data.errFormRegisterName'),
            'email' => trans('data.errFormRegisterEmail'),
            'password' => trans('data.errFormRegisterPassword'),
            'password.min' => trans('data.errFormRegisterPassMin', ['count' => 5])
        ]);
    }

    /**
     * Create new user
     *
     * @param array $data
     * @return User
     */
    public static function createUser(array $data)
    {
        $user = self::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'status' => 1
        ]);

        $role = Role::where('name', self::DEFAULT_ROLE)->first();

        $user->attachRole($role);

        return $user;
    }

    /**
     * Generate token activate user
     *
     * @param User $user
     * @return string
     */
    public static function getTokenActiveUser(User $user)
    {
        return md5($user->id . $user->email . $user->password . 'a--zZ_rT');
    }

    /**
     * Activated user
     *
     * @param string $token
     * @param string $email
     * @return bool
     */
    public static function activeUser($token, $email): bool
    {
        $user = self::where('email', '=', $email)
            ->where('status', '=', 1)
            ->first();

        if (empty($user) || self::getTokenActiveUser($user) !== $token) {
            return false;
        }

        $user->update(['status' => 2]);

        return true;
    }

    /**
     * users with [roleId, role]
     *
     * @param int $id
     * @return User
     */
    public static function identity(int $id)
    {
        if (!self::$identity) {
            self::$identity = self::where('id', '=', $id)
                ->select(['id', 'name', 'email', 'status'])
                ->with([
                    'roleId' => function ($model) {
                        return $model->with([
                            'role' => function ($model) {
                                return $model->select(['name', 'id']);
                            }
                        ]);
                    }
                ])
                ->first();
        }

        return self::$identity;
    }

    /**
     * Access token
     *
     * @param string $email
     * @param string $token
     * @throws \Exception
     * @return bool
     */
    public static function accessToken($email, $token): bool
    {
        $validate = Validator::make(['email' => $email, 'token' => $token], [
            'email' => 'required|string|email',
            'token' => 'required|string'
        ]);

        if ($validate->fails()) {
            throw new \Exception('ErrorValidateDatas');
        }

        $user = self::where('email', '=', $email)->where('remember_token', '=', $token)->first();

        if (empty($user) || $user->status != 2) {
            throw new \Exception('ErrorUserNotActive');
        }

        return true;
    }
}
