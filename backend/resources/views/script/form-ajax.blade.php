<script type="text/javascript">

  var FormAjax = function(id, isNewRecord, redirectUrl) {
    this.selector = '#' + id;
    this.htmlSelector = $(document.querySelector(this.selector));
    this.isNewRecord = isNewRecord;
    this.redirectUrl = redirectUrl;

    this.init();
  };

  FormAjax.prototype = {

    init: function() {
      this.events();
    },

    events: function() {
      var self = this;

      this.htmlSelector.off('submit').on('submit', function(e) {
        e.preventDefault();
        var url = $(this).attr('action');
        self.fetchAjax(url, $(this).serialize());
        return false;
      });
    },

    fetchAjax: function(url, data) {

      this.htmlSelector.find('[valid-name]').each(function() {
        var elem = $(this);
        elem.removeClass('has-error');
        elem.find('.help-block').remove();
      });

      $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: url,
        data: data ? data : {},
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(response) {
          if (response.status === 'success') {
            if (this.isNewRecord === 1) {
              window.location.href = this.redirectUrl;
            } else {
              window.location.reload();
            }
          }

          return false;
        }.bind(this),
        error: function(err, stat) {
          this.showErrors(err.responseJSON);
        }.bind(this)
      });
    },

    showErrors: function(errors) {
      for (var name in errors) {
        var messageError = errors[name].join(', ');
        var container = this.htmlSelector.find('[valid-name="' + name + '"]');
        if (container.length) {
          container.addClass('has-error');
          container.append('<span class="help-block">' + messageError + '</span>');
        }
      }
    },

  };

  new FormAjax('{{ $id }}', {{ $isNewRecord ? 1 : 0 }}, '{{ $listUrl }}');

</script>