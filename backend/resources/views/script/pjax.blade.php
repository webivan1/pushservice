<script type="text/javascript">

    var GridClient = function(id) {
      this.selector = '#' + id;
      this.htmlSelector = $(document.querySelector(this.selector));

      this.init();
    };

    GridClient.prototype = {

      init: function() {
        this.events();
      },

      events: function() {
        var self = this;

        this.find('a[href]:not(.not-pjax)').off('click').on('click', function(e) {
          e.preventDefault();
          var url = $(this).attr('href');
          self.fetchAjax(url);
          return false;
        });

        $('.form-ajax-grid').off('submit').on('submit', function(e) {
          e.preventDefault();
          var url = $(this).attr('action');
          self.fetchAjax(url, 'POST', $(this).serialize());
          return false;
        });
      },

      update: function() {
        this.fetchAjax(window.location.pathname);
      },

      fetchAjax: function(url, type, data) {
        $.ajax({
          type: type ? type : 'get',
          url: url,
          data: data ? data : {},
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          success: function(response) {
            var html = $(response).find(this.selector).html();
            this.htmlSelector.html(html);

            // init
            new GridClient(this.selector.replace('#', ''));

            return false;
          }.bind(this)
        });
      },

      find: function(selector) {
        return this.htmlSelector.find(selector);
      }

    };

    new GridClient('{{ $id }}');

</script>