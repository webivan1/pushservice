<?php

    $url = $request->getSchemeAndHttpHost()
        . '/'
        . app()->getLocale()
        . '/user/activate/' . $user->getTokenActiveUser($user)
        . '?email=' . $user->email;

?>

<h3>{{ trans('data.HelloNewUser', ['name' => $user->name]) }}</h3>
<h4>{{ trans('data.SuccessRegisterSite') }}</h4>
<p>{% trans('data.ActiveMessageAcc', ['url' => $url]) %}</p>