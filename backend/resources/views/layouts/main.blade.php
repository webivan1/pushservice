<?php

use App\Assets\AppBundle;

$bundle = AppBundle::register();

?>

<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <title>@yield('title', 'Push Service')</title>
    <meta name="description" content="@yield('description', 'Push Service')" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <?= $bundle->generateCssScripts() ?>

    <base href="/" />
</head>
<body>

<app-root>@yield('content')</app-root>

<script type="text/javascript">
  window.pushConfigure = {
    apiKey: 'aaa-bbb-1234'
  };
</script>

<?= $bundle->generateJsScripts() ?>

</body>
</html>
