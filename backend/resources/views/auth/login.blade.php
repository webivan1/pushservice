@extends('layouts.main')

@section('content')
<div class="modal-body">
    <form method="POST" action="{{ route('login') }}" class="js-form-auth" novalidate>
        {{ csrf_field() }}

        <div class="form-group">
            <label for="email" class="control-label">E-Mail Address</label>
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
        </div>

        <div class="form-group">
            <label for="password" class="control-label">Password</label>
            <input id="password" type="password" class="form-control" name="password" required>
        </div>

        <div class="form-group">
            <label class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" name="remember" {{ old('remember') ? 'checked' : '' }} />
                <span class="custom-control-indicator"></span>
                <span class="custom-control-description">Remember Me</span>
            </label>
        </div>

        <div class="js-error"></div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">
                Login
            </button>

            <a class="btn btn-link" href="javascript:void(0);" onclick="new $scope.render(this, '{{ route("password.request") }}', 'response').run()">
                Forgot Your Password?
            </a>
        </div>
    </form>
</div>

<!--div class="modal-footer">
    <a class="btn btn-link" href="javascript:void(0);" onclick="new $scope.render(this, '{{ route("register") }}', 'response').run()">
        @lang('data.RegisterTextLink')
    </a>
</div>

<script type="text/javascript">
    var form = new Components.FormAuthComponent();
    form.submitForm('.js-form-auth', null, '{{ route("panel.index") }}');
</script-->
@endsection