<div class="modal-body">
    <form class="js-form-auth" method="POST" action="{{ route('register') }}" novalidate>
        {{ csrf_field() }}

        <div class="form-group">
            <label for="name" class="control-label">Name</label>
            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
        </div>

        <div class="form-group">
            <label for="email" class="control-label">E-Mail Address</label>
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="control-label">Password</label>
            <input id="password" type="password" class="form-control" name="password" required>
        </div>

        <div class="form-group">
            <label for="password-confirm" class="control-label">Confirm Password</label>
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
        </div>

        <div class="js-error"></div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">
                Register
            </button>

            <a class="btn btn-link" href="javascript:void(0);" onclick="new $scope.render(this, '{{ route("login") }}', 'response').run()">
                Есть аккаунт? Войти
            </a>
        </div>

    </form>
</div>

<script type="text/javascript">
  var form = new Components.FormAuthComponent();
  form.submitForm('.js-form-auth', null, '{{ route("panel.index") }}');
</script>
