<div class="modal-body">
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <form class="js-form-auth" method="POST" action="{{ route('password.email') }}" novalidate>
        {{ csrf_field() }}

        <div class="form-group">
            <label for="email" class="control-label">E-Mail Address</label>
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
        </div>

        <div class="js-error"></div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">
                Send Password Reset Link
            </button>
        </div>
    </form>
</div>

<div class="modal-footer">
    <a class="btn btn-link" href="javascript:void(0);" onclick="new $scope.render(this, '{{ route("login") }}', 'response').run()">
        Вспомнили пароль? Войти
    </a>
</div>

<script type="text/javascript">
  var form = new Components.FormAuthComponent();
  form.submitForm('.js-form-auth', null, '{{ route("password.email") }}');
</script>
