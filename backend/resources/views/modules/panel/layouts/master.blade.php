<?php

use Modules\Panel\Assets\AppBundle;

$bundle = AppBundle::register();

?>

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

        <title>Module Panel</title>

        <?= $bundle->generateCssScripts() ?>

        <base href="/" />
    </head>
    <body>
        <app-root></app-root>

        <?= $bundle->generateJsScripts() ?>
    </body>
</html>
