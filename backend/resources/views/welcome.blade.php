
@extends('layouts.main')

@section('title') @lang('data.MetaTitleWelcome') @stop
@section('description') @lang('data.MetaDescriptionWelcome') @stop

@section('content')
    @parent

    <div class="container-fluid white-bg">
        <div class="container container-content">
            <h1>@lang('data.Hello')</h1>
        </div>
    </div>
@endsection