<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();

        Schema::create('subscribers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('app_id');
            $table->string('token', 200);
            $table->string('lang', 5);
            $table->string('user_agent', 255);
            $table->string('referer', 100);
            $table->integer('dev', false, 0);
            $table->timestamps();

            $table->index('app_id', 'app_id');
            $table->index('token', 'token');
            $table->index('lang', 'lang');
        });

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subscribers');
    }
}
