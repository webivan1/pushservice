<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMessages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();

        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('success', false, 0);
            $table->integer('fails', false, 0);
            $table->integer('app_id');
            $table->text('push_message');

            $table->timestamps();

            $table->index('app_id', 'app_id');
        });

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('messages');
    }
}
