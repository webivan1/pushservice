<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\Role;

class CreateRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $admin = new Role();
        $admin->name = 'admin';
        $admin->display_name = 'Admin';
        $admin->description = 'Администратор';
        $admin->save();

        $moderator = new Role();
        $moderator->name = 'moderator';
        $moderator->display_name = 'Moderator';
        $moderator->description = 'Модератор';
        $moderator->parent_id = $admin->id;
        $moderator->save();

        $partner = new Role();
        $partner->name = 'partner';
        $partner->display_name = 'Partner';
        $partner->description = 'Партнер';
        $partner->parent_id = $moderator->id;
        $partner->save();

        $clientVip = new Role();
        $clientVip->name = 'vip';
        $clientVip->display_name = 'VIP';
        $clientVip->description = 'VIP';
        $clientVip->parent_id = $partner->id;
        $clientVip->save();

        $client = new Role();
        $client->name = 'client';
        $client->display_name = 'Client';
        $client->description = 'Клиент';
        $client->parent_id = $partner->id;
        $client->save();

        $user = new Role();
        $user->name = 'user';
        $user->display_name = 'User';
        $user->description = 'Пользователь';
        $user->parent_id = $client->id;
        $user->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
