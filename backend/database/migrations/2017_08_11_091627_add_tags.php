<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTags extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();

        Schema::create('tags', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('app_id');
            $table->string('alias', 50);
            $table->integer('def', false, 0);
            $table->timestamps();

            $table->index('app_id', 'app_id');
            $table->index('alias', 'alias');

            $table->unique(['app_id', 'alias'], 'app_alias');
        });

        Schema::create('tag_sub', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('sub_id');
            $table->integer('tag_id');
            $table->timestamps();

            $table->index('sub_id', 'sub_id');

            $table->unique(['sub_id', 'tag_id'], 'sub_tag_unique');
        });

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tags');
        Schema::drop('tag_sub');
    }
}
