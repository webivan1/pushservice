<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppsDomains extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();

        Schema::create('apps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('state', false, 2);
            $table->string('key', 150)->unique();
            $table->timestamps();

            $table->index('user_id', 'user_id');
            $table->index('state', 'state');
        });

        Schema::create('domains', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->index();
            $table->string('name', 100);
            $table->timestamps();

            $table->index('user_id', 'user_id');
            $table->index('name', 'name');
        });

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('apps');
        Schema::drop('domains');
    }
}
