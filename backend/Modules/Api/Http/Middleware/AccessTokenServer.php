<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 12.08.2017
 * Time: 1:18
 */

namespace Modules\Api\Http\Middleware;

use Closure;
use App\Helpers\Hash;
use App\Helpers\Html;
use App\Models\Apps;
use App\Models\Users;
use Illuminate\Http\Request;
use Modules\Api\Services\SendPushParamsService;

class AccessTokenServer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        SendPushParamsService::getInstance()->setAttrsIsValid([
            'userId' => $request->header('User-id'),
            'apiKey' => $request->header('Api-key'),
            'appKey' => $request->header('App-key')
        ], [
            'userId' => 'required|int',
            'apiKey' => 'required|string',
            'appKey' => 'required|string'
        ]);

        $this->accessDoctype();

        return $next($request);
    }

    public function accessDoctype()
    {
        $service = SendPushParamsService::getInstance();

        $user = Users::where('id', '=', intval($service->userId))->firstOr(['*'], function () {
            abort(403);
        });

        $app = Apps::from('apps AS a')
            ->select(['a.*'])
            ->join('users AS u', 'u.id', '=', 'a.user_id')
            ->where('a.key', '=', Html::encode($service->appKey))
            ->where('a.server_key', '=', Html::encode($service->apiKey))
            ->where('u.id', '=', $user->id)
            ->groupBy('a.id')
            ->first();

        if (empty($app) || $user->status != 2) {
            abort(403);
        }

        $service->setAttrs(['appId' => $app->id]);
    }
}