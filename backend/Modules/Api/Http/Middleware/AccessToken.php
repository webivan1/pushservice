<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 10.08.2017
 * Time: 11:29
 */

namespace Modules\Api\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Modules\Api\Http\Components\Api;

use App\Models\{
    Users,
    Apps,
    Domains
};
use Modules\Api\Services\ApiDataService;

class AccessToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $api = new Api($request);

        preg_replace_callback('/([^\.]+\.[a-z]+)$/', function($value) use(&$domain) {
            $domain = $value[0];
        }, $api->domain);

        $requestParam = [
            'appKey' => $api->app_id,
            'domain' => $domain
        ];

        if (!$app = $this->hasAppAndUserActive($requestParam)) {
            abort(403);
        }

        // safe service
        ApiDataService::getInstance([
            'api' => $api,
            'app' => $app
        ]);

        return $next($request);
    }

    /**
     * Проверяем наличие приложения и статус юзера + разрешенные домены
     *
     * @param array $param
     * @return bool|Apps
     */
    public function hasAppAndUserActive(array $param)
    {
        $app = Apps::select('apps.*')
            ->join('users AS u', 'u.id', '=', 'apps.user_id')
            ->join('domains AS d', 'd.user_id', '=', 'u.id')
            ->whereRaw('
                apps.key = ? AND 
                u.status = 2 AND 
                apps.state = 2 AND 
                d.name = ?
            ', [$param['appKey'], $param['domain']])
            ->groupBy('apps.id')
            ->first();

        return empty($app) ? false : $app;
    }
}