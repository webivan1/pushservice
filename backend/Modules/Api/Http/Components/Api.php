<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 10.08.2017
 * Time: 23:18
 */

namespace Modules\Api\Http\Components;

use Illuminate\Http\Request;

class Api
{
    private $attrs;

    public function __construct(Request $request)
    {
        $this->attrs = [
            'app_id' => $request->header('App-key'),
            'domain' => $request->header('Domain-referer')
        ];
    }

    public function getAll()
    {
        return $this->attrs;
    }

    public function __get($name)
    {
        return isset($this->attrs[$name]) ? $this->attrs[$name] : null;
    }
}