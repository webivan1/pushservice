<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 13.08.2017
 * Time: 11:36
 */

namespace Modules\Api\Http\Components;

use App\Models\Messages;
use Modules\Api\Services\InputPushDatas;
use Modules\Api\Services\SendPushParamsService;

class SendPush
{
    /**
     * @property string
     */
    private static $url = 'https://fcm.googleapis.com/fcm/send';

    /**
     * @property string
     */
    private static $serverKey = 'AAAAECHScXs:APA91bGR59H5oNj4NMBzVs7es9pkzqcuyGrJAK2eC36bB4qgQYmzeiedd4Dc4YxgSenr5Z51Ge0-owkijOIqxPXHgooKBXnCDq29aRZf6w1UpKZ0vUfpQjyBTUXbIylbocgDOIEV56Ma';

    /**
     * Send All
     *
     * @param array $templateArray
     * @return array
     */
    public static function send(array $template)
    {
        $answer = [];

        $service = SendPushParamsService::getInstance();

        $messageCreate = [
            'success' => 0,
            'fails' => 0,
            'app_id' => $service->appId,
            'push_message' => json_encode($template['notification'])
        ];

        $response = self::sendItem($template);

        if (is_string($response)) {
            $responseArray = @json_decode($response, true);

            if (!empty($responseArray) && isset($responseArray['multicast_id'])) {
                $messageCreate['success'] = (int) $responseArray['success'] ?? 0;
                $messageCreate['fails'] = (int) $responseArray['failure'] ?? 0;
            }
        }

        $answer[] = $response;

        // Insert
        Messages::create($messageCreate);

        return $messageCreate;
    }

    /**
     * Send item
     *
     * @param array $template
     * @return string
     */
    public static function sendItem(array $template)
    {
        $ch = curl_init(self::$url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Authorization: key=' . self::$serverKey,
        ]);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($template));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        $response = curl_exec($ch);

        curl_close($ch);

        return $response;
    }
}