<?php

use App\Models\Subscribers;

Route::group([
    'middleware' => [
        'web',
    ],
    'prefix' => 'api',
    'namespace' => 'Modules\Api\Http\Controllers'
], function () {

    // Users
    Route::group([
        'prefix' => 'get',
    ], function() {

        Route::get('languages', function () {
            $output = [];

            foreach (LaravelLocalization::getSupportedLocales() as $lang => $info) {
                $output[] = [
                    'id' => $lang,
                    'name' => $info['name']
                ];
            }

            return $output;
        });

        Route::get('langs-list', function () {
            return \App\Models\Languages::select(['id', 'name'])->cache(60 * 12)->orderBy('name')->get();
        });

        Route::get('user', function () {
            return Auth::user() ?? ['status' => 0];
        });

    });

    // Users
    Route::group([
        'prefix' => 'user',
        'middleware' => ['Modules\Api\Http\Middleware\AccessToken']
    ], function() {

        Route::post('/', function (\Illuminate\Http\Request $request) {
            return Subscribers::getUser(
                \Modules\Api\Services\ApiDataService::getInstance()->app->id,
                $request->input('token')
            );
        });

        Route::post('create', function (\Illuminate\Http\Request $request) {
            return Subscribers::createSub($request);
        });

    });

    // Tags
    Route::group([
        'prefix' => 'tags',
        'middleware' => ['Modules\Api\Http\Middleware\AccessToken']
    ], function() {

        Route::post('/', function (\Illuminate\Http\Request $request) {
            return \App\Models\Tags::tagsSubs($request->input('token'));
        });

        Route::get('/all', function (\Illuminate\Http\Request $request) {
            return \App\Models\Tags::allTags($request);
        });

        Route::post('/create', function (\Illuminate\Http\Request $request) {
            return (new \App\Models\Tags())->tagCreateWithSubs($request);
        });

        Route::post('/set-tags', function (\Illuminate\Http\Request $request) {
            return (new \App\Models\Tags())->tagsCreateWithSubs($request);
        });

        Route::post('/delete', function (\Illuminate\Http\Request $request) {
            return (new \App\Models\Tags())->removeTag($request);
        });

        Route::post('/delete-tags', function (\Illuminate\Http\Request $request) {
            return (new \App\Models\Tags())->removeTags($request);
        });

    });

    // Send
    Route::group([
        'prefix' => 'send',
        'middleware' => [
            'Modules\Api\Http\Middleware\AccessTokenServer'
        ]
    ], function() {

        Route::get('/', 'SendController@index');

    });

});
