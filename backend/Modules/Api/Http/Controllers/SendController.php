<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 12.08.2017
 * Time: 16:16
 */

namespace Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Api\Services\ContentService;
use Modules\Api\Services\SendPushParamsService;
use Modules\Api\Http\Components\SendPush;

class SendController extends Controller
{
    /**
     * Send push messages
     *
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        // generate content, validation datas
        $content = ContentService::getInstance($request->input('datas'))->run();

        // search subs
        $filter = FilterSendService::getInstance(['service' => $content]);

        $response = [];

        $filter->getMessages(function ($messages) use ($response) {
            foreach ($messages as $lang => $message) {
                $response[] = SendPush::send($message);
            }
        });

        return $response;
    }
}