<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 13.08.2017
 * Time: 0:02
 */

namespace Modules\Api\Services;

use App\Components\BaseService;

class SendPushParamsService extends BaseService
{
    /**
     * User id
     *
     * @property int
     */
    public $userId;

    /**
     * Apps key
     *
     * @property string
     */
    public $appKey;

    /**
     * Apps id
     *
     * @property int
     */
    public $appId;

    /**
     * Api key
     * @link \App\Helpers\Hash::generateApiKey
     * @property string
     */
    public $apiKey;
}