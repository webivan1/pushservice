<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 07.10.2017
 * Time: 21:42
 */

namespace Modules\Api\Services;

use App\Components\BaseService;
use App\Models\Templates;
use Faker\Provider\Base;
use Illuminate\Support\Facades\Validator;

class ContentService extends BaseService
{
    /**
     * @property array
     */
    public $title;

    /**
     * @property array
     */
    public $body;

    /**
     * @property array
     */
    public $link;

    /**
     * @property string
     */
    public $icon;

    /**
     * @property string
     */
    public $image;

    /**
     * @property array
     */
    public $template;

    /**
     * @property array
     */
    public $filters;

    /**
     * Start script, validation datas
     *
     * @return BaseService
     */
    public function run(): self
    {
        return $this->validateTemplate()
            ->propsWithTemplate()
            ->validateContent();
    }

    /**
     * Result datas
     *
     * @return array
     */
    public function getResult(): array
    {
        $result = [];

        $this->keyLanguagesCategory($this->title, 'title', $result)
            ->keyLanguagesCategory($this->body, 'body', $result)
            ->keyLanguagesCategory($this->link, 'link', $result);

        return [
            'datas' => $this->toArray(),
            'languageCategory' => $result
        ];
    }

    /**
     * Set category language
     *
     * @param array $props
     * @param string $category
     * @param array $result
     * @return BaseService
     */
    private function keyLanguagesCategory(array $props, string $category, &$result = []): self
    {
        foreach ($props as $lang => $prop) {
            $result[$lang][] = $category;
            $result[$lang] = array_unique($result[$lang]);
        }

        return $this;
    }

    /**
     * Validation template
     *
     * @return BaseService
     */
    private function validateTemplate(): self
    {
        Validator::make($this->toArray(), [
            'template' => 'array',
            'template.uid' => 'required|int',
            'template.token' => 'required|string',
        ])->validate();

        return $this;
    }

    /**
     * Validation all
     *
     * @return BaseService
     */
    private function validateContent(): self
    {
        Validator::make($this->toArray(), [
            'title' => 'array',
            'title.en' => 'required|string',
            'title.*' => 'string',
            'body' => 'array',
            'body.en' => 'required|string',
            'body.*' => 'string',
            'icon' => 'required|string|url',
            'link' => 'array',
            'link.en' => 'required|url',
            'link.*' => 'url',
            'image' => 'url',
            'filters' => 'array'
        ])->validate();

        return $this;
    }

    /**
     * With template datas
     *
     * @return BaseService
     */
    private function propsWithTemplate(): self
    {
        if (!$this->template) {
            return $this;
        }

        $data = Templates::select(['icon', 'image', 'id'])
            ->cache(10)
            ->with(['contents' => function ($model) {
                return $model->select(['type', 'languages_id', 'content', 'template_id'])
                    ->with('lang');
            }])
            ->whereRaw('id = ? AND token = ? AND app_id = ?', [
                $this->template['uid'],
                $this->template['token'],
                SendPushParamsService::getInstance()->appId
            ])
            ->first();

        if (empty($data)) {
            return $this;
        } else {
            $data = $data->toArray();
        }

        $this->icon = $this->icon ?? $data['icon'];
        $this->image = $this->image ?? $data['image'];

        if (!empty($data['contents'])) {
            foreach ($data['contents'] as $content) {
                $mergeArray = [
                    $content['lang']['value'] => $content['content']
                ];

                switch ($content['type']) {
                    case 'heading' :
                        $this->title = $this->title ? array_merge($mergeArray, $this->title) : $mergeArray;
                    break;

                    case 'content' :
                        $this->body = $this->body ? array_merge($mergeArray, $this->body) : $mergeArray;
                    break;

                    case 'link' :
                        $this->link = $this->link ? array_merge($mergeArray, $this->link) : $mergeArray;
                    break;
                }
            }
        }

        return $this;
    }
}