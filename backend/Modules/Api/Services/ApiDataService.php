<?php
/**
 * Created by PhpStorm.
 * User: maltsev
 * Date: 11.09.2017
 * Time: 12:12
 */

namespace Modules\Api\Services;

use App\Components\BaseService;

class ApiDataService extends BaseService
{
    /**
     * @property \Modules\Api\Http\Components\Api
     */
    public $api;

    /**
     * @property \App\Models\Apps
     */
    public $app;
}