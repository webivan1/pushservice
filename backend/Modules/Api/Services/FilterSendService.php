<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 08.10.2017
 * Time: 19:33
 */

namespace Modules\Api\Services;

use App\Components\BaseService;
use App\Components\SearchComponent;
use App\Models\Languages;
use App\Models\Subscribers;
use Illuminate\Database\Eloquent\Collection;
use Modules\Panel\Services\SearchSubsService;

class FilterSendService extends BaseService
{
    /**
     * @property ContentService
     */
    public $service;

    /**
     * Generate messages to google
     *
     * @param \Closure $callHandler
     * @return void
     */
    public function getMessages(\Closure $callHandler)
    {
        $query = Subscribers::alias('t')
            ->select(['t.lang', 't.token', 't.app_id'])
            ->where('t.app_id', '=', SendPushParamsService::getInstance()->appId)
            ->orderBy('t.id', 'asc');

        SearchSubsService::getInstance($this->service->filters ?? []);

        // with search params
        new SearchComponent($query, SearchSubsService::getInstance());

        $query->chunk(1000, function (Collection $subs) use ($callHandler) {
            if (!empty($subs)) {
                call_user_func($callHandler, $this->setTemplateNotify($subs));
            }
        });
    }

    /**
     * @return array
     */
    private function map()
    {
        return [
            'body' => 'body',
            'title' => 'title',
            'link' => 'click_action'
        ];
    }

    /**
     * Set template messages
     *
     * @param Collection $subs
     * @return array
     */
    private function setTemplateNotify(Collection $subs): array
    {
        $sendTemplate = [];
        $textFullComplete = [];

        $map = $this->map();

        $defaultNotification = [
            'title' => $this->service->title['en'],
            'body' => $this->service->body['en'],
            'click_action' => $this->service->link['en'],
            'icon' => $this->service->icon
        ];

        $resultServiceContent = $this->service->getResult()['languageCategory'] ?? [];

        foreach ($subs as $sub) {
            if (!isset($sendTemplate[$sub->lang])) {
                $sendTemplate[$sub->lang] = [
                    'registration_ids' => [$sub->token],
                    'notification' => $defaultNotification
                ];
            } else {
                array_push($sendTemplate[$sub->lang]['registration_ids'], $sub->token);
            }

            if ($sub->lang != Languages::DEFAULT_LANG && isset($resultServiceContent[$sub->lang]) && !isset($textFullComplete[$sub->lang])) {
                foreach ($resultServiceContent[$sub->lang] as $cat) {
                    if (isset($map[$cat]) && property_exists($this->service, $cat) && !empty($this->service->{$cat}[$sub->lang])) {
                        $sendTemplate[$sub->lang]['notification'][$map[$cat]] = $this->service->{$cat}[$sub->lang];
                    }
                }

                $textFullComplete[$sub->lang] = true;
            }
        }

        return $sendTemplate;
    }
}