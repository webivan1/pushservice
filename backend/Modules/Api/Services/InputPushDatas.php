<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 13.08.2017
 * Time: 11:39
 */

namespace Modules\Api\Services;

use App\Components\BaseService;
use App\Helpers\Html;
use App\Models\Subscribers;
use Illuminate\Support\Facades\DB;
use Exception;

class InputPushDatas extends BaseService
{
    /**
     * Tags
     *
     * @property array<string>
     */
    public $tags;

    /**
     * @ToDo Templates
     *
     * @property int
     */
    public $templateId;

    /**
     * @property string
     */
    public $title;

    /**
     * @property string
     */
    public $body;

    /**
     * @property string
     */
    public $link;

    /**
     * @property string
     */
    public $icon;

    /**
     * Generate google template push
     *
     * @throws Exception
     * @return array
     */
    public function templateGoogleSend()
    {
        $service = SendPushParamsService::getInstance();

        $this->tags = array_filter($this->tags, function (&$value) {
            if (is_string($value)) {
                $value = Html::encode($value);
                return true;
            } else {
                return false;
            }
        });

        if (empty($this->tags)) {
            throw new Exception("Empty tags", 422);
        }

        if (!empty($this->templateId)) {
            // @ToDo template sender
        }

        $notification = [
            'title' => $this->title,
            'body' => $this->body,
            'icon' => $this->icon,
            'click_action' => $this->link
        ];

        $notification = array_diff($notification, ['']);

        $sender = [];

        $allUsers = DB::table('subscribers AS s')
            ->select(['s.token'])
            ->join('tag_sub AS ts', 'ts.sub_id', '=', 's.id')
            ->join('tags AS t', 't.id', '=', 'ts.tag_id')
            ->whereIn('t.alias', $this->tags)
            ->where('s.app_id', '=', $service->appId)
            ->groupBy('s.id')
            ->orderBy('s.id')
            ->chunk(1000, function ($subs) use (&$sender, $notification) {
                $sender[] = array_merge([
                    'registration_ids' => array_column($subs->toArray(), 'token')
                ], [
                    'notification' => $notification
                ]);
            });

        if (empty($sender)) {
            throw new Exception("Not subscribers", 422);
        }

        return $sender;
    }
}