
@extends('admin::layouts.main')

@section('content')

    <h1>Users</h1>
    <hr />

    <div class="panel">
        <div class="panel-body">
            @include('admin::users.form-search')
        </div>
    </div>

    <?php \App\Helpers\Pjax::start('js-grid-users') ?>

    <p>Всего найдено: {{ $models->total() }}</p>

    <table class="table">
        <thead>
            <tr>
                <th>{!! $sort->link('id', 'ID') !!}</th>
                <th>Name</th>
                <th>Email</th>
                <th>{!! $sort->link('status', 'Status') !!}</th>
                <th>Role</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($models as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->email }}</td>
                <td>{{ $item->status }}</td>
                <td>{{ $item->roleId->role->name }}</td>
                <td>
                    <div class="btn-group">
                        <a class="btn btn-sm btn-info not-pjax" href="{{ route('users') }}/update/{{ $item->id }}">
                            Edit
                        </a>
                        <a class="btn btn-sm btn-danger not-pjax" href="{{ route('users') }}/delete/{{ $item->id }}">
                            Delete
                        </a>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    {{ $models->appends([$sort->pageVar => $sort->getString()])->links() }}

    <?php \App\Helpers\Pjax::end() ?>

@endsection