<form action="" method="post" class="form-ajax-grid form-group">
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                <label class="control-label">ID</label>
                <input type="text" name="Users[id]" value="" class="form-control" />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label class="control-label">Name</label>
                <input type="text" name="Users[name]" value="" class="form-control" />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label class="control-label">Email</label>
                <input type="text" name="Users[email]" value="" class="form-control" />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label class="control-label">Status</label>
                <select name="Users[status]" class="form-control">
                    <option value="">Выбрать</option>
                    <option value="1">На ожидании</option>
                    <option value="2">Активен</option>
                    <option value="3">Заблокирован</option>
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label class="control-label">Role</label>
                <select name="Users[role_id]" class="form-control">
                    {{ $roles = \App\Models\Role::all() }}
                    <option value="">Выбрать</option>
                    @foreach($roles as $role)
                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label class="control-label">&nbsp;</label>
                <div>
                    <button class="btn btn-primary">Найти</button>
                </div>
            </div>
        </div>
    </div>
</form>