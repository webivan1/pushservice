@extends('admin::layouts.main')

@section('content')

    <h1>{{ $isNewRecord ? 'Добавить' : 'Обновить' }} юзера #{{ $model->id }}</h1>

    <div class="row">
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-body">
                    <?php \App\Helpers\FormAjax::start([
                        'id' => 'form-users-update',
                        'action' => '',
                        'method' => 'post',
                        'isNewRecord' => $isNewRecord,
                        'listUrl' => route('users')
                    ]) ?>

                        <div class="form-group" valid-name="name">
                            <label class="control-label">Name</label>
                            <input class="form-control" type="text" name="Users[name]" value="{{ $model->name ?? null }}" />
                        </div>

                        <div class="form-group" valid-name="email">
                            <label class="control-label">Email</label>
                            <input class="form-control" type="text" name="Users[email]" value="{{ $model->email ?? null }}" />
                        </div>

                        <div class="form-group" valid-name="status">
                            <label class="control-label">Status</label>
                            <?php
                                $options = [
                                    1 => 'На ожидании',
                                    2 => 'Активен',
                                    3 => 'Заблокирован'
                                ];
                            ?>
                            <select name="Users[status]" class="form-control">
                                <option value="">Выбрать</option>
                                @foreach($options as $value => $name)
                                    <option {{ !$isNewRecord && $value == $model->status ? 'selected' : null }} value="{{ $value }}">{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group" valid-name="role">
                            <label class="control-label">Role</label>
                            <select name="Users[role_id]" class="form-control">
                                {{ $roles = \App\Models\Role::all() }}
                                <option value="">Выбрать</option>
                                @foreach($roles as $role)
                                    <option {{ !$isNewRecord && $role->id == $model->roleId->role_id ? 'selected' : null }} value="{{ $role->id }}">{{ $role->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <button class="btn btn-primary">{{ $isNewRecord ? 'Добавить' : 'Обновить' }}</button>

                    <?php \App\Helpers\FormAjax::end() ?>
                </div>
            </div>
        </div>
    </div>

@endsection