@extends('admin::layouts.main')

@section('content')

    <h1>{{ $isNewRecord ? 'Добавить' : 'Обновить' }} язык #{{ $model->id }}</h1>

    <div class="row">
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-body">
                    <?php \App\Helpers\FormAjax::start([
                        'id' => 'form-tags-update',
                        'action' => '',
                        'method' => 'post',
                        'isNewRecord' => $isNewRecord,
                        'listUrl' => route('tags')
                    ]) ?>

                        <div class="form-group" valid-name="value">
                            <label class="control-label">Value</label>
                            <input class="form-control" type="text" name="Tags[alias]" value="{{ $model->alias ?? null }}" />
                        </div>

                        <button class="btn btn-primary">{{ $isNewRecord ? 'Добавить' : 'Обновить' }}</button>

                    <?php \App\Helpers\FormAjax::end() ?>
                </div>
            </div>
        </div>
    </div>

@endsection