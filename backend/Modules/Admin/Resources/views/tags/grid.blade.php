
@extends('admin::layouts.main')

@section('content')

    <h1>Tags default</h1>
    <hr />

    <div class="panel">
        <div class="panel-body">
            @include('admin::tags.form-search')
        </div>
    </div>

    <?php \App\Helpers\Pjax::start('js-grid-languages') ?>

    <div class="clearfix">
        <a href="{{ route('tags.create') }}" class="btn btn-info pull-right not-pjax">Create</a>
        <p>Всего найдено: {{ $models->total() }}</p>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th>{!! $sort->link('id', 'ID') !!}</th>
                <th>{!! $sort->link('alias', 'Alias') !!}</th>
                <th>Name</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($models as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->alias }}</td>
                <td>
                    <div class="btn-group">
                        <a class="btn btn-sm btn-info not-pjax" href="{{ route('tags') }}/update/{{ $item->id }}">
                            Edit
                        </a>
                        <a class="btn btn-sm btn-danger not-pjax" href="{{ route('tags') }}/delete/{{ $item->id }}">
                            Delete
                        </a>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    {{ $models->appends([$sort->pageVar => $sort->getString()])->links() }}

    <?php \App\Helpers\Pjax::end() ?>

@endsection