<form action="" method="post" class="form-ajax-grid form-group">
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                <label class="control-label">ID</label>
                <input type="text" name="Tags[id]" value="" class="form-control" />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label class="control-label">Alias</label>
                <input type="text" name="Tags[alias]" value="" class="form-control" />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label class="control-label">&nbsp;</label>
                <div>
                    <button class="btn btn-primary">Найти</button>
                </div>
            </div>
        </div>
    </div>
</form>