@extends('admin::layouts.main')

@section('content')

    <h1>{{ $isNewRecord ? 'Добавить' : 'Обновить' }} перевод #{{ $model->id }}</h1>

    <div class="row">
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-body">
                    <?php \App\Helpers\FormAjax::start([
                        'id' => 'form-words-update',
                        'action' => '',
                        'method' => 'post',
                        'isNewRecord' => $isNewRecord,
                        'listUrl' => route('words')
                    ]) ?>

                        <div class="form-group" valid-name="name">
                            <label class="control-label">Key</label>
                            <input class="form-control" type="text" name="Words[name]" value="{{ $model->name ?? null }}" />
                        </div>

                        <?php
                            $allLangs = \App\Models\Languages::where('state', '=', 2)->get();
                            $allWords = !$isNewRecord ? \App\Helpers\ArrayHelper::map($model->words, 'language_id', 'translate') : null;
                        ?>

                        @foreach($allLangs as $key => $lang)
                            <input type="hidden" name="Words[words][{{ $key }}][language_id]" value="{{ $lang->id }}" />
                            <div class="form-group" valid-name="translate">
                                <label class="control-label">{{ $lang->name }}</label>
                                <textarea class="form-control" name="Words[words][{{ $key }}][translate]">{{ is_null($allWords) ? null : ($allWords[$lang->id] ?? null) }}</textarea>
                            </div>
                        @endforeach

                        <button class="btn btn-primary">{{ $isNewRecord ? 'Добавить' : 'Обновить' }}</button>

                    <?php \App\Helpers\FormAjax::end() ?>
                </div>
            </div>
        </div>
    </div>

@endsection