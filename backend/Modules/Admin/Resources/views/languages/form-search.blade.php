<form action="" method="post" class="form-ajax-grid form-group">
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                <label class="control-label">ID</label>
                <input type="text" name="Languages[id]" value="" class="form-control" />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label class="control-label">Value</label>
                <input type="text" name="Languages[value]" value="" class="form-control" />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label class="control-label">Name</label>
                <input type="text" name="Languages[name]" value="" class="form-control" />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label class="control-label">&nbsp;</label>
                <div>
                    <button class="btn btn-primary">Найти</button>
                </div>
            </div>
        </div>
    </div>
</form>