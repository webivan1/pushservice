@extends('admin::layouts.main')

@section('content')

    <h1>{{ $isNewRecord ? 'Добавить' : 'Обновить' }} язык #{{ $model->id }}</h1>

    <div class="row">
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-body">
                    <?php \App\Helpers\FormAjax::start([
                        'id' => 'form-language-update',
                        'action' => '',
                        'method' => 'post',
                        'isNewRecord' => $isNewRecord,
                        'listUrl' => route('languages')
                    ]) ?>

                        <div class="form-group" valid-name="value">
                            <label class="control-label">Value</label>
                            <input class="form-control" type="text" name="Languages[value]" value="{{ $model->value ?? null }}" />
                        </div>

                        <div class="form-group" valid-name="name">
                            <label class="control-label">Name</label>
                            <input class="form-control" type="text" name="Languages[name]" value="{{ $model->name ?? null }}" />
                        </div>

                        <div class="form-group" valid-name="script">
                            <label class="control-label">Script</label>
                            <input class="form-control" type="text" name="Languages[script]" value="{{ $model->script ?? null }}" />
                        </div>

                        <div class="form-group" valid-name="native">
                            <label class="control-label">Native</label>
                            <input class="form-control" type="text" name="Languages[native]" value="{{ $model->native ?? null }}" />
                        </div>

                        <div class="form-group" valid-name="regional">
                            <label class="control-label">Regional</label>
                            <input class="form-control" type="text" name="Languages[regional]" value="{{ $model->regional ?? null }}" />
                        </div>

                        <button class="btn btn-primary">{{ $isNewRecord ? 'Добавить' : 'Обновить' }}</button>

                    <?php \App\Helpers\FormAjax::end() ?>
                </div>
            </div>
        </div>
    </div>

@endsection