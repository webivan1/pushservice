<?php
/**
 * Created by PhpStorm.
 * User: maltsev
 * Date: 25.07.2017
 * Time: 13:01
 */

namespace Modules\Admin\Models;

use App\Models\WordKey as Base;
use App\Models\Words as TranslateWords;
use App\Components\Sort;
use App\Helpers\Html;
use Illuminate\Support\Facades\Validator;
use DB;

class Words extends Base implements InterfaceModel
{
    /**
     * @property array
     */
    public $staticSearch = ['text', 'words'];

    /**
     * Rules
     *
     * @return array
     */
    public function rulesUpdate(): array
    {
        return [
            'name' => 'required|string|max:150',
            'words' => 'required|array'
        ];
    }

    /**
     * Rules
     *
     * @return array
     */
    public function rulesSearch(): array
    {
        return [
            'id' => 'int',
            'name' => 'string',
            'text' => 'string'
        ];
    }

    /**
     * Search grid
     *
     * @param array $search
     * @return array
     */
    public function search(array $search = [])
    {
        $query = self::from($this->table . ' AS t')
            ->select(['t.*', DB::raw('COUNT(w.id)')])
            ->join('words AS w', 'w.word_key_id', '=', 't.id')
            ->groupBy(['t.id']);

        if (!empty($search)) {
            $autoSearch = array_diff_key($search, array_flip($this->staticSearch));

            foreach ($autoSearch as $key => $value) {
                $value = Html::encode($value);

                if (is_numeric($value)) {
                    $query->where('t.' . $key, $value);
                } else {
                    $query->where('t.' . $key, 'like', "%$value%");
                }
            }
        }

        if (!empty($search['text'])) {
            $query->where('w.translate', 'like', '%'. Html::encode($search['text']) .'%');
        }

        $sort = new Sort([
            'attributes' => [
                'id' => [
                    'asc' => [
                        ['t.id', Sort::SORT_ASC]
                    ],
                    'desc' => [
                        ['t.id', Sort::SORT_DESC]
                    ]
                ],
                'name' => [
                    'asc' => [
                        ['t.name', Sort::SORT_ASC]
                    ],
                    'desc' => [
                        ['t.name', Sort::SORT_DESC]
                    ]
                ]
            ],
            'defaultOrder' => 'name.asc'
        ]);

        foreach ($sort->sortBy() as $sortItem) {
            $query->orderBy($sortItem[0], $sortItem[1]);
        }

        return [
            'sort' => $sort,
            'models' => $query->paginate(20)
        ];
    }

    /**
     * Change item
     *
     * @param array $data
     * @return bool
     */
    public function changeItem(array $data = [])
    {
        foreach (array_diff_key($data, array_flip($this->staticSearch)) as $key => $value) {
            $this->{$key} = $value;
        }

        $this->save();

        // Clear all translates
        TranslateWords::where('word_key_id', $this->id)->delete();

        foreach ($data['words'] as $item) {
            $data = array_merge($item, ['word_key_id' => $this->id]);

            Validator::make($data, [
                'language_id' => 'required|int',
                'word_key_id' => 'required|int',
                'translate' => 'required|string',
            ])->validate();

            TranslateWords::create($data);
        }

        $this->generateTranslateFiles();

        return true;
    }

    public function generateTranslateFiles()
    {
        $allLanguage = Languages::all();

        foreach ($allLanguage as $lang) {
            $words = TranslateWords::from('words AS t')
                ->select(['t.translate', 'wk.name'])
                ->join('word_key AS wk', 'wk.id', '=', 't.word_key_id')
                ->where('t.language_id', $lang->id)
                ->groupBy('t.id')
                ->get()
                ->toArray();

            if (empty($words)) {
                continue;
            }

            $words = \App\Helpers\ArrayHelper::map($words, 'name', 'translate');

            $this->safeLangsServer($words, $lang->value);
            $this->safeLangsClient($words, $lang->value);
        }
    }

    private function safeLangsServer($words, $langName)
    {
        $pathFiles = realpath('./') . '/../backend/resources/lang/' . $langName;

        if (!is_dir($pathFiles)) {
            @mkdir($pathFiles, 0777);
        }

        file_put_contents($pathFiles . '/data.php', "<?php \nreturn " . var_export($words, true) . ';');

        @chmod($pathFiles . '/data.php', 0664);
    }

    private function safeLangsClient($words, $langName)
    {
        $pathFiles = realpath('./') . '/lang';

        if (!is_dir($pathFiles)) {
            @mkdir($pathFiles, 0777);
        }

        file_put_contents($pathFiles . '/' . $langName . '.json', json_encode($words, JSON_UNESCAPED_UNICODE));

        @chmod($pathFiles . '/' . $langName . '.json', 0664);
    }
}