<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 23.07.2017
 * Time: 20:56
 */

namespace Modules\Admin\Models;

use App\Models\Users as Base;
use App\Components\Sort;
use App\Helpers\Html;
use League\Flysystem\Exception;

class Users extends Base implements InterfaceModel
{
    /**
     * @property array
     */
    public $staticSearch = ['role_id'];

    /**
     * Rules
     *
     * @return array
     */
    public function rulesUpdate(): array
    {
        return [
            'name' => 'required|string|max:150',
            'email' => 'required|string|email|max:150',
            'status' => 'required|int',
            'role_id' => 'required|int'
        ];
    }

    /**
     * Rules
     *
     * @return array
     */
    public function rulesSearch(): array
    {
        return [
            'name' => 'string|max:150',
            'email' => 'string|max:150',
            'status' => 'int',
            'id' => 'int',
            'role_id' => 'int'
        ];
    }

    /**
     * Search grid
     *
     * @param array $search
     * @return array
     */
    public function search(array $search = [])
    {
        $query = self::from($this->table . ' AS u')
            ->select('u.*')
            ->join('role_user AS ru', 'u.id', '=', 'ru.user_id')
            ->with([
                'roleId' => function($model) {
                    return $model->with('role');
                }
            ]);

        if (!empty($search)) {
            $autoSearch = array_diff_key($search, array_flip($this->staticSearch));

            foreach ($autoSearch as $key => $value) {
                $value = Html::encode($value);

                if (is_numeric($value)) {
                    $query->where('u.' . $key, $value);
                } else {
                    $query->where('u.' . $key, 'like', "%$value%");
                }
            }
        }

        if (!empty($search['role_id'])) {
            $query->where('ru.role_id', (int) $search['role_id']);
        }

        $sort = new Sort([
            'attributes' => [
                'id' => [
                    'asc' => [
                        ['u.id', Sort::SORT_ASC]
                    ],
                    'desc' => [
                        ['u.id', Sort::SORT_DESC]
                    ]
                ],
                'status' => [
                    'asc' => [
                        ['u.status', Sort::SORT_ASC]
                    ],
                    'desc' => [
                        ['u.status', Sort::SORT_DESC]
                    ]
                ]
            ],
            'defaultOrder' => 'id.desc'
        ]);

        foreach ($sort->sortBy() as $sortItem) {
            $query->orderBy($sortItem[0], $sortItem[1]);
        }

        return [
            'sort' => $sort,
            'models' => $query->paginate(20)
        ];
    }

    /**
     * Change item
     *
     * @param array $data
     * @return bool
     */
    public function changeItem(array $data = [])
    {
        foreach (array_diff_key($data, ['role_id' => 1]) as $key => $value) {
            $this->{$key} = $value;
        }

        $this->save();

        if (!empty($data['role_id'])) {
            if ($roleUser = \App\Models\RoleUser::where('user_id', $this->id)) {
                if ($roleUser != $data['role_id']) {
                    $roleUser->role_id = $data['role_id'];
                }
            }
        }

        return true;
    }
}