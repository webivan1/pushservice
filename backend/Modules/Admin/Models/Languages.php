<?php
/**
 * Created by PhpStorm.
 * User: maltsev
 * Date: 25.07.2017
 * Time: 12:24
 */

namespace Modules\Admin\Models;

use App\Models\Languages as Base;
use App\Components\Sort;
use App\Helpers\Html;

class Languages extends Base implements InterfaceModel
{
    /**
     * @property array
     */
    public $staticSearch = [];

    /**
     * Rules
     *
     * @return array
     */
    public function rulesUpdate(): array
    {
        return [
            'value' => 'required|string|max:10',
            'name' => 'required|string|max:20',
            'script' => 'required|string|max:20',
            'native' => 'required|string|max:20',
            'regional' => 'required|string|max:10',
        ];
    }

    /**
     * Rules
     *
     * @return array
     */
    public function rulesSearch(): array
    {
        return [
            'id' => 'int',
            'value' => 'string|max:10',
            'name' => 'string|max:20',
            'script' => 'string|max:20',
            'native' => 'string|max:20',
            'regional' => 'string|max:10',
        ];
    }

    /**
     * Search grid
     *
     * @param array $search
     * @return array
     */
    public function search(array $search = [])
    {
        $query = self::select('*');

        if (!empty($search)) {
            $autoSearch = array_diff_key($search, array_flip($this->staticSearch));

            foreach ($autoSearch as $key => $value) {
                $value = Html::encode($value);

                if (is_numeric($value)) {
                    $query->where($key, $value);
                } else {
                    $query->where($key, 'like', "%$value%");
                }
            }
        }

        $sort = new Sort([
            'attributes' => [
                'id' => [
                    'asc' => [
                        ['id', Sort::SORT_ASC]
                    ],
                    'desc' => [
                        ['id', Sort::SORT_DESC]
                    ]
                ],
                'value' => [
                    'asc' => [
                        ['value', Sort::SORT_ASC]
                    ],
                    'desc' => [
                        ['value', Sort::SORT_DESC]
                    ]
                ]
            ],
            'defaultOrder' => 'value.asc'
        ]);

        foreach ($sort->sortBy() as $sortItem) {
            $query->orderBy($sortItem[0], $sortItem[1]);
        }

        return [
            'sort' => $sort,
            'models' => $query->paginate(10)
        ];
    }

    /**
     * Change item
     *
     * @param array $data
     * @return bool
     */
    public function changeItem(array $data = [])
    {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }

        $this->save();

        return true;
    }
}