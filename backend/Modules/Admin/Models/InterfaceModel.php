<?php
/**
 * Created by PhpStorm.
 * User: maltsev
 * Date: 24.07.2017
 * Time: 17:37
 */

namespace Modules\Admin\Models;

interface InterfaceModel
{
    /**
     * Rules
     *
     * @return array
     */
    public function rulesUpdate(): array;

    /**
     * Rules
     *
     * @return array
     */
    public function rulesSearch(): array;

    /**
     * Search grid
     *
     * @param array $search
     * @return array
     */
    public function search(array $search = []);

    /**
     * Change item
     *
     * @param array $data
     * @return bool
     */
    public function changeItem(array $data = []);
}