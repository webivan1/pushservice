<?php
/**
 * Created by PhpStorm.
 * User: maltsev
 * Date: 24.07.2017
 * Time: 14:25
 */

namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Http\Components\CrudController;
use Modules\Admin\Models\Words;
use Illuminate\Http\Request;

class WordsController extends CrudController
{
    public $model;
    public $name = 'words';
    public $getParam = 'Words';

    public function __construct()
    {
        $this->model = new Words();
    }
}