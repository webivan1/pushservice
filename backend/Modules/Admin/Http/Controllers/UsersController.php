<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 23.07.2017
 * Time: 0:51
 */

namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Http\Components\CrudController;
use Modules\Admin\Models\Users;
use Illuminate\Http\Request;

class UsersController extends CrudController
{
    public $model;
    public $name = 'users';
    public $getParam = 'Users';

    public function __construct()
    {
        $this->model = new Users();
    }

    public function create(Request $request)
    {
        abort(404);
    }
}
