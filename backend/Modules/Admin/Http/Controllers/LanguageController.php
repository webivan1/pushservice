<?php
/**
 * Created by PhpStorm.
 * User: maltsev
 * Date: 24.07.2017
 * Time: 14:25
 */

namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Http\Components\CrudController;
use Modules\Admin\Models\Languages;
use Illuminate\Http\Request;

class LanguageController extends CrudController
{
    public $model;
    public $name = 'languages';
    public $getParam = 'Languages';

    public function __construct()
    {
        $this->model = new Languages();
    }
}