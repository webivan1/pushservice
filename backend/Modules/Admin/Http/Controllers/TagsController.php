<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 14.08.2017
 * Time: 21:06
 */

namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Http\Components\CrudController;
use Modules\Admin\Models\Languages;
use Illuminate\Http\Request;
use Modules\Admin\Models\Tags;

class TagsController extends CrudController
{
    public $model;
    public $name = 'tags';
    public $getParam = 'Tags';

    public function __construct()
    {
        $this->model = new Tags();
    }
}