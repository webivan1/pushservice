<?php

Route::group([
    'middleware' => [
        'web',
        'auth',
        'App\Http\Middleware\AccessRules:moderator'
    ],
    'prefix' => 'admin',
    'namespace' => 'Modules\Admin\Http\Controllers'
], function() {

    Route::get('/', 'AdminController@index')->name('admin');

    // Users
    Route::group([
        'prefix' => 'users',
        'middleware' => 'App\Http\Middleware\AccessRules:admin'
    ], function() {
        Route::match(['get', 'post'], '/', 'UsersController@index')->name('users');
        Route::match(['get', 'post'], '/update/{id}', 'UsersController@update')->name('users.update')->where('id', '[0-9]+');
    });

    // Language
    Route::group([
        'prefix' => 'languages',
        'middleware' => 'App\Http\Middleware\AccessRules:moderator'
    ], function() {
        Route::match(['get', 'post'], '/', 'LanguageController@index')->name('languages');
        Route::match(['get', 'post'], '/update/{id}', 'LanguageController@update')->name('languages.update')->where('id', '[0-9]+');
        Route::match(['get', 'post'], '/create', 'LanguageController@create')->name('languages.create');
        Route::get('/delete/{id}', 'LanguageController@delete')->name('languages.delete')->where('id', '[0-9]+');
    });

    // Words
    Route::group([
        'prefix' => 'words',
        'middleware' => 'App\Http\Middleware\AccessRules:moderator'
    ], function() {
        Route::match(['get', 'post'], '/', 'WordsController@index')->name('words');
        Route::match(['get', 'post'], '/update/{id}', 'WordsController@update')->name('words.update')->where('id', '[0-9]+');
        Route::match(['get', 'post'], '/create', 'WordsController@create')->name('words.create');
        Route::get('/delete/{id}', 'WordsController@delete')->name('words.delete')->where('id', '[0-9]+');
    });

    // Tags
    Route::group([
        'prefix' => 'tags',
        'middleware' => 'App\Http\Middleware\AccessRules:moderator'
    ], function() {
        Route::match(['get', 'post'], '/', 'TagsController@index')->name('tags');
        Route::match(['get', 'post'], '/update/{id}', 'TagsController@update')->name('tags.update')->where('id', '[0-9]+');
        Route::match(['get', 'post'], '/create', 'TagsController@create')->name('tags.create');
        Route::get('/delete/{id}', 'TagsController@delete')->name('tags.delete')->where('id', '[0-9]+');
    });
});
