<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 23.07.2017
 * Time: 0:57
 */

namespace Modules\Admin\Http\Components;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;

abstract class CrudController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $formData = [];

        if ($request->isMethod('post') && $request->ajax() && $form = $request->get($this->getParam)) {
            $rules = $this->model->rulesSearch();

            $form = array_filter($form, function($value) {
                return $value != '';
            });

            $formData = array_intersect_key($form, $rules);

            Validator::make($formData, $rules)->validate();
        }

        return view("admin::{$this->name}.grid", $this->model->search($formData));
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        if ($request->isMethod('post') && $request->ajax() && $form = $request->get($this->getParam)) {
            $this->model->isNewRecord = true;

            $rules = $this->model->rulesUpdate();

            $form = array_filter($form, function($value) {
                return $value != '';
            });

            $formData = array_intersect_key($form, $rules);

            Validator::make($formData, $rules)->validate();

            if ($this->model->changeItem($formData)) {
                return ['status' => 'success'];
            } else {
                return ['status' => 'error'];
            }
        }

        return View("admin::{$this->name}.form", [
            'model' => $this->model,
            'isNewRecord' => true
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        if (is_null($model = $this->model->find($id))) {
            abort(404);
        }

        if ($request->isMethod('post') && $request->ajax() && $form = $request->get($this->getParam)) {
            $this->model->isNewRecord = false;

            $rules = $model->rulesUpdate();

            $form = array_filter($form, function($value) {
                return $value != '';
            });

            $formData = array_intersect_key($form, $rules);

            Validator::make($formData, $rules)->validate();

            if ($model->changeItem($formData)) {
                return ['status' => 'success'];
            } else {
                return ['status' => 'error'];
            }
        }

        return View("admin::{$this->name}.form", [
            'model' => $model,
            'isNewRecord' => false
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return Redirect
     */
    public function delete(Request $request, $id)
    {
        if (is_null($model = $this->model->find($id))) {
            abort(404);
        }

        $model->delete();

        return redirect()->route($this->name);
    }
}