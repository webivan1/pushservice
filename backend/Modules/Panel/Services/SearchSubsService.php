<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 16.09.2017
 * Time: 17:40
 */

namespace Modules\Panel\Services;

use App\Components\SearchInterface;
use App\Components\BaseService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Validation\Validator;
use Modules\Admin\Models\Tags;

class SearchSubsService extends BaseService implements SearchInterface
{
    /**
     * @property int
     */
    public $id;

    /**
     * @property string
     */
    public $token;

    /**
     * @property string
     */
    public $lang;

    /**
     * @property string
     */
    public $userAgent;

    /**
     * @property string
     */
    public $referer;

    /**
     * @property string
     */
    public $create;

    /**
     * @property array<string>
     */
    public $tags;

    /**
     * @property string
     */
    public $browser;

    /**
     * @property string
     */
    public $device;

    /**
     * @property string
     */
    public $os_name;

    /**
     * @property string
     */
    public $last_active;

    /**
     * @inheritdoc
     */
    public function params(): array
    {
        return [
            'id' => function (Builder $query, int $value) {
                $query->where('t.id', '=', $value);
            },
            'token' => function (Builder $query, string $value) {
                $query->where('t.token', '=', $value);
            },
            'lang' => function (Builder $query, string $value) {
                $query->where('t.lang', 'LIKE', "%$value%");
            },
            'userAgent' => function (Builder $query, string $value) {
                $query->where('t.user_agent', 'LIKE', "%$value%");
            },
            'device' => function (Builder $query, string $value) {
                $query->where('t.device', 'LIKE', "%$value%");
            },
            'browser' => function (Builder $query, string $value) {
                $query->where('t.browser', 'LIKE', "%$value%");
            },
            'os_name' => function (Builder $query, string $value) {
                $query->where('t.os_name', 'LIKE', "%$value%");
            },
            'referer' => function (Builder $query, string $value) {
                $query->where('t.referer', 'LIKE', "%$value%");
            },
            'create' => function (Builder $query, string $value) {
                $query->where('t.created_at', '>=', date('Y-m-d', strtotime($value)));
            },
            'last_active' => function (Builder $query, string $value) {
                $query->where('t.last_active', '>=', date('Y-m-d', strtotime($value)));
            },
            'tags' => function (Builder $query, array $value) {
                $value = array_filter($value, function ($value) {
                    return is_string($value);
                });

                $query->join('tag_sub AS ts', 'ts.sub_id', '=', 't.id')
                    ->join('tags', function ($join) use ($value) {
                        $join->on('tags.id', '=', 'ts.tag_id')
                            ->whereIn('tags.alias', $value);
                    });
            }
        ];
    }

    /**
     * @inheritdoc
     */
    public function rule(): Validator
    {
        return \Illuminate\Support\Facades\Validator::make($this->toArray(), [
            'id' => 'required|integer',
            'token' => 'required|string',
            'lang' => 'required|string',
            'userAgent' => 'required|string',
            'referer' => 'required|string',
            'create' => 'required|date',
            'last_active' => 'required|date',
            'tags' => 'required|array',
            'device' => 'required|string|max:100',
            'browser' => 'required|string|max:100',
            'os_name' => 'required|string|max:100',
        ]);
    }
}