<?php
/**
 * Created by PhpStorm.
 * User: maltsev
 * Date: 28.09.2017
 * Time: 16:05
 */

namespace Modules\Panel\Services;

use App\Components\BaseService;
use Modules\Panel\Models\Apps;

class AppDataItemService extends BaseService
{
    /**
     * @property Apps
     */
    public $app;
}