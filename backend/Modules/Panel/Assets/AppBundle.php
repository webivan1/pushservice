<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 14.10.2017
 * Time: 1:52
 */

namespace Modules\Panel\Assets;

use App\Assets\AppBundle as Base;

class AppBundle extends Base
{
    /**
     * Дефолтный урл скриптов
     *
     * @property string
     */
    public $basePath = '/assets/panel/bundle';

    /**
     * @property array
     */
    public $js = [
        '/bower_components/chart.js/dist/Chart.bundle.min.js'
    ];
}