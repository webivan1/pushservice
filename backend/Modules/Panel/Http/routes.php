<?php

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => [
        'localeSessionRedirect',
        'localizationRedirect',
        'localeViewPath',
    ]
], function () {
    /** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/

    Route::group([
        'middleware' => [
            'web',
            'auth',
            'App\Http\Middleware\AccessRules:user'
        ],
        'prefix' => 'panel',
        'namespace' => 'Modules\Panel\Http\Controllers'
    ], function() {

        Route::get('/', 'PanelController@index')->name('panel.index');

        Route::group(['prefix' => 'user'], function() {
            Route::post('/change-name', 'Profile\ProfileController@changeName');
            Route::post('/send-code-email', 'Profile\ChangeEmailController@sendKey');
            Route::post('/save-new-email', 'Profile\ChangeEmailController@changeEmail');
            Route::post('/change-password', 'Profile\ChangePasswordController@changePassword');
        });

        Route::group(['prefix' => 'apps'], function() {
            Route::post('/get', 'Apps\AppsListController@getModels');
            Route::post('/create', 'Apps\AppsCreateController@createAction');
            Route::post('/{id}', 'Apps\AppsGetInfoController@getAction')->where('id', '[0-9]+');
            Route::delete('/delete/{id}', 'Apps\AppDeleteController@delete')->where('id', '[0-9]+');
        });

        Route::group([
            'prefix' => 'templates',
            'middleware' => [
                'Modules\Panel\Http\Middleware\AccessApp:app'
            ]
        ], function() {
            Route::post('/{app}/get', 'Templates\TemplateListController@getModels')->where(['app' => '[0-9]+']);
            Route::post('/{app}/create', 'Templates\CreateTemplateController@create')->where(['app' => '[0-9]+']);
            Route::get('/{app}/update/{id}', 'Templates\GetTemplateController@getData')->where(['app' => '[0-9]+', 'id' => '[0-9]+']);
            Route::post('/{app}/update/{id}', 'Templates\UpdateTemplateController@edit')->where(['app' => '[0-9]+', 'id' => '[0-9]+']);
            Route::delete('/{app}/delete/{id}', 'Templates\DeleteTemplateController@delete')->where(['app' => '[0-9]+', 'id' => '[0-9]+']);
        });

        Route::group(['prefix' => 'subs'], function() {
            Route::post('/get/{id}', 'Subs\SubsListController@getModels')->where('id', '[0-9]+');
        });

        Route::group(['prefix' => 'tags'], function() {
            Route::post('/search/{id}', 'Tags\TagsSearchController@search')->where('id', '[0-9]+');
        });

        Route::get('{any}', function () {
            return view('panel::index');
        })->where('any', '(.*)?');

    });
});
