<?php
/**
 * Created by PhpStorm.
 * User: maltsev
 * Date: 28.09.2017
 * Time: 15:56
 */

namespace Modules\Panel\Http\Middleware;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Panel\Models\Apps;
use Modules\Panel\Services\AppDataItemService;

class AccessApp
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param string $inputParam
     * @return mixed
     */
    public function handle(Request $request, \Closure $next, string $inputParam)
    {
        $appId = $request->route($inputParam);

        if (empty($appId) || !is_numeric($appId)) {
            abort(403);
        }

        $app = Apps::where('id', '=', (int) $appId)
            ->where('user_id', '=', Auth::id())
            ->firstOrFail(['key', 'user_id', 'id', 'name']);

        // Save the app
        AppDataItemService::getInstance(['app' => $app]);

        return $next($request);
    }
}