<?php
/**
 * Created by PhpStorm.
 * User: maltsev
 * Date: 14.09.2017
 * Time: 13:28
 */

namespace Modules\Panel\Http\Controllers\Apps;

use Modules\Panel\Models\Apps;
use Illuminate\Routing\Controller;

class AppsListController extends Controller
{
    /**
     * Action get models apps
     */
    public function getModels()
    {
        return Apps::search(4);
    }
}