<?php
/**
 * Created by PhpStorm.
 * User: maltsev
 * Date: 11.10.2017
 * Time: 11:19
 */

namespace Modules\Panel\Http\Controllers\Apps;

use Illuminate\Routing\Controller;
use Modules\Panel\Models\Apps;

class AppDeleteController extends Controller
{
    public function delete(int $id)
    {
        return Apps::deleteItem($id)
            ? response()->json(['status' => 'success'])
            : response()->json(['status' => 'fail'], 422);
    }
}