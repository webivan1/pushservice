<?php
/**
 * Created by PhpStorm.
 * User: maltsev
 * Date: 15.09.2017
 * Time: 13:52
 */

namespace Modules\Panel\Http\Controllers\Apps;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Panel\Models\Apps;

class AppsGetInfoController extends Controller
{
    /**
     * Action get model
     *
     * @param int $id
     * @return Response
     */
    public function getAction(int $id)
    {
        $this->validate($id);

        return $this->find($id);
    }

    /**
     * Find app
     *
     * @param int $id
     * @return Response
     */
    private function find($id)
    {
        $model = Apps::getItem($id);

        return empty($model)
            ? response()->json(['status' => '404'], 404)
            : $model;
    }

    /**
     * Validate
     *
     * @param int $id
     */
    private function validate($id)
    {
        Validator::make(['id' => $id], [
            'id' => 'required|integer|exists:apps',
        ])->validate();
    }
}