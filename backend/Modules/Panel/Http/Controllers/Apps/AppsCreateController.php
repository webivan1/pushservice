<?php
/**
 * Created by PhpStorm.
 * User: maltsev
 * Date: 15.09.2017
 * Time: 12:58
 */

namespace Modules\Panel\Http\Controllers\Apps;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\Panel\Models\Apps;

class AppsCreateController extends Controller
{
    /**
     * Action create App
     *
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request)
    {
        $name = $request->input('name');

        $this->validate($name);

        return $this->createApp($name);
    }

    /**
     * Validate
     */
    private function validate(string $name)
    {
        Validator::make(['name' => $name], [
            'name' => 'required|string|min:2|max:25'
        ])->validate();
    }

    /**
     * Create App
     *
     * @param string $name
     * @return Response
     */
    private function createApp(string $name)
    {
        $model = new Apps();
        $model->name = $name;

        return $model->insertApp()
            ? response()->json(['status' => 'success', 'id' => $model->id], 200)
            : response()->json(['name' => trans('data.ErrorCreateAppsServer')], 422);
    }
}