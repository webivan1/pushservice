<?php

namespace Modules\Panel\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class PanelController extends Controller
{
    /**
     * View layout start
     *
     * @return Response
     */
    public function index()
    {
        return view('panel::index');
    }
}
