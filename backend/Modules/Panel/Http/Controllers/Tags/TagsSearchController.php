<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 17.09.2017
 * Time: 18:39
 */

namespace Modules\Panel\Http\Controllers\Tags;

use App\Helpers\Html;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Panel\Models\Tags;

class TagsSearchController extends Controller
{
    /**
     * Action /{lang}/panel/tags/search/{app}
     *
     * @param int $id
     * @param Request $request
     */
    public function search(int $id, Request $request)
    {
        $this->validate($value = $request->input('value'));

        return response()->json(Tags::autocomplete(Html::encode($value), $id) ?? [], 200);
    }

    /**
     * Validate
     *
     * @param string $value
     */
    private function validate($value)
    {
        Validator::make(['value' => $value], [
            'value' => 'required|string|min:2'
        ])->validate();
    }
}