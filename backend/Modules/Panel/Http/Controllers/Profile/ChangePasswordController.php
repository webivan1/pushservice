<?php
/**
 * Created by PhpStorm.
 * User: maltsev
 * Date: 13.09.2017
 * Time: 14:42
 */

namespace Modules\Panel\Http\Controllers\Profile;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;

class ChangePasswordController extends Controller
{
    /**
     * Action /{lang}/panel/user/change-password
     *
     * @param Request $request
     * @return Response
     */
    public function changePassword(Request $request)
    {
        $this->validateDatas($data = [
            'password' => $request->input('password'),
            'newPassword' => $request->input('newPassword')
        ]);

        return $this->handler($data);
    }

    /**
     * Validate
     *
     * @param array $data
     */
    private function validateDatas(array $data)
    {
        Validator::make($data, [
            'password' => 'required|string|min:5',
            'newPassword' => 'required|string|min:5'
        ])->validate();
    }

    /**
     * Save new password
     *
     * @param array $data
     * @return Response
     */
    private function handler(array $data)
    {
        if (!Auth::validate(['password' => $data['password']])) {
            return response()->json([
                'status' => 'error',
                'message' => 'ErrorPasswordProfileForm'
            ]);
        }

        $user = User::identity(Auth::id());
        $user->password = bcrypt($data['newPassword']);
        $user->save();

        return response()->json([
            'status' => 'success',
            'message' => 'SuccessPasswordProfileForm'
        ]);
    }
}

