<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 12.09.2017
 * Time: 23:12
 */

namespace Modules\Panel\Http\Controllers\Profile;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class ChangeEmailController extends Controller
{
    /**
     * @property string
     */
    private $email;

    /**
     * Send key code
     *
     * @param Request $request
     */
    public function sendKey(Request $request)
    {
        $this->validateEmail($this->email = $request->input('email'));

        return $this->sendMail();
    }

    /**
     * Change email
     *
     * @param Request $request
     */
    public function changeEmail(Request $request)
    {
        $this->validateDatas($data = [
            'email' => $request->input('email'),
            'secretKey' => $request->input('secretKey')
        ]);

        if ($this->hasEmailAndSecretKey($data, $request)) {
            $this->saveEmail($data['email']);
            // response 200 OK
            return response()->json(['status' => 'success'], 200);
        } else {
            return response()->json(['secretKey' => 'Not valid'], 422);
        }
    }

    /**
     * Проверяем email и секретный ключ
     *
     * @param array $data
     * @return bool
     */
    private function hasEmailAndSecretKey(array $data, Request $request): bool
    {
        $cookieValue = $request->cookie(md5($data['email']));

        if (empty($cookieValue)) {
            return false;
        }

        $key = Crypt::decryptString($cookieValue);

        if ($key !== $data['secretKey']) {
            return false;
        }

        return true;
    }

    /**
     * Save new email
     *
     * @param string $email
     */
    private function saveEmail(string $email)
    {
        $model = User::identity(Auth::id());
        $model->email = $email;
        return $model->save();
    }

    /**
     * Validate
     *
     * @param array $data
     */
    private function validateDatas(array $data)
    {
        Validator::make($data, [
            'email' => 'required|string|email|max:70|unique:users',
            'secretKey' => 'required|string|min:6|max:16'
        ])->validate();
    }

    /**
     * Validate
     *
     * @param string $email
     */
    private function validateEmail(string $email)
    {
        Validator::make(['email' => $email], [
            'email' => 'required|string|email|max:70|unique:users'
        ])->validate();
    }

    /**
     * Random code
     *
     * @return string
     */
    private function generateKeyCode(): string
    {
        return str_random(mt_rand(6, 16));
    }

    /**
     * Send mail secretKey
     *
     * @param Request $request
     */
    private function sendMail()
    {
        $key = $this->generateKeyCode();

        // save
        $cookie = cookie(md5($this->email), Crypt::encryptString($key), 60);

        Mail::send('emails.keyEmail', ['user' => Auth::user(), 'key' => $key], function ($model) {
            $model->from(config('email'), config('name'));
            $model->to($this->email, Auth::user()->name)->subject(trans('data.SubjectMailChangeEmail'));
        });

        return response('success')->cookie($cookie);
    }
}