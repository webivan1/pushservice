<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 10.09.2017
 * Time: 20:20
 */

namespace Modules\Panel\Http\Controllers\Profile;

use App\User;
use Illuminate\Auth\Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    /**
     * Edit username action
     *
     * @param Request $request
     */
    public function changeName(Request $request)
    {
        $this->validateUserName($user = ['userName' => $request->input('name')]);

        $this->changeAttributeUser(
            User::identity(Auth::id()), 'name', $user['userName']
        );
    }

    /**
     * Validate username
     *
     * @throws error 422
     * @param array $data
     */
    private function validateUserName(array $data)
    {
        Validator::make($data, [
            'userName' => 'required|string|min:3'
        ])->validate();
    }

    /**
     * Save new attribute user
     *
     * @param User $model
     * @param string $attribute
     * @param any $value
     */
    private function changeAttributeUser(User $model, string $attribute, $value)
    {
        if ($model) {
            $model->{$attribute} = $value;
            $model->save();
        }
    }
}