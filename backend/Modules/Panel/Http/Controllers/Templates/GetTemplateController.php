<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 30.09.2017
 * Time: 20:09
 */

namespace Modules\Panel\Http\Controllers\Templates;

use App\Models\Templates;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Panel\Services\AppDataItemService;

class GetTemplateController extends Controller
{
    /**
     * Action get all templates
     */
    public function getData(Request $request, int $app, int $id)
    {
        return Templates::getTemplate($id, AppDataItemService::getInstance()->app->id);
    }
}