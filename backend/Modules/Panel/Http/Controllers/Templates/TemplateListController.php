<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 01.10.2017
 * Time: 17:18
 */

namespace Modules\Panel\Http\Controllers\Templates;

use Modules\Panel\Models\Templates;
use Nwidart\Modules\Routing\Controller;

class TemplateListController extends Controller
{
    /**
     * Action get models template
     */
    public function getModels()
    {
        return Templates::search(5);
    }
}