<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 10.10.2017
 * Time: 22:52
 */

namespace Modules\Panel\Http\Controllers\Templates;

use Illuminate\Routing\Controller;
use Modules\Panel\Models\Templates;

class DeleteTemplateController extends Controller
{
    /**
     * Action delete item template
     *
     * @param int $app
     * @param int $id
     * @return array
     */
    public function delete(int $app, int $id)
    {
        return Templates::deleteItem($id)
            ? response()->json(['status' => 'ok'])
            : response()->json(['status' => 'fail'], 422);
    }
}