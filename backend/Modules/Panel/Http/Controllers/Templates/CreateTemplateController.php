<?php
/**
 * Created by PhpStorm.
 * User: maltsev
 * Date: 28.09.2017
 * Time: 16:10
 */

namespace Modules\Panel\Http\Controllers\Templates;

use App\Models\Templates;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Panel\Services\AppDataItemService;

class CreateTemplateController extends Controller
{
    /**
     * @property Templates
     */
    private $model;

    /**
     * Action create new template
     *
     * @param Request $request
     * @param int $app
     * @return Response
     */
    public function create(Request $request, $app)
    {
        $this->model = new Templates();

        // validate
        $this->validate(
            $this->outputItem($request)
        );

        return response()->json(['status' => 'ok', 'id' => $this->model->id], 200);
    }

    /**
     * Filter item
     *
     * @param Request $request
     * @return array
     */
    private function outputItem(Request $request): array
    {
        $data = array_intersect_key($request->input() ?? [], $this->model->rules());
        $data = array_filter($data, function ($value) {
            return !empty($value);
        });

        return array_merge($data, [
            'app_id' => AppDataItemService::getInstance()->app->id
        ]);
    }

    /**
     * Validate
     *
     * @param array $datas
     * @return void
     */
    private function validate(array $datas)
    {
        $this->model->setAttributes($datas);
        $this->model->validate($datas);
        $this->model->save();
    }
}