<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 30.09.2017
 * Time: 15:00
 */

namespace Modules\Panel\Http\Controllers\Templates;

use App\Models\Templates;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Panel\Services\AppDataItemService;

class UpdateTemplateController extends Controller
{
    /**
     * @property Templates
     */
    private $model;

    /**
     * Action edit template
     *
     * @param Request $request
     * @param int $app
     * @param int $id
     * @return Response
     */
    public function edit(Request $request, int $app, int $id)
    {
        $this->model = $this->getModel($id);

        $this->validate(
            $this->outputItem($request)
        );

        // Save model
        $this->model->save();

        return response()->json(['status' => 'ok'], 200);
    }

    /**
     * Filter item
     *
     * @param Request $request
     * @return array
     */
    private function outputItem(Request $request): array
    {
        $data = array_intersect_key($request->input() ?? [], $this->model->rules());
        $data = array_filter($data, function ($value) {
            return !empty($value);
        });

        return array_merge($data, [
            'app_id' => AppDataItemService::getInstance()->app->id
        ]);
    }

    /**
     * Validate
     *
     * @param array $datas
     * @return void
     */
    private function validate(array $datas)
    {
        $this->model->setAttributes($datas);
        $this->model->validate();
    }

    /**
     * Get model
     *
     * @param int $id
     * @return Templates
     */
    private function getModel(int $id): Templates
    {
        return Templates::getTemplate($id, AppDataItemService::getInstance()->app->id);
    }
}