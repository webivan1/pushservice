<?php
/**
 * Created by PhpStorm.
 * User: maltsev
 * Date: 14.09.2017
 * Time: 13:28
 */

namespace Modules\Panel\Http\Controllers\Subs;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Routing\Controller;
use Modules\Panel\Models\Subs;
use Modules\Panel\Services\SearchSubsService;

class SubsListController extends Controller
{
    /**
     * Action get models apps
     */
    public function getModels(int $id, Request $request)
    {
        $this->validate($id);

        // Save request all params
        SearchSubsService::getInstance($request->all());

        return Subs::search($id, 25);
    }

    /**
     * Validate
     */
    private function validate($id)
    {
        Validator::make(['id' => $id], [
            'id' => 'required|integer|exists:apps'
        ])->validate();
    }
}