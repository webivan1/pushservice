<?php
/**
 * Created by PhpStorm.
 * User: maltsev
 * Date: 15.09.2017
 * Time: 18:00
 */

namespace Modules\Panel\Models;

use App\Components\SearchComponent;
use App\Components\Sort;
use App\Helpers\Hash;
use App\Models\Subscribers;
use App\Models\TagSub;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\Panel\Services\SearchSubsService;

class Subs extends Subscribers
{
    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        TagSub::where('sub_id', '=', $this->id)->delete();
    }

    /**
     * Relation get tags
     */
    public function getTags()
    {
        return $this->hasMany(TagSub::class, 'id', 'tag_sub')
            ->with('getTags');
    }

    /**
     * Get datas
     *
     * @param int
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public static function search(int $appId, int $pageNums = 25)
    {
        $query = self::alias('t')
            ->with('getTags')
            ->select([
                't.id', 't.app_id', 't.lang', 't.browser', 't.last_active',
                't.referer', 't.dev', 't.created_at', 't.device', 't.os_name'
            ])
            ->join('apps AS a', function ($join) {
                $join->on('a.id', '=', 't.app_id')
                    ->where('a.user_id', '=', Auth::id())
                    ->where('a.state', '=', 2);
            })
            ->where('t.app_id', '=', $appId)
            ->groupBy(['t.id']);

        // with search params
        new SearchComponent($query, SearchSubsService::getInstance());

        // sort
        $sort = new Sort([
            'attributes' => [
                'id' => [
                    'asc' => [
                        ['t.id', Sort::SORT_ASC]
                    ],
                    'desc' => [
                        ['t.id', Sort::SORT_DESC]
                    ]
                ],
                'created_at' => [
                    'asc' => [
                        ['t.created_at', Sort::SORT_ASC]
                    ],
                    'desc' => [
                        ['t.created_at', Sort::SORT_DESC]
                    ]
                ],
                'active' => [
                    'asc' => [
                        ['t.last_active', Sort::SORT_ASC]
                    ],
                    'desc' => [
                        ['t.last_active', Sort::SORT_DESC]
                    ]
                ]
            ],
            'defaultOrder' => 'created_at.desc'
        ]);

        foreach ($sort->sortBy() as $sortItem) {
            $query->orderBy($sortItem[0], $sortItem[1]);
        }

        return $query->paginate($pageNums);
    }
}