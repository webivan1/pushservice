<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 01.10.2017
 * Time: 17:20
 */

namespace Modules\Panel\Models;

use App\Models\TemplateContent;
use App\Models\Templates as Base;
use Modules\Panel\Services\AppDataItemService;

class Templates extends Base
{
    /**
     * Get datas
     *
     * @param int
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public static function search(int $pageNums = 6)
    {
        return self::where('app_id', '=', AppDataItemService::getInstance()->app->id)
            ->orderBy('id', 'desc')
            ->paginate($pageNums);
    }

    /**
     * Delete item by id
     *
     * @param int $id
     * @return bool
     */
    public static function deleteItem(int $id)
    {
        $model = self::where('id', '=', $id)
            ->where('app_id', '=', AppDataItemService::getInstance()->app->id)
            ->firstOrFail();

        return $model->delete();
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        TemplateContent::where('template_id', '=', $this->id)->delete();
    }
}