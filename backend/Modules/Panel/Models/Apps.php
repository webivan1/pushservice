<?php
/**
 * Created by PhpStorm.
 * User: maltsev
 * Date: 14.09.2017
 * Time: 13:31
 */

namespace Modules\Panel\Models;

use App\Helpers\ArrayHelper;
use App\Models\Apps as Base;
use App\Models\Subscribers;
use Faker\Provider\DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class Apps extends Base
{
    /**
     * @inheritdoc
     */
    public function beforeDelete(): bool
    {
        Subs::where('app_id', '=', $this->id)->delete();

        $collectionTemplates = Templates::where('app_id', '=', $this->id)->get();

        foreach ($collectionTemplates as $template) {
            $template->delete();
        }

        $collectionTags = Tags::where('app_id', '=', $this->id)->get();

        foreach ($collectionTags as $tag) {
            $tag->delete();
        }

        return true;
    }

    /**
     * Delete app
     *
     * @param int $id
     * @return bool
     */
    public static function deleteItem(int $id)
    {
        $model = self::where('user_id', '=', Auth::id())
            ->where('id', '=', $id)
            ->firstOrFail();

        return $model->delete();
    }

    /**
     * Relation stat
     */
    public function getStatSubs()
    {
        return $this->hasMany(Subscribers::class, 'app_id', 'id')
            ->select([DB::raw('COUNT(id) AS items'), DB::raw('DAY(created_at) AS day'), 'app_id'])
            ->where('created_at', '>=', DB::raw('NOW() - INTERVAL 7 DAY'))
            ->groupBy([DB::raw('DATE(created_at)')]);
    }

    /**
     * Relation detail stat
     */
    public function getDetailStatSubs()
    {
        return $this->hasMany(Subscribers::class, 'app_id', 'id')
            ->select([
                DB::raw('COUNT(id) AS items'),
                DB::raw('DAY(created_at) AS day'),
                DB::raw('DATE_FORMAT(DATE(created_at), "%Y-%c") AS date'),
                'app_id',
            ])
            ->groupBy([DB::raw('DATE(created_at)')])
            ->orderBy('date')
            ->orderBy('day');
    }

    /**
     * Get datas
     *
     * @param int
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public static function search(int $pageNums = 6)
    {
        $query = self::from('apps AS t')
            ->with('getStatSubs')
            ->where('t.user_id', '=', Auth::id())
            ->where('t.state', '=', 2)
            ->select(['t.key', 't.name', 't.id', DB::raw('COUNT(DISTINCT(s.id)) AS count_subs')])
            ->leftJoin('subscribers AS s', 's.app_id', '=', 't.id')
            ->groupBy(['t.id']);

        return $query->paginate($pageNums);
    }

    /**
     * Unique key code
     *
     * @return string
     */
    public static function generateUniqueKey(): string
    {
        $key = str_random(16);

        $validator = Validator::make(['key' => $key], [
            'key' => 'required|string|unique:apps'
        ]);

        if ($validator->fails()) {
            return self::generateUniqueKey();
        } else {
            return $key;
        }
    }

    /**
     * Create new app
     *
     * @return bool
     */
    public function insertApp(): bool
    {
        $this->user_id = Auth::id();
        $this->state = 2;
        $this->key = self::generateUniqueKey();
        $this->server_key = bcrypt(str_random(24));
        return (bool) $this->save();
    }

    /**
     * Item model app
     *
     * @param int $id
     * @return Apps
     */
    public static function getItem(int $id)
    {
        $datas = self::where('id', '=', $id)->where('state', '=', 2)->with('getDetailStatSubs')->first();

        self::groupDatasChart($datas);

        return $datas;
    }

    /**
     * Item model app
     *
     * @param Apps $datas
     */
    public static function groupDatasChart(Apps &$datas)
    {
        if (!empty($datas->getDetailStatSubs)) {
            $monthAll = [];

            foreach ($datas->getDetailStatSubs as $sub) {
                $monthAll[] = $sub->date;
            }

            $monthAll = array_unique($monthAll);
            sort($monthAll);

            $groupMonth = ArrayHelper::map($datas->getDetailStatSubs, 'date', ['day', 'items'], true);

            $chartDatas = [];
            $chartLabels = [];

            foreach ($groupMonth as $monthKey => $data) {

                list($year, $month) = explode('-', $monthKey);

                //$totalNumDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);
                $totalNumDays = 31;

                if (!$totalNumDays) {
                    continue;
                }

                $fullDaysStat = [];

                for ($i = 1; $i <= $totalNumDays; $i++) {
                    $countSubs = 0;

                    foreach ($data as $row) {
                        if ($row[0] === $i) {
                            $countSubs = $row[1];
                            break;
                        }
                    }

                    $fullDaysStat[$i] = [$i, $countSubs];
                }

                $chartDatas[$monthKey] = array_column($fullDaysStat, 1);
                $chartLabels[$monthKey] = array_column($fullDaysStat, 0);
            }

            $datas['chart'] = [
                'datas' => $chartDatas,
                'labels' => $chartLabels,
                'monthAll' => array_map(function ($date) {
                    return [
                        'value' => $date,
                        'label' => date_format(date_create($date . '-1'), 'F, Y')
                    ];
                }, $monthAll)
            ];
        }
    }
}