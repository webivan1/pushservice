<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 17.09.2017
 * Time: 18:42
 */

namespace Modules\Panel\Models;

use App\Models\Tags as Base;
use App\Models\TagSub;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class Tags extends Base
{
    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        TagSub::where('tag_id', '=', $this->id)->delete();
    }

    /**
     * Autocomplete tags
     *
     * @param string $value
     * @param int $appId
     * @return
     */
    public static function autocomplete($value, $appId)
    {
        return Cache::remember("TagsArrayAutocomplete-$appId-$value", 60 * 2, function () use ($value, $appId) {
            return self::from('tags AS t')
                ->select(['t.alias'])
                ->leftJoin('apps AS a', function ($join) use ($appId) {
                    $join->on('a.id', '=', 't.app_id')
                        ->where('a.user_id', '=', Auth::id())
                        ->where('a.id', '=', $appId);
                })
                ->whereRaw("t.alias LIKE ? AND (t.app_id = 0 OR a.id IS NOT NULL)", ["%$value%"])
                ->groupBy(['t.alias'])
                ->orderBy('t.alias')
                ->limit(20)
                ->get();
        });
    }
}